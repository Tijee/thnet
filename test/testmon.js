'use strict';

const spawn = require('child_process').spawn;
const net = require('net');
const thnet = require('../thnet');

if(thnet.tjapp){
    const server = net.createServer( function(c) {
        info("connect from ", c.remoteAddress);
        c.on('end', () => {
           console.log('client disconnected');
        });
        c.write('hello\r\n');
        c.pipe(c);
    });
    server.listen({fd:3});

    //tcp.on('error', function(err){
    //    error('fd 3 error', err);
    //});
    //assert(tcp.address().port == 3398, "fd 3 failed");

    thnet.CtrlSend('none', 'einfo', {}, function(ob, peer, type){
        thnet.assert(ob.error === 'unknown peer', 'CtrlSend einfo', JSON.stringify(ob));
    });
    thnet.CtrlSend('tjapps', 'info', {}, function(ob, peer, type){
        thnet.assert(ob.hwid && ob.euid, "CtrlSend tjapps info", JSON.stringify(ob));
    });
    thnet.CtrlSend('thnet', 'start', {hid:0, prefix:"2002::1", addr:'20.1.1.1'}, function(ob, peer, type){
        thnet.assert(ob.action>0 && ob.tun>0, "CtrlSend thnet start", JSON.stringify(ob));
    });
    
    setTimeout(function(){
        process.exit(33);
    }, 2000);
    return;
}

var options = {
    thnet: {
        interfaces: {
            tun: 'virtual',
            udp: 3399,
        },
    },
    test: {
        fd: {
            'tcp/[::1]:3398': 3,
        },
        uid: process.geteuid(),
        'exec': '/usr/bin/node ' + __dirname + '/testmon.js',
    },
};

thnet.info(JSON.stringify(options));
var app = spawn(__dirname + '/../build/Debug/tjapps', [JSON.stringify(options)]);
app.stderr.on("data", function(x){
    console.error( x.toString('ascii').trim() );
});

var t1;
var t2;
app.on('close', function(code){
    console.assert(t1 === null && t2 !== null, "not ended by SIGINT");
    console.assert(code === 0, "return code != 0");
    clearTimeout(t2);
});

function sigint(){
    clearTimeout(t1);
    t1 = null;
    app.kill('SIGINT');
    thnet.error('-- SIGINT');
    t2 = setTimeout(function(){
        process.exit(2);
        thnet.error('-- SIGTERM');
        t2 = null;
        app.kill();
    }, 2000);
}
setTimeout(sigint, 60000);

