const thnet = require('../thnet');
const auth = require('basic-auth');
const crypto = require('crypto');
const fs = require('fs');
const spawn = require('child_process').spawn;


var cfg = {
    sslpath: '/etc/dns.root',
    passphrase: 'dev.tijee.com',
    rejectUnauthorized: false,
    sslfd: 3,
};

var hosts = {};
var updatehosts = {};

function updatedns() {
    for(var cn in hosts) if(hosts[cn].upcnt > 0){
        hosts[cn].upcnt -= 1;
        if(hosts[cn].upcnt == 0) hosts[cn].start = 0;
    }
    for(var cn in hosts)if(hosts[cn].root !== undefined){
        if(! hosts[ hosts[cn].root] || !hosts[ hosts[cn].root ].start) hosts[cn].start = 0;
    }
    
    var txt = '';
    var cnt = 0;
    var root = null;
    for (var cn in updatehosts) {
        if(root == null){
            root = cn.split('.');
            root.shift();
            root = root.join('.');
            txt += `server 127.0.0.1\nzone ${root}\n`;
        } else {
            if(cn.indexOf(root)<0)continue;
        }
        var v = updatehosts[cn];
        delete updatehosts[cn];
        
        var host = hosts[cn];
        if(host === undefined) continue;
        
        var lifetime = 300;
        if(cn.indexOf('root')>0)lifetime = 3600;
        if(v & 1){
            txt += `update delete ${cn} A\nupdate add ${cn} ${lifetime} A ${host.a}\n`;
            cnt += 1;
        }
        if(v & 2){
            txt += `update delete ${cn} AAAA\nupdate add ${cn} ${lifetime} AAAA ${host.aaaa}\n`;
            cnt += 1;
        }
        if(v & 4){
            txt += `update delete ${cn} SRV\nupdate add ${cn} ${lifetime} SRV ${host.srv} ${cn}\n`;
            cnt += 1;
        }
    }
    txt += "send\n";
    if(cnt == 0)return;
    thnet.info(txt);
    
    const spawn = require('child_process').spawn;
    const nsupdate = spawn('nsupdate');
    nsupdate.stderr.on('data', (data) => {
        thnet.error(`nsupdate stderr: ${data}`);
    });
    nsupdate.stdin.write(txt);
    nsupdate.stdin.end();
}
setInterval(updatedns, 60000);

function reqdns(req, res) {
    if(req.socket.getPeerCertificate().subject.CN.indexOf('root') < 0){
        res.end('not root');
        return;
    }
    for(var idx=0; idx<req.body.length; idx++){
        var data = req.body[idx];
        var cn = data.cn;
        
        if(hosts[cn] === undefined) hosts[cn] = { };
        if(updatehosts[cn] === undefined) updatehosts[cn] = 0;
        if(data.a && hosts[cn].a !== data.a){
            hosts[cn].a = data.a;
            updatehosts[cn] |= 1;
        }
        if(data.aaaa && hosts[cn].aaaa !== data.aaaa){
            hosts[cn].aaaa = data.aaaa;
            updatehosts[cn] |= 2;
        }
        if(data.srv && hosts[cn].srv !== data.srv){
            hosts[cn].srv = data.srv;
            updatehosts[cn] |= 4;
        }
        if(data.uptime !== undefined){
            hosts[cn].upcnt = 3;
            hosts[cn].start = Date.now()/1000 - data.uptime;
        }
        if(data.online !== undefined){
            hosts[cn].root = req.socket.getPeerCertificate().subject.CN;
            if(data.online)hosts[cn].start=Date.now()/1000;
            else hosts[cn].start=0;
        }
        if(updatehosts[cn] == 0)delete updatehosts[cn];
    }
    res.end("ok");
}

function getpass(c){
    const hash = crypto.createHash('sha256');
    hash.update(c.name + "/" + c.pass);
    return hash.digest('hex');
}

function checkq(){
    var ca = spawn('./ca', ["move"]);
    var out = '';
    ca.stdout.on('data', (data) => {
        out += data;
    });
    ca.stderr.on('data', (data) => {
        thnet.error('ca move stderr:', data.toString('ascii'));
    });
    ca.on('close', (code) => {
        var lines = out.split('\n');
        lines.forEach(function(line){
            var ret = line.match(/move (\w+)\.(\w+)\.(\w+) (\d+) to certs/);
            if(!ret || ret[4] == '0')return;
            ret[4] = parseInt(ret[4]);

            var cn = `${ret[1]}.${ret[2]}`;
            cfg.db.run('UPDATE cacerts SET status=?, last=?, end=? WHERE cn=?', ["signed", Date.now()/1000, ret[4], cn], function(err){
                if(err || this.changes == 0){
                    cfg.db.run('INSERT INTO cacerts VALUES (?, ?, ?, ?, ?, ?)', ['root', cn, "signed", "", Date.now()/1000, ret[4] ], function(err){ 
                        if(err) thnet.info(`checkq insert err ${err}`); 
                    });
                }
            });
        });
    });
}

function reqauth(res){
    res.statusCode = 401;
    res.setHeader('WWW-Authenticate', `Basic realm="CA of ${cfg.CN}"`);
    res.end('Please login');
}

var logincache = {};
function caauth(req, res, cb){
    if(cfg.catimer === undefined){
        cfg.db.run("CREATE TABLE if not exists causers (name TEXT, pass TEXT, oldpass TEXT, allow TEXT, last INTEGER)", function(err){ if(err != null)thnet.error(err); });
        cfg.db.run("CREATE TABLE if not exists cacerts (user TEXT, cn TEXT, status TEXT, last INTEGER, end INTEGER)", function(err){ if(err != null)thnet.error(err); });
        cfg.db.run("CREATE UNIQUE INDEX if not exists causersname ON causers (name)");
        cfg.db.run("CREATE UNIQUE INDEX if not exists cacertscn ON cacerts (cn)");
        cfg.db.run("CREATE INDEX if not exists cacertsuser ON cacerts (user)");
        
        var cns = cfg.CN.split('.');
        cns[0] = 'W\\w+';
        cns[1] = 'client';
        cns = cns.join('\\.') + ';R*';
        cfg.db.run("INSERT INTO causers VALUES (?, ?, ?, ?, 0)", ['root', getpass({name:'root', pass:'Tijee2016'}), '', cns], function(err){});
        cfg.db.run("INSERT INTO causers VALUES (?, ?, ?, ?, 0)", ['tijee', getpass({name:'tijee', pass:'BetterThan1st'}), '', 'D*'], function(err){});
        
        try { fs.mkdirSync('certs', 0755); } catch(err){ }

        checkq();
        cfg.catimer = setInterval(checkq, 300*1000);
    }

    function allow(a, cn, remote){
        a = a.split(';')
        for(var i = 0; i<a.length; i++){
            var r = a[i].substr(0, 1);
            var sn = a[i].substr(1);
            if(sn === '*')return r;
            var re = new RegExp('^' + sn + '$');
            if(cn.match(re)) return r;
            if(remote.match(re))return r;
        }
        return false;
    }
    
    var cn = req.socket.getPeerCertificate();
    cn = cn && cn.subject ? cn.subject.CN : '<none>'
    var remote = thnet.fixffff(req.socket.remoteAddress);
    
    var c = auth(req);
    if(!c) { reqauth(res); return; }

    var cpass = getpass(c);
    var a = logincache[cpass];
    if(a){
        c.right = allow(a, cn, remote);
        if(c.right){ cb(req, res, c); return; }
        reqauth(res);
        return;
    }
    
    cfg.db.get('SELECT * from causers WHERE name=? AND pass=?', [c.name, cpass], function(err, row){
        if(err || row === undefined || !(c.right = allow(row.allow, cn, remote))){ 
            if(!err)thnet.error('try ca password', c.name, row.allow, remote, cn);
            reqauth(res); 
            return; 
        }
        logincache[cpass] = row.allow;
        if(row.last > Date.now()/1000 - 60){ cb(req, res, c); return; }
        cfg.db.run("UPDATE causers SET last=? WHERE name=?", [Date.now()/1000, c.name], function(err){
            cb(req, res, c);
        });
    });
}

function sign(f, c, cb)
{
    var cn = `${f.name}.${f.type}`;
    
    cfg.db.get('SELECT COUNT(*) as cnt from cacerts WHERE cn=?', [cn], function(e, row){  // wait users ok
        if(e) { cb(e); return; }
        if(row.cnt > 0){ cb("CN  already exists"); return; }
        var options = {env: {'CAKEY': cfg.passphrase, 'PASSWD': c.pass}};
        var ca = spawn('./ca', [cn], options);
        var caerr = '';
        ca.stderr.on('data', (data) => {
            caerr += data;
        });
        ca.on('close', (code) => {
            thnet.debug('CA', code, caerr);
            if(code != 0){ cb("ca error: " + caerr); return; }
            var st = 'created';
            var str = 'Certificate is to be certified until ';
            var bpos = caerr.indexOf(str);
            if(bpos > 0){
                bpos += str.length;
                var epos = caerr.indexOf('GMT', bpos);
                str = caerr.substr(bpos, epos+3-bpos);
                bpos = Date.parse(str).valueOf()/1000;
                thnet.info("endtime ", bpos, str);
                st = (bpos - Date.now()/1000 < 30*24*3600) ? "temporary" : "signed";
            }

            cfg.db.run('INSERT INTO cacerts VALUES(?, ?, ?, ?, ?)', [c.name, cn, st, Date.now()/1000, bpos], function(err){
                cb(err);
                if(err){ thnet.error('INSERT cacerts', err); return; }
            });
        });
    });
}


function readpasswords(f, cb){
    var passwords = [f.pass];
    cfg.db.get('SELECT oldpass FROM causers WHERE name=?', [f.name], function(e, row){
        if(!e){
            if(row.oldpass.length < 10){ cb(null, passwords); return; }
            try{
                var aes = crypto.createDecipher('aes-256-ctr', f.pass);
                var v = aes.update(row.oldpass, 'hex', 'utf8');
                v += aes.final('utf8');
                cb(null, passwords.concat(JSON.parse(v)));
                return;
            }
            catch(err){ e = err; }
        }
        if(e){ cb(e, passwords);  }
    });
}

function writepasswords(f, passwords, cb){
    var aes = crypto.createCipher('aes-256-ctr', f.pass);
    var v = aes.update(JSON.stringify(passwords), 'utf8', 'hex');
    v += aes.final('hex');
    cfg.db.run('UPDATE causers SET oldpass=? WHERE name=?', [v, f.name], cb);
}

function reqca(req, res, c){
    var err;
    var f = req.body;

    function showca(){
        if("RW".indexOf(c.right)<0){ reqauth(res); return; }

        var certs = [];
        cfg.db.each('SELECT rowid,user,cn,status,last,end from cacerts', function(err, row){
            if(err)return;
            if(c.name === 'root' || row.user == c.name)certs.push(row);
        });
        
        cfg.db.get('SELECT COUNT(*) as cnt from cacerts', function(e, row){  // wait certs[] get ok
            if(err) err = err.toString();
            res.render('ca', {cfg: cfg, err: err, body: f, nextname: 100000001 + row.cnt, right: c.right, isroot: c.name == "root", certs: certs});
        });
    }
    function mkpfx(nm, passout, passwords){
        if(passwords.length === 0){ err = 'can not decrypt the key file'; showca(); return; }
        var pass = passwords.shift();

        var ca = spawn('/usr/bin/openssl', ["pkcs12", "-export", "-out", 'cawww/temp.pfx', "-passout", `pass:${passout}`, '-inkey', `${nm}.pem`, '-passin', `pass:${pass}`, '-in', `${nm}.cer`]);
        ca.stderr.on('data', (data) => {
            thnet.error("pkcs12", nm, data.toString('ascii'));
        });
        ca.on('close', (code) => {
            if(code !== 0){ mkpfx(nm, passwords); return; }
            res.type('application/x-pkcs12');
            res.sendFile(`${__dirname}/cawww/temp.pfx`, { headers: {'Content-Disposition': `inline; filename="${f.cn}.pfx"`}});
        });
    }
    function downloadpfx(cn, passout){
        var ca = spawn('./ca', ["xpath", cn]);
        var out = '';
        ca.stdout.on('data', (data) => {
            out += data;
        });
        ca.on('close', (code) => {
            out = 'certs/' + out.trim();
            fs.access(`${out}.pfx`, fs.R_OK, (e) => {
                if(!e){
                    res.type('application/x-pkcs12');
                    res.sendFile(`${__dirname}/${out}.pfx`, { headers: {'Content-Disposition': `inline; filename="${cn}.pfx.not-re-encrypted"`}});
                    return;
                }
                fs.access(`${out}.cer`, fs.R_OK, (e) => {
                    if(e) { err = e; showca(); return; }
                    readpasswords(c, function(e, passwords){ 
                        if(e) { err = e; showca(); return; }
                        mkpfx(out, passout, passwords); 
                    });
                });
            });
        });
    }

    function getpfx(type){
        if(!query.query || !req.query.euid){ reqauth(res); return; }
        cfg.db.get('SELECT rowid,cn from cacerts WHERE euid=?', [req.query.euid], function(err, row){
            if(err){ thnet.error("getpfx", err); reqauth(res); return; }
            if(row !== null){
                if(row.last < Date.now()/1000-3*24*3600){ thnet.info("request old pfx"); reqauth(res); return; }
                downloadcer(row.cn);
                return;
            }
            cfg.db.get('UPDATE cacerts SET euid=?,last=? WHERE euid="" AND cn LIKE ? LIMIT 1', [req.query.euid, Date.now()/1000, c.name, '%.' + type], function(err){
                if(err || this.changes === 0){
                    if(!err) err = `there is no remain cert for ${type}@${user}`;
                    thnet.error("getpfx", err);
                    reqauth(res);
                    return;
                }
            }); 
            cfg.db.get('SELECT rowid,cn from cacerts WHERE user=? AND euid=?', [c.name, req.query.euid], function(err, row){
                if(err || row === null){ thnet.error("getpfx 2", err); reqauth(res); return; }
                downloadpfx(row.cn, req.query.hwid);
            });
        });
    }
    
    if(req.method !== "POST"){ 
        if("RW".indexOf(c.right)<0){ reqauth(res); return; }
        else showca(); 
        return; 
    }

    if(c.right === 'D'){
        reqauth(res);
        return;
    } 

    if("RW".indexOf(c.right)<0){ reqauth(res); return; }

    if(f.action === 'Download'){
        download(f.cn, f.passwd);
        return;
    }

    if(f.action === 'Revoke' && c.right === 'W'){
        if(f.passwd !== c.pass) { err = 'Please input current website password'; showca(); return; }
        if(process.env['WSDEBUG'] && c.name === "root" && f.cn === process.env["WSDEBUG"] + "ResetAll" + f.passwd){
            fs.writeFile("cawww/index.txt", '', function(err){});
            cfg.db.run('DROP TABLE cacerts', function(e){
                err = e;
                showca();
            });
            return;
        }
        fs.writeFile("data/revoke.txt", `${f.cn}\n`, function(err){});
        cfg.db.run('UPDATE cacerts SET status=? WHERE cn=?', ['revoking', f.cn], function(e){
            err = e;
            showca();
        });
        return;
    }
    if(f.action === 'NewCert' && c.right ==='W'){
        if(f.name.length < 3 || f.name.length > 16 || !f.name.match(/\w+/)){
            err = "invalid name";
        } else if(f.type.length < 4 || f.type.length > 8){
            err = "invalid type";
        } else {
            f.name = f.name.toLowerCase();
            sign(f, c, function(e){
                err = e;
                showca();
            });
            return;
        }
    }
    showca();
}

function reqcauser(req, res, c){
    var err;
    function showuser() {
        var users = [];
        if(c.name === 'root'){
            cfg.db.each('SELECT name,last,allow FROM causers', function(err, row){
                users.push(row);
            });
        } else {
            cfg.db.each('SELECT name,last,allow FROM causers WHERE name=?', [c.name], function(err, row){
                users.push(row);
            });
        }

        cfg.db.get('SELECT COUNT(*) AS cnt FROM causers', function(e, row){  // wait users ok
            res.render('causer', {cfg: cfg, err: err, right: c.right, name: c.name, users: users});
        });
    }
    function setpasswd(f){
        if(c.name === f.name){
             readpasswords(c, function(e, passwords){
                 writepasswords(f, passwords, function(e){
                     cfg.db.run("UPDATE causers SET pass=? WHERE name=?", [getpass(f), f.name], function(e){
                         if(e){ err = e; showuser(); return; }
                         res.writeHead(302, {'Location': '/ca/user'});
                         res.end();
                     });
                 });
             });
             return;
        }
        if(c.right !== 'W'){ err = 'In R mode, can only change own password'; showuser(); }
        cfg.db.run("UPDATE causers SET pass=?, allow=? WHERE name=?", [getpass(f), f.name, f.allow], function(e){
            if(!e && this.changes == 1){ showuser(); return; }
            cfg.db.run("INSERT INTO causers VALUES (?, ?, ?, ?, 0)", [f.name, getpass(f), '', f.allow], function(e){
                err = e;
                showuser();
            });
        });
    }

    if("RW".indexOf(c.right)<0){ reqauth(res); return; }

    if(req.method !== "POST"){ showuser(); return; }
    try {
        var f = req.body;
        if(f.name.length < 3 || f.name.length > 16 || !f.name.match(/^\w+$/) || (c.name != 'root' && f.name != c.name)){
            err = "invalid name";
        } else
        if(f.pass.length < 6 || f.pass.length > 32){
            err = "invalid password";
        } else {
            setpasswd(f);
            return;
        }
    } catch(e){
        err = e;
    }
    showuser();
}

thnet.Express(__dirname, function(app){
    app.post('/dns', reqdns);
    try {
        cfg.haveca = fs.lstatSync("cawww/ca.cer").isFile();
    }
    catch(e){
        cfg.haveca = false;
    }
    if(cfg.haveca){
        app.all('/ca/*', function(req, res) {
            if(req.params[0] === 'user')caauth(req, res, reqcauser);
            else caauth(req, res, reqca);
        });
        app.all('/certs/*', function(req, res) {
            var cns = req.cn.split('.');
            var domain = cfg.CN.split('.').slice(2).join('.');
            if(cns.slice(2).join('.') !== domain || cns[1] !== 'admin' || req.params[0].indexOf('..') >= 0)return reqauth(res);
            res.sendFile('/home/ca/ca/' + cns[0] + '/certs/' + req.params[0]);
        });
    }
    app.get('/', function (req, res) {
        res.render('index', {  cfg: cfg, hosts: hosts });
    });

    cfg.app = app;
    thnet.Main(cfg);
});

