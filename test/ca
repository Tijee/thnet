#!/usr/bin/python
# online CA

import sys, os, socket, time, subprocess, getpass

def choice(question, default="yes"):
    valid = {"yes": True, "y": True, "ye": True, "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        c = raw_input().lower()
        if default is not None and c == '':
            return valid[default]
        elif c in valid:
            return valid[c]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' (or 'y' or 'n').\n")

def serial():
    with open('serial') as f:
        return f.read().strip().lower()

def getstr(prompt, cb):
    while True:
        sys.stdout.write("Input %s: " % prompt)
        c = raw_input()
        if cb(c): break
    return c

def cpcfg():
    with open('../ca.cfg', 'r') as s:
        with open('ca.cfg', 'w') as t:
            for line in s:
                print >>t, line.strip().replace('${domain}', domain).replace('${ca}', 'ca')

def xpath(csr):
    import hashlib
    m = hashlib.md5()
    # same as dnsroot.js,   4/7b/aaa 0/8f/bbb 9/df/ccc
    x = csr.split('.')
    if x[-1] in ['csr', 'cer', 'pem', 'pfx']:
        x = x[:-1]
        
    for s in x: m.update(s)
    c = m.hexdigest()
    return '%s/%s/%s' % ( c[0:1], c[1:3], '.'.join(x) )

def gettm(x):
    for line in subprocess.check_output(('openssl x509 -in data/%s -noout -dates' % x).split()).split('\n'):
        if line.startswith('notAfter='): return int(time.mktime(time.strptime(line[9:], "%b %d %H:%M:%S %Y GMT")))
    return 0
    

if len(sys.argv) == 3 and sys.argv[1] == 'xpath':
    print xpath(sys.argv[2])
    sys.exit(0)

if len(sys.argv) == 2 and sys.argv[1] == 'move':
    for x in os.listdir('data'):
        ext = ''
        tm = 0
        if x.endswith('.cer'): 
            ext = '.cer'
            tm = gettm(x)
        if x.endswith('.pfx'): ext = '.pfx'
        if ext == '': continue
        xp = ['certs']
        xp.extend( xpath(x).split('/') )
        for i in range(1, len(xp)):
            try:
                os.mkdir('/'.join(xp[:i]))
            except:
                pass
        xp = '/'.join(xp)
        print "move %s %d to %s" %(x, tm, xp + ext)
        os.system('mv data/%s %s' %(x, xp + ext))
    sys.exit(0)


if not os.path.isdir('cawww'):
    print "cawww directory is not found"
    sys.exit(1)
os.chdir('cawww')


import fcntl
lkr = open('.lock', 'w+')
fcntl.flock(lkr, fcntl.LOCK_EX | fcntl.LOCK_NB)
os.environ['RANDFILE'] = ".rnd"

# get CA password for late using
if 'CAKEY' in os.environ:
    key = os.environ["CAKEY"]
else:
    key = getpass.getpass("CA password: ")

# create new CA
if not os.path.isfile('ca.pem'): 
    if not choice("ca.pem not found. CREATE a new root CA?", "no"): sys.exit(1)
    os.system('openssl genrsa -out ca.pem -aes256 2048')

if not os.path.isfile('ca.cer'):
    c = getstr("country code (2 chars, such as US)", lambda c: len(c) == 2)
    st = getstr("state name", lambda c: len(c) >= 2 and len(c) <= 16)
    o = getstr("organization", lambda c: len(c) >= 5 and len(c) <= 64)
    domain = getstr("domain (such as test.com)", lambda c: len(c) >= 5 and len(c) <= 32 and '.' in c)
    cpcfg()
    os.system('openssl req -new -key ca.pem -subj "/C=%s/ST=%s/O=%s/CN=CA of %s" -passin pass:%s -out temp.csr' % (c, st, o, domain, key))
    os.system('openssl x509 -req -days 3650 -extfile ca.cfg -extensions v3_ca_ext -in temp.csr -signkey ca.pem -passin pass:%s -out ca.cer' % key)
if not os.path.isfile('index.txt'):
    open('index.txt', 'w').close()
if not os.path.isfile('index.txt.attr'):
    with open('index.txt.attr', 'w') as f: print >>f, "unique_subject = yes"
if not os.path.isfile('serial'):
    with open('serial', 'w') as f: print >>f, "100000"
if not os.path.isdir('certs'): os.mkdir('certs')

if os.system('openssl rsa -in ca.pem -passin pass:%s -out /dev/null' % key) != 0:
    print 'CA password is incorrect'
    sys.exit(1)

# get ca domain
SUB = dict()
os.system('openssl x509 -in ca.cer -noout -subject >temp.txt')
with open('temp.txt') as f:
    for line in f:
        words = line.strip().split('/')
        for w in words:
            k,v = w.split('=')
            SUB[k] = v
            
if SUB["CN"].startswith('CA of '):
    domain = SUB["CN"][6:]
    isroot = True
else:    
    domain = '.'.join( SUB["CN"].split('.')[2:] )
    isroot = False
    

if not os.path.isdir('ca.cfg'): cpcfg()


# sign cawww request
files = []
if len(sys.argv) > 1: files.append(sys.argv[1] + '.csr')

if 'PASSWD' in os.environ:
    passwd = os.environ["PASSWD"]
else:
    passwd = domain

ret = 0
for x in files:
    os.system('openssl genrsa -out temp.pem -aes256 -passout pass:%s 2048' % passwd)
    print 'x=', x, 'domain=', domain
    names = ["%s.%s" % (x.replace('.csr', ''), domain)]
    sub = "/C=%s/ST=%s/O=%s/CN=%s" % (SUB["C"], SUB["ST"], SUB["O"], names[0])
    for i in range(2, len(sys.argv)):
        if '@' in sys.argv[i]:
            sub += '/emailAddress=' + sys.argv[i]
        else:
            names.append(sys.argv[i])
    with open('temp.cfg', 'w') as f: print >>f, "[req]\ndistinguished_name=san\n[san]\nsubjectAltName=DNS:" + ",DNS:".join(names)
    os.system('openssl req -new -config temp.cfg -reqexts san -key temp.pem -passin pass:%s -out temp.csr -subj "%s"' % (passwd, sub))
        
    
    # validating the CSR
    SAN = ['none']
    x509v3 = []
    SUB = dict()
    for line in subprocess.check_output('openssl req -in temp.csr -noout -text'.split()).split('\n'):
        if 'X509v3 Subject Alternative Name' in line:
            SAN = []
            continue
        if len(SAN) == 0:
            SAN = map(lambda c: c.strip().replace('DNS:', ''), line.strip().split(','))
            continue
        
        if 'X509v3' in line: 
            x509v3.append(line.strip())
        if 'Subject:' in line:
            for w in line.split(','):
                if '=' not in w: continue
                k,v = w.strip().split('=')
                SUB[k] = v

    if len(SAN) == 0:
        print >>sys.stderr, "Empty SAN %s" % x
        continue

    print 'SAN =', SAN
    
    ext = ""
    if ".root."+domain in SAN[0] or ".gateway."+domain in SAN[0] or ".node."+domain in SAN[0]: 
        ext = "thnet_ext"
        
    if ".client."+domain in SAN[0]:
        ext = "client_ext"
        
    if SAN[0] != SUB['CN'] or ext == "":
        print >>sys.stderr, "BAD SAN %s" % str(x)
        ret = 1
        continue
    
    if len(SAN)>1 or len(x509v3)>0:
        if isroot: continue
        print >>sys.stderr, 'delayed for main CA handling:', x
        os.system('mv temp.csr ../data/%s' % x)
        continue
        
    days = 366 if isroot else 11
    if os.system('openssl ca -config ca.cfg -name ca -key %s -days %s -batch -extensions %s -in temp.csr -out temp.cer' % (key, days, ext)) != 0:
        print >>sys.stderr, 'can not sign %s' % x
        ret = 2
        continue
    os.system("cat ca.cer >>temp.cer");
    
    xp = ['..', 'certs']
    xp.extend( xpath(x).split('/') )
    for i in range(1, len(xp)):
        try:
            os.mkdir('/'.join(xp[:i]))
        except:
            pass
    xp = '/'.join(xp)

    if not isroot: os.system('mv temp.csr ../data/%s' % x)
    os.system('mv temp.pem %s.pem' % xp)
    os.system('mv temp.cer %s.cer' % xp)
    print 'Signed for %d days.' % days
        
os.system('rm certs/*.pem 2>/dev/null')
print 'Done (%d) .' % ret

sys.exit(ret)

