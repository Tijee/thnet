'use strict';

const spawn = require('child_process').spawn;
const dgram = require('dgram');


var tests = [];
function test(tmout, param, startcb, exitcb, valgrind){
    tests.push([tmout, param, startcb, exitcb, valgrind]);
}

function checkstr(str, xx){
    var idx = 0;
    var ok = true;
    var idxs = [];
    for(var i=0; i<xx.length; i+=2){
        idx = str.indexOf(xx[i], idx);
        if(idx == -1)console.error(str.trim(), xx[i]);
        if(idx !== xx[i+1])ok = false;
        idxs.push(idx);
        idx += 1;
    }
    console.assert(ok, 'idx @ ', idxs, str);
}


test(2000, ['logger', JSON.stringify({kickidle:900})], function(app){
    app.sout = '';
    app.stdout.on("data", function(x){
        app.sout += x.toString('ascii');
    });
    app.serr = '';
    app.stderr.on("data", function(x){
        app.serr += x.toString('ascii');
    });
    app.stdin.write('zzz\n[test] aaa\nbbb\n');
    setTimeout(function(){ app.stdin.write('ddddd\n[test] ccc\n'); }, 1000);
}, function(app){
    console.assert(app.sout == '', 'out should be empty');
    checkstr(app.serr, ['aaa', 7, 'kickidle', 43, 'kickidle', 89, 'ccc', 116, 'kickidle', 152]);
    return true;
});

function thnet(param, cmds, valgrind){
    test(5000, ['thnet', JSON.stringify(param)], function(app, sigint){
        function send(target, type, param){
            app.stdin.write(`\x01${target}!${type}!zzz!` + JSON.stringify(param) + '\n');
        }
        app.cmds = cmds;
        app.ntype = 'start';
        var expout;
        app.next = function(){
            if(app.cmds.length == 0){
                sigint();
                return;
            }
            var cmd = app.cmds.shift();
            if(typeof cmd == 'string'){
                app.ntype = cmd;
                cmd = app.cmds.shift();
            }
            send('thctl', app.ntype, cmd);
            var str = app.cmds.shift();
            if(Array.isArray(str) || typeof str === 'function') expout = str;
            else expout = [app.ntype, 7, 'error', 14+app.ntype.length, str, 22+app.ntype.length];
        }
        app.stdout.on("data", function(x){
            x = x.toString('ascii').trim();
            if(expout === undefined){  // first is all!init!!
                var ex = "\x01all!init!!{}";
                console.assert(x === ex, "init string", x, ex);
            } else if(typeof expout === 'function'){
                var n = x.indexOf('{');
                var ss = x.split('!', 3);
                console.assert(n > 0 && ss.length == 3 && ss[0] == '\x02thctl' && ss[2] == 'zzz', 'ctrlmsg ' + x);
                if( expout(app, ss[1], JSON.parse(x.substr(n)) ) ) return;
            } else {
                checkstr(x, expout);
            }
            app.next();
        });
        app.stderr.on("data", function(x){
            console.error( x.toString('ascii').trim() );
        });
    }, function(app){
        console.assert(app.cmds.length === 0, 'starts not finished');
        return true;
    }, valgrind);
}

thnet({}, [
    {hid:33333}, "cfg.hid range [0, 32760]",
    {hid:0, encrypt:3}, "cfg.encrypt should be in range [0, 2]",
    {hid:0, mtu:2}, "cfg.mtu should be in range [1380, 1500]",
    {hid:0, mtu:1400}, "cfg should have interfaces member",
    {hid:0, interfaces:{}, prefix:"12"}, "cfg.prefix should be ipv6 address",
    {hid:0, interfaces:{}, prefix:"2002::1"}, "cfg.addr should be global IPv4 address",
    {hid:0, interfaces:{}, prefix:"2002::1", addr:'20.1.1.1'}, "need root privilege to create tun device",
]);

thnet({interfaces:{tun:'virtual', udp:11}}, [
    {hid:0, prefix:"2002::1", addr:'20.1.1.1'}, "udp bind failed 2",
    {hid:0, interfaces:{}, prefix:"2002::1", addr:'20.1.1.1'}, "udp bind failed 2",
]);


var action;
var actmsg;
var acthid;
var tun;
thnet({interfaces:{tun:'virtual', udp:3399}}, [
    {hid:0, prefix:"2002::1", addr:'20.1.1.1'}, function(app, type, ob){
        console.assert(type === 'start' && ob.action > 1000 && ob.tun > 1000, `ports message error ${type} ` + JSON.stringify(ob));
        action = dgram.createSocket('udp6');
        action.send('aa', ob.action, '::1');
        action.on('message', function(msg, rinfo){
            acthid = msg.readInt16BE(0);
            actmsg = JSON.parse( msg.toString('ascii', 2) );
        });
        
        tun = dgram.createSocket('udp6');
        tun.send('aa', ob.tun, '::1');
        tun.on('message', function(msg, rinfo){
            console.error('tun', rinfo, msg);
        });
    },
    'newclient',
    {hid:1, euid:'aaaaaaa'}, "euid should be 16-chars hex string",
    {hid:1, euid:'aaaabbbbccccdddd', lifetime:10}, "lifetime range [20, ROOT_TIMEOUT]",
    {hid:1, euid:'aaaabbbbccccdddd', lifetime:110}, function(app, type, ob){
        console.assert(type === 'newclient' && !ob.error, `message error ${type}`);
        setTimeout(function(){
            console.assert(acthid === 1 && actmsg.prefix === '2002:0:0:1::', 'action failed');
            action.close();
            tun.close();
            app.next();
        }, 300);
        return true;
    },
]);


function next(){
    if(tests.length == 0)return;
    var t = tests.shift();
    console.info('running "%s" for %d ms', t[1], t[0]);
    var exe = __dirname + '/../build/Debug/tjapps';
    var param = t[1];
    if(t[4]){
        param = ['--log-file=valgrind.txt', exe].concat(t[1]);
        exe = 'valgrind';
    }
    var app = spawn(exe, param);
    var t1;
    var t2;
    function sigint(){
        clearTimeout(t1);
        t1 = null;
        app.kill('SIGINT');
        console.error('-- SIGINT');
        t2 = setTimeout(function(){
            console.error('-- SIGTERM');
            t2 = null;
            app.kill();
        }, 500);
    }
    t[2](app, sigint);
    app.on('close', function(code){
        console.assert(t[3](app), "check output failed", code);
        console.assert(t1 === null && t2 !== null, "not ended by SIGINT");
        console.assert(code === 0, "return code != 0");
        clearTimeout(t2);
        setTimeout(next, 100);
    });
    t1 = setTimeout(sigint, t[0]);
}

next();

