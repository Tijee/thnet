#!/usr/bin/env node
/*
MIT License
Copyright (c) 2016 Tijee Corporation.

Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to use, 
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
Software, and to permit persons to whom the Software is furnished to do so, subject 
to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

'use strict';

const fs = require('fs');
const dns = require('dns');
const dgram = require('dgram');
const WebSocket = require('ws');
const https = require('https');

const util = require('util');
const tjapp = process.env['TJAPP'];
function logline(...args){
    function pad(num, len){
        len -= String(num).length - 1;
        if (len > 0) return new Array(len).join('0') + num;
        return num;
    }
    var now = new Date();
    var line = `[${this} ${pad(now.getDate(), 2)} ${pad(now.getHours(), 2)}:${pad(now.getMinutes(), 2)}:${pad(now.getSeconds(), 2)}.${pad(now.getMilliseconds(), 3)} ${tjapp}] ` + util.format.apply(null, args) + '\n';
    process.stderr.write(line);
}

function debug(...args){
    logline.apply('D', args);
}
function info(...args){
    logline.apply('I', args);
}
function warn(...args){
    logline.apply('W', args);
}
function error(...args){
    logline.apply('E', args);
}
function assert(b, ...args){
    if(!b){
        var err = new Error("assert failed " + util.format.apply(null, args));
        error(err.stack);
        process.exit(98);
    }
}

var ctrlid = 0;
var ctrlcb = {};
function CtrlSend(peer, type, param, cb, ch){
    var id = ctrlid;
    ctrlid += 1;
    if(!ch)ch = '\x01';
    if(id === 0){
        process.stdin.on('data', function(chunk) {
            var lines = chunk.toString('ascii').split('\n');
            lines.forEach(function(x){
                if(x.length < 4 || x.indexOf('!') < 0)return;
                var n = x.indexOf('{');
                var ss = x.substr(1).split('!', 3);
                var cb = ctrlcb[`cb${ss[2]}`];
                if(cb){
                    delete ctrlcb[`cb${ss[2]}`];
                    clearTimeout(ctrlcb[`timer${ss[2]}`]);
                } else {
                    cb = ctrlcb[`cb.${ss[0]}.${ss[1]}`];
                    if(!cb)return;
                }
                cb( JSON.parse(x.substr(n)), ss[0], ss[1], ss[2] );
            });
        });
    }
    process.stdout.write(`${ch}${peer}!${type}!${id}!` + JSON.stringify(param) + '\n');
    if(cb !== undefined){
        ctrlcb[`cb${id}`] = cb;
        ctrlcb[`timer${id}`] = setTimeout(function(){
            delete ctrlcb[`cb${id}`];
            cb(null, peer, type, id);
        }, 1000);
    }
}
function CtrlRecv(peer, type, cb){
    ctrlcb[`cb.${peer}.${type}`] = cb;
}
function CtrlReply(peer, type, param){
    CtrlSend(peer, type, param, null, '\x02');
}

function fixffff(addr) {
    // IPv4 address in IPv6 format
    if(addr.indexOf('::ffff') == 0)return addr.substring(7);
    return addr;
}

function LocalInfo(req, res) {
    var addr = fixffff( req.socket.remoteAddress );
    res.end(JSON.stringify({port: req.socket.remotePort, address: addr, CN: req.socket.getPeerCertificate().subject.CN}));
}

function SSLOption(sslpath, passphrase){
    //info("pass ", sslpath, passphrase);
    var options = { 
    	key: fs.readFileSync(sslpath + '.pem'),
        passphrase: passphrase
    };
    try {
        options.crl = fs.readFileSync(sslpath + ".crl");
    }
    catch(e){
    }
    var certs =[];
    var lines = fs.readFileSync(sslpath + '.cer').toString('ascii').split('\n');
    var b = -1;
    var e = 0;
    while(e < lines.length){
        var line = lines[e].trim();
        if(line === "-----BEGIN CERTIFICATE-----")b=e;
        if(line === "-----END CERTIFICATE-----" && b>=0){
            certs.push(lines.slice(b, e+1).join('\n'));
        }
        e++;
    }
    options.cert = certs.join('\n');
    options.ca = certs[certs.length-1];
    //info("cert", certs.length);
    return options;
}

function TLSServer(cfg){
    var options = SSLOption(cfg.sslpath, cfg.passphrase);
    options.requestCert = cfg.requestCert !== undefined ? cfg.requestCert : true;
    options.rejectUnauthorized = cfg.rejectUnauthorized !== undefined ? cfg.rejectUnauthorized : true;

    var cb = cfg.app;
    if(!cb){
        cb = function(req, res) {
            if(req.url == '/localinfo'){
                LocalInfo(req, res);
                return;
            }
            res.writeHead(200);
            res.end("Default !\n");
        }
    }
    var app;
    if(cfg.port){
        app = https.createServer(options, cb);
        app.listen(cfg.port);
    } else if(cfg.sslfd){
        app = https.createServer(options, cb);
        app.listen({fd:cfg.sslfd});
        assert( app.address(), `fd=${cfg.sslfd} should have been initialized as server socket` );
        cfg.port = app.address().port;
    } else {
        return null;
    }
    app.on('clientError', function(err, socket){
        error('clientError', err);
    });
    info(`TLS bind on port ${cfg.port}`);
    return app;
}

function CheckNAT(cfg, cb){
    var CN;
    var options;
    
    function ipify(){
        if(cb === null)return;
        
        options = {
            family: 4,
            hostname: 'api.ipify.org',
            port: 443,
            path: '/'
        };
        https.get(options, (res) => {
            res.on('data', function(d){
                if(cb === null)return;
                cb(CN, d.toString('ascii'), res.socket.localAddress);
                cb = null;
            });
        }).on('error', function(e){
            error('ipify', e);
        });
    }

    function localinfo(){
        var cns = CN.split('.');
        cns.shift();
        cns.shift();
        dns.resolve(cns.join('.'), "SRV", function(err, srv){
            var cnt = srv.length;
            
            srv.forEach(function(s){
                delete options.host;
                options.family = 4;
                options.hostname = s.name;
                options.port = s.port;
                options.method = 'GET';
                options.path = '/localinfo';
    
                var c = https.get(options, function(res) {
                    if(res.statusCode == 200){
                        res.setEncoding('utf8');
                        res.on('data', function(data) {
                            data = JSON.parse(data.toString('ascii'));
                            if(cb !== null){
                                cb(CN, data.address, res.socket.localAddress);
                                cb = null;
                            }
                        });
                    }
                    res.on('end', function(){
                        cnt -= 1;
                        if(cnt == 0)ipify();
                    });
                }).on('error', function(err){
                    //console.log('Error', err);
                    cnt -= 1;
                    if(cnt == 0)ipify();
                });
            });
        });
        setTimeout(ipify, 3000);
    }
    
    CtrlSend('tjapps', 'info', {}, function(i){
        assert(i, 'tjapps protocol error');
        cfg.name = i.name;
        cfg.euid = i.euid;
        if(!cfg.passphrase)cfg.passphrase = i.hwid;
        
        // read cert CN
        options = SSLOption(cfg.sslpath, cfg.passphrase);
        options.host = '::1';
        options.checkServerIdentity = function(servername, cert){
            CN = cert.subject.CN;
            delete options.checkServerIdentity;
            localinfo();
        };
        
        const tls = require('tls');
        var server = tls.createServer(options);
        server.listen(0, options.host, function(){
            options.port = server.address().port;
            var client = tls.connect(options, function(){
                client.end();
                client.unref();
                server.close();
                server.unref();
            });
            client.on('error', function(err){
                error('cget self cert', err);
            });
        }).on('clientError', function(err){
            error('get self cert', err);
        });
    });
}


function Report(cfg, msg){
    var options = SSLOption(cfg.sslpath, cfg.passphrase);
    
    var cns = cfg.CN.split('.');
    cns.shift();
    cns.shift();
    options.hostname = 'dns.root.' + cns.join('.');
    options.method = 'POST';
    options.path = '/dns';

    var txt = JSON.stringify(msg);
    options.headers = {
        'Content-Type': 'application/json',
        'Content-Length': Buffer.byteLength(txt),
        'User-Agent': 'thnet',
    };
    
    var c = https.request(options, function(res){});
    c.on('error', function(err){});
    c.write(txt);
    c.end();
}

function Start(options, recvcb, firstmsg) {
    var net = {
        send: function(hid, msg){
            var b;
            if(Buffer.isBuffer(msg)){
                b = Buffer.alloc(msg.length + 2);
                b.writeUInt16BE(hid, 0);
                msg.copy(b, 2);
            } else {
                b = Buffer.alloc(Buffer.byteLength(msg) + 2);
                b.writeUInt16BE(hid, 0);
                b.write(msg, 2);
            }
            this.sock.send(b, this.port, '::1');
        },
        
        close: function(){
            this.sock.close();
            this.sock.unref();
            this.sock = null;
        },
        
        recv: recvcb,
        
        open: function(port){
            this.port = port;
            this.sock = dgram.createSocket('udp6', function(msg, rinfo){
                if(port !== rinfo.port)return;
                var hid = msg.readUInt16BE(0);
                if( msg[2] == 0x7b ){ // '{'
                    msg = msg.toString('utf8', 2);
                } else {
                    msg = msg.slice(2);
                }
                net.recv(hid, msg);
            });
            if(typeof firstmsg === "function"){
                firstmsg(this);
            } else {
                if(!firstmsg) firstmsg = 'a';
                this.send(0, firstmsg);
            }
        }
    };
    
    CtrlSend('thnet', 'start', options, function(ob){
        if(!ob){ error("thnet start timeout"); return; }
        if(ob.error){ error("thnet start error:", ob.error); return; }
        net.open(ob.action);
    });
    return net;
}

function Root(tls, cfg){
    const url = require('url');
    const sqlite3 = require('sqlite3').verbose();

    var db = new sqlite3.Database('data/thnet.db');
    db.serialize();
    db.run("CREATE TABLE if not exists clients (hid INTEGER, euid TEXT, cn TEXT, local TEXT, a TEXT, aaaa TEXT, last INTEGER)", function(err){ if(err != null)error(err); });
    db.run("CREATE UNIQUE INDEX if not exists clientsHid ON clients (hid)");
    db.run("CREATE UNIQUE INDEX if not exists clientsUid ON clients (euid)");
    db.run("CREATE UNIQUE INDEX if not exists clientsCN ON clients (cn)");
    db.run("CREATE INDEX if not exists clientsLast ON clients (last)");

    var conns = [];  // hid -> conn mapping

    function wssend(hid, msg){
        if(process.env['WSDEBUG']) debug('wssend', hid, msg);
        if(conns[hid]) conns[hid].send(msg);
    }

    /////////////// DDNS
    var starttm = Date.now();
    var stats = [];
    function UpdateStatus(){
        var cnt = 0;
        for(var i = 0; i<conns.length; i++)if(conns[i])cnt += 1;
        cnt = (10 + (cnt / 20)) | 0;
        stats.push({'cn': cfg.CN, 'uptime': (Date.now() - starttm)/1000, 'srv': `${cnt}  10  ${cfg.port}`});
        Report(cfg, stats);
        stats = [];
    }
    setInterval(UpdateStatus, 60000);
    stats.push( {'cn': cfg.CN, 'a': cfg.addr, 'aaaa': cfg.aaaa } );
    db.each("SELECT cn,a,aaaa FROM clients", function(err, row) {
        if(err != null)return;
        stats.push( {'cn': row.cn, 'a': row.a, 'aaaa': row.aaaa } );
    });
    UpdateStatus();


    //////////////////// start 
    CtrlRecv('thnet', 'updateaddr', function(ob){
        info("updateaddr", JSON.stringify(ob));

        db.run("UPDATE clients SET last=?,a=?,aaaa=? WHERE hid=?", [Date.now(), ob.a, ob.aaaa, ob.hid], function(err){
            if(err == null || ob.hid != 0)return;
            // have no root information
            db.run("INSERT INTO clients VALUES(0, ?, ?, '', ?, ?, ?)", [cfg.euid, mycn, ob.a, ob.aaaa, Date.now()], function(err){});
        });

        if(conns[ob.hid])stats.push({'cn': conns[ob.hid].CN, 'a':ob.a, 'aaaa': ob.aaaa});
    });


    var options = { hid:0, aaaa:cfg.aaaa, prefix:cfg.prefix, dns:cfg.dns, mtu:cfg.mtu, addr:cfg.addr };
    var net = Start(options, wssend);
    CtrlRecv('thnet', 'init', function(ob){
        if(net)net.close();
        for(var hid in conns){ // close all TLS connections
            var conn = conns[hid];
            if(!conn || conn.euid === undefined)continue;
            conn.close();
        }
        conns = [];
        net = Start(options, wssend);
    });
    cfg.db = db; // export db to express
    cfg.hid = 0;

    function newclient(conn, cn, hid, query, insert){
        function result(err) {
            if(err != null){
                error(err);
                conn.close();
                return;
            }
            conns[hid] = conn;
            conn.hid = hid;
            conn.CN = cn;
            conn.euid = query.euid;
            conn.lifetime = query.local == query.addr ? 3600 : 30;
            conn.upgradeReq = null;
            stats.push({'cn': cn, 'online': true});
            
            setTimeout(function(){ 
                info("newclient", cn, hid, query.euid, query.local, query.addr);
                CtrlSend('thnet', 'newclient', {'hid':hid, 'euid':conn.euid, 'lifetime':conn.lifetime});
            }, 100);
            return;
        }

        if(insert){
            db.run("INSERT INTO clients VALUES(?, ?, ?, ?, '', '', ?)", [hid, query.euid, cn, query.local, Date.now()], result);
        } else {
            db.run("UPDATE clients SET local=?,last=? WHERE cn=?", [query.local, Date.now(), cn], result);
        }
    }

    function connection(conn){
        var u = url.parse(conn.upgradeReq.url, true);
        var cn = conn.upgradeReq.socket.getPeerCertificate().subject.CN;
        if(cn.indexOf('gateway') > 0 && u.pathname == "/thnet" && 'euid' in u.query && 'local' in u.query){
            u.query.addr = fixffff( conn.upgradeReq.socket.remoteAddress );
            conn.on('message', function (message) {
                if(process.env['WSDEBUG']) debug('wsrecv', conn.hid, message);
                net.send(conn.hid, message);
            });
            conn.on('close', function() {
                info(cn, 'closed');
                stats.push({'cn': cn, 'online': false});
                conns[conn.hid] = null;
            });
            conn.on('error', function(e) {
                error(cn, e.messsage);
                conn.close();
            });
            
            db.get("SELECT hid FROM clients WHERE euid=? AND cn=?", [u.query.euid, cn], function(err, row) {
                if(err != null){ error(err); conn.close(); return; }
                if(row != undefined){
                    newclient(conn, cn, row.hid, u.query, false);
                    return;
                }
                db.get("SELECT COUNT(*) as cnt FROM clients", function(err, row) {
                    if(err != null){ error(err); conn.close(); return; }
                    if(row.cnt > 32000){
                        db.get("SELECT hid FROM clients ORDER BY last LIMIT 1", function(err, row) {
                            if(err != null){ error(err); conn.close(); return; }
                            newclient(conn, cn, row.hid, u.query, true);
                        });
                    } else {
                        newclient(conn, cn, row.cnt+100, u.query, true);
                    }
                });
            });
            return;

        } else {
            error(`invalid request '${conn.upgradeReq.url}' from '${cn}'`);
        }

        conn.close();
    }


    var wss = new WebSocket.Server( { server: tls } );
    wss.on('connection', connection);
} // end of Root


function Gateway(cfg) {
    var options = SSLOption(cfg.sslpath, cfg.passphrase);
    options.rejectUnauthorized = true;
    
    function run(){
        var roots = cfg.root.slice(0);
        
        function next(){
            if(roots.length == 0){
                setTimeout(run, 60000);
                return;
            }
            var root = roots.pop();
            var retry = 0;
            var thnet = null;

            function current(retry){
                if(retry > 2*60*4){  // at least 2 hours
                    if(thnet !== null)thnet.close();
                    setTimeout(next, 60000);
                    return;
                }
                
                var url = 'wss://' + root + '/thnet?euid=' + cfg.euid + '&local=' + cfg.local;
                var ws = new WebSocket(url, options);
                
                function end(err){
                    if(ws === null)return;
                    ws.close();
                    ws = null;
                    if(err){
                        error('WSError', root, err);
                    } else {
                        info('WSClose', root);
                    }
                    if(thnet === null)retry = retry+1;
                    setTimeout(current, 15000);
                }
                // we should start over for correcting key if thnet restarts
                CtrlRecv('thnet', 'init', end);
                
                ws.on('error', function(err) { end(err); });
                ws.on('close', function() { end(null); });
                ws.on('open', function(){
                    if(thnet !== null) {
                        // delay stop. when internet is unaccessible, gateway still delievers Icmp6 RA
                        thnet.close();
                        thnet = null;
                    }
                });
                
                var wstimer;
                ws.on('message', function(msg, flags){
                    if(flags.binary)return;
                    try{
                        function wssend(hid, data){
                            if(process.env['WSDEBUG']) debug('wssend', hid, data);
                            if(ws !== null){
                                ws.send(data);
                                if(!wstimer) wstimer = setTimeout(function(){ end('ws timeout'); }, 5000);
                            }
                        }
                        
                        if(process.env['WSDEBUG']) debug('wsrecv', msg);
                        if(thnet === null){
                            var ncfg = JSON.parse(msg);
                            if(cfg.dns !== undefined) ncfg.dns = cfg.dns;
                            ncfg.addr = cfg.addr;
                            thnet = Start(ncfg, wssend, msg);
                        } else {
                            thnet.send(0, msg);
                            if(wstimer){
                                clearTimeout(wstimer);
                                wstimer = null;
                            }
                        }
                    }
                    catch(err){
                        end(err);
                    }
                });
            }
            current();
        };
        
        next();
    }
    
    if(cfg.root && cfg.root.length > 0){
        run();
    } else {
        var cns = cfg.CN.split('.');
        cns.shift();
        cns.shift();
        dns.resolve(cns.join('.'), "SRV", function(err, srv){
            cfg.root = [];
            srv.forEach(function(s){
                cfg.root.push(s.name + ":" + s.port);
            });
            run();
        });
    }
}

function Express(path, cb) {
    const express = require('express');
    const onFinished = require('on-finished');
    const cookieParser = require('cookie-parser');
    const bodyParser = require('body-parser');
    const ECT = require('ect');
    
    var app = express();
    app.set('view engine', 'ect');
    var ectRenderer = ECT({ watch: true, root: path + '/views', ext : '.ect' });
    app.engine('ect', ectRenderer.render);
    
    // http log
    app.use( function(req, res, next) {
        function getStatus() {  return res._header  ? String(res.statusCode) : undefined; }
        function getReferrer() { return req.headers['referer'] || req.headers['referrer']; }
        function getHttpVersion() {  return req.httpVersionMajor + '.' + req.httpVersionMinor; }
        function getUrl() { return req.originalUrl || req.url; }
        function getResponse(field) {  if (!res._header) { return undefined; }  var header = res.getHeader(field);  return Array.isArray(header) ? header.join(', ') : header; }
        function logRequest() {
            var remote = fixffff(req.socket.remoteAddress);
            info(remote, req.cn, "\"" + req.method, getUrl() + "\" ", getStatus(), getResponse("content-length"), "\"" + getReferrer() + "\"", req.headers['user-agent']);
        };
        var cn = req.socket.getPeerCertificate();
        if(!cn)
            req.cn = '-';
        else
            req.cn = cn.subject ? cn.subject.CN : '<none>';

        onFinished(res, logRequest);
        next();
    });
    
    app.use(bodyParser.json());    // for parsing application/json
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(cookieParser());
    app.use(express.static(path + '/public'));
    
    app.get('/localinfo', LocalInfo);

    cb(app, express);
    
    app.use(function(req, res, next) {
      var err = new Error('Not Found');
      err.status = 404;
      next(err);
    });
    
    // production error handler
    // no stacktraces leaked to user
    app.use(function(err, req, res, next) {
      error(err, err.stack);
      
      res.status(err.status || 500);
      res.render('error', {
        message: err.message,
        error: process.env['DEBUG'] ? err : {}
      });
    });
    return app;
}


function Main(cfg, cb) {
    assert(typeof tjapp === 'string', 'thnet.js should be run by tjapps');
    
    function run(type){
        var tls = TLSServer(cfg);
        if(cb)cb(tls);
        if(type != "root"){
            Gateway(cfg);
            return;
        }
        if(!tls)throw "Error: config should have port/sslfd defination";
        if(cfg.addr != cfg.local)throw "Error: root will NOT work behide a NAT firewall";
        
        var it;
        var retry = 0;
        function ipv6(){
            retry += 1;
            if(retry == 4){
                const exec = require('child_process').exec;
                var cmds = "echo 0 >/proc/sys/net/ipv6/conf/all/disable_ipv6;";
                cmds += "echo 1 >/proc/sys/net/ipv6/conf/all/forwarding;";
                cmds += "ipv6=`printf '2002:%02x%02x:%02x%02x::1' " + cfg.addr.split('.').join(' ') + "`;";
                cmds += "ifconfig sit0 up;";
                cmds += "ip -6 route del default via ::192.88.99.1;";
                cmds += "ip -6 address add dev sit0 $ipv6/16;";
                cmds += "ip -6 route add default via ::192.88.99.1 dev sit0;";
                exec(cmds, (error, stdout, stderr) => {
                    if (error) {
                        error(`exec error: ${error}`);
                        return;
                     }
                });
            }
            if(retry == 8){
                clearInterval(it);
                error("can NOT get ipv6 connection, so gateway will retry in 10 minutes");
                setTimeout(function() { Main(cfg); }, 600*1000);
                return;
            }
            var options = {
                family: 6,
                hostname: 'ipv6.google.com',
                port: 443,
                path: '/'
            };
            
            https.get(options, (res) => {
                res.on('data', function(d){
                    clearInterval(it);
                    if(tls === null)return;
                    
                    var os = require('os');
                    var ifs = os.networkInterfaces();
                    for (var ifname in ifs) if(ifs.hasOwnProperty(ifname)) {
                        ifs[ifname].forEach(function(addr){
                            if( addr.family != 'IPv6')return;
                            var ips = addr.address.split(':');
                            if( ips.length < 4 )return;
                            if(ips[0] == 'fe80' || ips[0] == '')return;
                            cfg.aaaa = res.socket.localAddress;
                            cfg.prefix = ips.slice(0, 3).join(':') + "::";
                            cfg.dns = [cfg.prefix + "1", "2001:4860:4860::8888"];
                            cfg.mtu = 1480;
                        });
                    }
                    Root(tls, cfg);
                    tls = null;
                });
            }).on('error', function(e){
                console.error('ipv6 google', e);
            });
        }
        it = setInterval(ipv6, 2000);
        ipv6();
    }

    CheckNAT(cfg, function(CN, addr, local){
        info('CheckNAT', CN, addr, local);
        
        cfg.CN = CN;
        cfg.addr = addr;
        cfg.local = local;
        
        var cns = CN.split('.');
        run(cns[1]);
    });
}


if( require.main === module ){
    Main({
        sslpath: process.env['SSLPATH'],
        passphrase: process.env['PASSPHRASE'],
        port: process.env['TLSPORT'],
    });
} else {
    exports.tjapp = tjapp;
    exports.debug = debug;
    exports.info = info;
    exports.warn = warn;
    exports.error = error;
    exports.assert = assert;
    exports.fixffff = fixffff;
    exports.CtrlSend = CtrlSend;
    exports.CtrlReply = CtrlReply;
    exports.CtrlRecv = CtrlRecv;
    exports.LocalInfo = LocalInfo;
    exports.SSLOption = SSLOption;
    exports.TLSServer = TLSServer;
    exports.CheckNAT = CheckNAT;
    exports.Root = Root;
    exports.Gateway = Gateway;
    exports.Report = Report;
    exports.Express = Express;
    exports.Main = Main;
}

