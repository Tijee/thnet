/*
 * MIT License
 * Copyright (c) 2016 Tijee Corporation.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of 
 * this software and associated documentation files (the "Software"), to deal in 
 * the Software without restriction, including without limitation the rights to use, 
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject 
 * to the following conditions:

 * The above copyright notice and this permission notice shall be included in all 
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *  
 * This file is part of the thnet project.
 */

#include "thnet.h"
#include <net/if.h>
#include <linux/if_link.h>
#include <linux/if_packet.h>
#include <ifaddrs.h>
#include <netdb.h>


namespace TS {
    static uint64 v;
    
    uint64 ns()
    {
        return v;
    }

    void update()
    {
        struct timespec ts;
        clock_gettime(CLOCK_BOOTTIME, &ts);
        v = ts.tv_sec * MILLISECOND(1000) + ts.tv_nsec;
        if(v == 0)v++;
    }
    
    uint16 tm(int seconds)
    {
        return (uint16)(v / MILLISECOND(5000) + seconds / 5);
    }
    
    bool expset(uint16& tt, int seconds)
    {
        seconds /= 5;
        if(seconds == 0)seconds = 1;
        if(seconds > 32000)seconds = 32000;
        if(seconds < -32000)seconds = -32000;
        uint16 t = tm(0);
        if((int16)(t - tt) >= seconds){
            tt = t;
            return true;
        } 
        return false;
    }
    
    bool expset(uint64& ts, int64 ns)
    {
        if( (int64)(ts - v) > ns ){
            ts = v;
            return true;
        }
        return false;
    }

    bool exp(uint16 tt, int seconds)
    {
        return expset(tt, seconds);
    }
    bool exp(uint64 ts, int64 ns)
    {
        return expset(ts, ns);
    }
}


bool GetGateway(int family, char* intf, NetAddress* next)
{
    FILE* f = fopen(family == AF_INET6 ? "/proc/net/ipv6_route" : "/proc/net/route", "r");
    if(f == NULL)return false;

    char line[200];
    vector<string> rt;
    unsigned m = 0x1000;
    
    while(fgets(line, sizeof(line), f)){
        tokenize(line, rt, " \t\r\n", true);
    
        //LOG_INFO("GW %d %d %s", family, rt.size(), line);
        if(family == AF_INET && rt.size() == 11){
            // Iface	Destination	Gateway 	Flags	RefCnt	Use	Metric	Mask		MTU	Window	IRTT            
            if(rt[1] == "00000000" && rt[7] == "00000000"){
                unsigned cm = atoi(rt[6].c_str());
                if(cm < m){
                    m = cm;
                    if(intf)strcpy(intf, rt[0].c_str());
                    if(next){
                        next->alen = 4;
                        sscanf(rt[2].c_str(), "%x", (int*)next->addr);
                    }
                }
            }
        }
        if(family == AF_INET6 && rt.size() == 10){
            // dest 0, destLen, src, srcLen, nextHop, Metric 5, ref, usage, flags, dev
            if(rt[0] == "00000000000000000000000000000000" && rt[1] == "00"){
                unsigned cm = 0;
                sscanf(rt[5].c_str(), "%x", &cm);
                if(cm < m){
                    m = cm;
                    if(intf) strcpy(intf, rt[9].c_str());
                    if(next){
                        next->alen = 16;
                        fromhex(rt[4].c_str(), next->addr);
                    }
                }
            }
        }
    }
    fclose(f);
    return m < 0x1000;
}



void show(cstr msg, const byte* data, size_t msz)
{
    char buf[200];
    char* t = buf;
    size_t i = 0;
    while(t + 4 < buf+sizeof(buf)){
        sprintf(t, "%02x ", data[i++]);
        if(i>=msz)break;
        t += 3;
    }
    LOG_DEBUG("%s %d [%s]", msg, msz, buf);
}


void rstrip(char *str)
{
    int i = strlen(str);
    while(i>0 && strchr(" \t\r\n", str[i-1]))i--;
    str[i]=0;
}



int ReadFile(cstr name, void* buf, unsigned sz)
{
    int h = open(name, O_RDONLY);
    if(h < 0)return -1;
    int ret = read(h, buf, sz);
    close(h);
    return ret;
}

int WriteFile(cstr name, const void* buf, unsigned sz)
{
    int h = open(name, O_WRONLY | O_CREAT, 0644);
    if(h < 0)return -1;
    int ret = write(h, buf, sz);
    close(h);
    return ret;
}


bool IFGetIP(cstr iface, vector<NetAddress>& addrs)
{
    struct ifaddrs *ifaddr, *ifa;
    
    if (getifaddrs(&ifaddr) == -1)return false;
    
    NetAddress na;
    for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {
        if(ifa->ifa_addr == NULL || strcmp(ifa->ifa_name, iface) != 0)continue;
        if(ifa->ifa_addr->sa_family == AF_INET){
            na.set(*(struct sockaddr_in*)ifa->ifa_addr);
            addrs.push_back(na);
        }
        if(ifa->ifa_addr->sa_family == AF_INET6){
            na.set(*(struct sockaddr_in6*)ifa->ifa_addr);
            addrs.push_back(na);
        }
    }
    freeifaddrs(ifaddr);
    
    return addrs.size() > 0;
}


bool IFSetIP(cstr iface, const vector<NetAddress>& addrs)
{
    bool v4 = false;
    bool v6 = false;
    for(auto i : addrs){
        if(i.alen == 4) v4 = true;
        if(i.alen == 16) v6 = true;
    }
    
    vector<string> cmds;
    char cmd[200];
    
    {
        int h = socket(AF_INET, SOCK_DGRAM, 0);    
        struct ifreq ifr;
        memset(&ifr, 0, sizeof(ifr));
        strncpy(ifr.ifr_name, iface, IFNAMSIZ);
        ioctl(h, SIOCGIFFLAGS, &ifr);
        close(h);
        
        if(!(ifr.ifr_flags & IFF_UP)){
            sprintf(cmd, "/sbin/ifconfig %s up", iface);
            cmds.push_back(cmd);
        }
    }
    
    struct ifaddrs *ifaddr, *ifa;
    if (getifaddrs(&ifaddr) == -1)return false;
    
    vector<bool> find(addrs.size());
    for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {
        if( ifa->ifa_addr == NULL || strcmp(ifa->ifa_name, iface) != 0)continue;
        
        NetAddress n;
        int alen = 0;
        const byte* pmask;
        if(ifa->ifa_addr->sa_family == AF_INET && v4){
            alen = 4;
            n.set((byte*)&((sockaddr_in*)ifa->ifa_addr)->sin_addr, alen);
            pmask = (byte*)&((sockaddr_in*)ifa->ifa_netmask)->sin_addr;
        }
        if(ifa->ifa_addr->sa_family == AF_INET6 && v6){
            alen = 16;
            n.set((byte*)&((sockaddr_in6*)ifa->ifa_addr)->sin6_addr, alen);
            pmask = (byte*)&((sockaddr_in6*)ifa->ifa_netmask)->sin6_addr;
            
            if(!memcmp(n.addr, "\xfe\x80", 2))continue;  // local link
        }
        if(!alen)continue;
        
        int netmask = 0;
        for(int i=0; i<alen; i++){
            for(int j=0; j<8; j++)if(pmask[i] & (1<<j))netmask++;
        }
        n.port = netmask;
        
        bool have = false;
        for(size_t i=0; i<addrs.size(); i++){
            if(addrs[i] == n){
                find[i] = true;
                have = true;
            }
        }
        if(have) continue;
        
        if(alen == 16){
            sprintf(cmd, "/sbin/ip -f inet6 address del dev %s %s/%d", iface, n.host().c_str(), n.port);
            cmds.push_back(cmd);
        }
    }
    freeifaddrs(ifaddr);
    
    for(size_t i=0; i<addrs.size(); i++) if(!find[i]) {
        if(addrs[i].alen == 16)
            sprintf(cmd, "/sbin/ip -f inet6 address add dev %s %s/%d", iface, addrs[i].host().c_str(), addrs[i].port);
        else
            sprintf(cmd, "/sbin/ip -f inet address add dev %s %s/%d", iface, addrs[i].host().c_str(), addrs[i].port);
        cmds.push_back(cmd);
    }
    
    for(auto i : cmds){
        LOG_INFO("cmd %s", i.c_str());
        ignore_result( system(i.c_str()) );
    }
    return true;
}

bool IFAddRoute(cstr iface, cstr target, cstr via)
{
    ostringstream os;
    os << "/sbin/ip -f inet6 route add " << target;
    if(via) os << " via " << via;
    os << " dev " << iface;
    
    string cmd(os.str());
    
    LOG_INFO("system %s", cmd.c_str());
    ignore_result( system(cmd.c_str()) );
    return true;
}

bool IFDelRoute(cstr iface, cstr target, cstr via)
{
    ostringstream os;
    os << "/sbin/ip -f inet6 route del " << target;
    if(via) os << " via " << via;
    os << " dev " << iface;
    
    string cmd(os.str());
    
    LOG_INFO("system %s", cmd.c_str());
    ignore_result( system(cmd.c_str()) );
    return true;
}

bool IFGetMac(cstr iface, byte mac[6])
{
    struct ifaddrs *ifaddr, *ifa;
    bool ret = false;
    if (getifaddrs(&ifaddr) == -1)return false;
    
    int64 rmac = 0;
    for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {
        if( ifa->ifa_addr == NULL || (iface != NULL && strcmp(ifa->ifa_name, iface) != 0) || ifa->ifa_addr->sa_family != AF_PACKET )continue;
        memcpy(&rmac, ((struct sockaddr_ll *)ifa->ifa_addr)->sll_addr, 6);
        if(iface == NULL){
            if(rmac == 0)continue;
            if(strstr(ifa->ifa_name, "veth"))continue;
            if(strstr(ifa->ifa_name, "docker"))continue;
            if(strstr(ifa->ifa_name, "br"))continue;
            if(strstr(ifa->ifa_name, "vmnet"))continue;
        }
        memcpy(mac, &rmac, 6);
        ret = true;
    }
    freeifaddrs(ifaddr);
    return ret;
}


bool GetEUID(byte euid[8])
{
    byte mac[6];
    if(! IFGetMac(NULL, mac) )return false;
    euid[0] = mac[0] | 0x20;
    euid[1] = mac[1];
    euid[2] = mac[2];
    euid[3] = 0xff;
    euid[4] = 0xfe;
    euid[5] = mac[3];
    euid[6] = mac[4];
    euid[7] = mac[5];
    return true;
}

string GetEUID()
{
    byte euid[8];
    if(!GetEUID(euid))return "<none>";
    return hex(euid, 8);
}

string hex(const byte*data, size_t sz)
{
    std::stringstream ss;
    char buf[6];
    while(sz>0){
        sprintf(buf, "%02x", *data);
        ss << buf;
        sz--;
        data++;
    }
    return ss.str();
}

size_t fromhex(cstr txt, byte* data)
{
    int v;
    size_t sz = 0;
    while(*txt){
        sscanf(txt, "%02x", &v);
        *data++=(byte)v;
        txt+=2;
        sz++;
    }
    return sz;
}


void NetAddress::get(ostream& os) const { 
    if(port == 0){
        os << host();
    } else if(alen == 16){
        os << "[" << host() << "]:" << port;
    } else {
        os << host() << ":" << port; 
    }
}

bool NetAddress::set(const byte* addr, byte w)
{
    if(w == 4 || w == 6){
        alen = 4;
        memcpy(this->addr, addr, alen);
        if(w == 6) port = (addr[4]<<8) + addr[5];
        return true;
    }
    if(w <= 16){
        alen = 16;
        memcpy(this->addr, addr, w);
        memset(this->addr+w, 0, alen-w);
        return true;
    }
    if(w == 18){
        alen = 16;
        port = (addr[16]<<8) + addr[17];
        memcpy(this->addr, addr, alen);
        return true;
    }
    return false;
}

bool NetAddress::set(const struct sockaddr* sa)
{
    if(sa->sa_family == AF_INET){
        port = htons( ((const struct sockaddr_in*)sa)->sin_port );
        return set( (const byte*)&((const struct sockaddr_in*)sa)->sin_addr, 4);
    }
    if(sa->sa_family == AF_INET6){
        port = htons( ((const struct sockaddr_in6*)sa)->sin6_port );
        return set( (const byte*)&((const struct sockaddr_in6*)sa)->sin6_addr, 16);
    }
    return false;
}

bool NetAddress::set(cstr url)
{
    alen = 0;
    if(!url || url[0] == 0)return false;

    if(!strncmp(url, "http:", 5))port = 80;
    else if(!strncmp(url, "https:", 6))port = 443;
    else if(!strncmp(url, "ftp:", 4))port = 21;
    else port = 0;
    
    // discard http://
    cstr b = strstr(url, "//");
    if(b) url = b+2;
    
    string host;
    //LOG_INFO("set1 %s %s", b, url);
    do {
        if(*url == '['){ // [x:x:x::]:3   ipv6
            b = ++url;
            while(*url && *url != ']')url++;
            host.assign(b, url);
            
            if(*url == ']')url++;
            if(*url != ':')break;
            b = url;
        } else { // 64:ff9b::192.168.1.1/   host part no '.' before ':', ipv6
            b = url;
            bool v6 = false;
            while(*url && *url != '.' && *url != '/'){
                if(*url == ':')v6 = true;
                url++;
            }
            if(*url != '.' || v6){
                host.assign(b, url);
                break;
            }
            // found one '.' before ':', ipv4, restart
            url = b;
        }
        //LOG_INFO("set2 %s %s", b, url);
        
        // x.x.x.x/ or x.x.x.x:y/
        while(*url && *url != ':' && *url != '/')url++;
        if(b != url) host.assign(b, url);
        if(*url != ':')break;
        b = ++url;
        while(*url && *url != '/')url++;

        //LOG_INFO("set3 %s %s", b, url);

        string sport(b, url);
        port = atoi(sport.c_str());
    } while(false);

    if(host.find(':') != string::npos){
        if( inet_pton(AF_INET6, host.c_str(), (struct in6_addr*)addr) != 0 ) {
            alen = 16;
            return true;
        }
    } else {
        if( inet_pton(AF_INET, host.c_str(), (struct in_addr*)addr) != 0 ) {
            alen = 4;
            return true;
        }
    }
#ifdef TIJEE
    return resolve(host.c_str());
#else
    return false;
#endif
}

string NetAddress::host() const 
{
    char buf[64];
    if(alen > 0){
        inet_ntop(alen == 16 ? AF_INET6 : AF_INET, addr, buf, sizeof(buf));
        return buf;
    }
    return "?";
}

bool NetAddress::get(struct sockaddr* sa, socklen_t& slen) const
{
    if(alen == sizeof(struct in_addr)){
        if(slen < sizeof(struct sockaddr_in))return false;
        slen = sizeof(struct sockaddr_in);
        ((struct sockaddr_in *)sa)->sin_family = AF_INET;
        ((struct sockaddr_in *)sa)->sin_port = htons(port);
        memcpy(&((struct sockaddr_in *)sa)->sin_addr, addr, alen);
        return true;
    }
    if(alen == sizeof(struct in6_addr)){
        if(slen < sizeof(struct sockaddr_in6))return false;
        slen = sizeof(struct sockaddr_in6);
        ((struct sockaddr_in6 *)sa)->sin6_family = AF_INET6;
        ((struct sockaddr_in6 *)sa)->sin6_port = htons(port);
        memcpy(&((struct sockaddr_in6 *)sa)->sin6_addr, addr, alen);
        return true;
    }
    return false;
}
    

struct sockaddr_in NetAddress::get4() const 
{ 
    struct sockaddr_in sin; 
    memset(&sin, 0, sizeof(sin)); 
    socklen_t slen = sizeof(sin); 
    get((struct sockaddr*)&sin, slen); 
    return sin; 
}

struct sockaddr_in6 NetAddress::get6() const 
{ 
    struct sockaddr_in6 sin; 
    memset(&sin, 0, sizeof(sin)); 
    socklen_t slen = sizeof(sin); 
    get((struct sockaddr*)&sin, slen); 
    return sin; 
}

byte NetAddress::get(byte* pb, byte w) const
{
    if(w <= alen){ 
        memcpy(pb, addr, w); 
        return w; 
    }
    if(w >= alen+2){
        memcpy(pb, addr, alen);
        pb[alen] = (byte)(port >> 8);
        pb[alen+1] = (byte)(port >> 0);
        return alen+2;
    }
    return 0;
}

bool NetAddress::adapt(const struct sockaddr* sa) const
{
    if(sa->sa_family == AF_INET){
        if(alen != 0){
            if(alen != sizeof(struct in_addr))return false;
            if(memcmp( &((struct sockaddr_in*)sa)->sin_addr, addr, alen) != 0)return false;
        }
        if(port && port != htons(((struct sockaddr_in*)sa)->sin_port))return false;
    } else
    if(sa->sa_family == AF_INET6){
        if(alen != 0){
            if(alen != sizeof(struct in6_addr))return false;
            if(memcmp( &((struct sockaddr_in6*)sa)->sin6_addr, addr, alen) != 0)return false;
        }
        if(port && port != htons(((struct sockaddr_in6*)sa)->sin6_port))return false;
    } else
        return false;
    return true;
}

bool NetAddress::isAny() const {
    uint32* pu = (uint32*)addr;
    if(alen == 0 || (alen==4 && pu[0] == 0))return true;
    if(alen == 16){
        return pu[0] == 0 && pu[1] == 0 && pu[2] == 0 && pu[3] == 0;
    }
    return false;
}


bool NetAddress::isLoopback() const {
    if(alen == 0 || (alen==4 && addr[0] == 0x7f))return true;
    if(alen == 16){
        uint32* pu = (uint32*)addr;
        return pu[0] == 0 && pu[1] == 0 && pu[2] == 0 && addr[15] == 1;
    }
    return false;
}

bool NetAddress::isLocal() const {
    return (alen == 4 && addr[0] == 169 && addr[1] == 254) || (alen==16 && addr[0] == 0xfe && (addr[1]&0xc0) == 0x80);
}

bool NetAddress::isMulticast() const {
    return (alen==4 && (addr[0]&0xc0) == 0xc0) || (alen==16 && addr[0] == 0xff);
}




int SingleApplication(cstr name, bool doset)
{
    cstr e = name;
    while(*e && *e != '/')e++;
    string pname(name, e);
    
    char fname[100];
    sprintf(fname, "/run/user/%d", getuid());
    if(access(fname, W_OK) != 0)strcpy(fname, "/var/run");
    sprintf(fname + strlen(fname), "/%s.pid", pname.c_str());
    
    int hf = -1;
    if(doset){
        for(int trycnt=0; trycnt<5; trycnt++){
            hf = open(fname, O_CREAT|O_RDWR, 0644);
            if(hf >= 0)break;
            usleep(100*1000);
        }
        ignore_result( lockf(hf, F_LOCK, 0) );
    } else {
        hf = open(fname, O_RDWR);
    }
    if(hf<0){
        if(!doset)return 0;
        
        LOG_ERROR("open %s failed", fname);
        abort();
    }
    
    char spid[16];

    int sz = read(hf, spid, sizeof(spid)-1);
    spid[sz] = 0;
    rstrip(spid);
    
    int pid = atoi(spid);
    
    if(pid != 0 && kill(pid, 0) == 0){
        close(hf);
        if(doset){
            LOG_INFO("%s already run as %d", pname.c_str(), pid);
            exit(0);
        }
        return pid;
    }
    if(doset){
        lseek(hf, 0, SEEK_SET);
        sprintf(spid, "%d", getpid());
        ignore_result( write(hf, spid, strlen(spid)) );
        ignore_result( ftruncate(hf, strlen(spid)) );
        
        lseek(hf, 0, SEEK_SET);
        ignore_result( lockf(hf, F_ULOCK, 0) );
    }
    close(hf);
    return 0;
}

void TJlog(char type, cstr fmt, ...)
{
    va_list v;
    va_start(v, fmt);

    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    struct tm* ptm = localtime(&ts.tv_sec);
    if(!ptm)return;

    char buf[1300];
    sprintf(buf, "[%c %02d %02d:%02d:%02d.%03d %s] ", type, ptm->tm_mday, ptm->tm_hour, ptm->tm_min, ptm->tm_sec, 
        (int)(ts.tv_nsec/MILLISECOND(1)), PollLoop::instance ? PollLoop::instance->appname : "none");

    size_t blen = strlen(buf);
    vsnprintf(buf+blen, sizeof(buf)-blen-2, fmt, v);
    buf[sizeof(buf)-2] = 0;

    blen = strlen(buf);
    buf[blen++] = '\n';
    buf[blen++] = 0;

    fputs(buf, stderr);
}


#include <openssl/sha.h>

string base64(const byte*data, size_t sz)
{
    cstr b64="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    char str[5];
    ostringstream os;
    
#define B(x, y) \
    (x)[0] = b64[ (y)[0] & 0x3f ]; \
    (x)[1] = b64[ (((y)[1]<<2) + ((y)[0]>>6)) & 0x3f ]; \
    (x)[2] = b64[ (((y)[2]<<4) + ((y)[1]>>4)) & 0x3f ]; \
    (x)[3] = b64[ (y)[2]>>2 ];
    
    str[4] = 0;
    while(sz >= 3){
        B(str, data);
        os << str;
        data += 3;
        sz -= 3;
    }
    byte td[3];
    if(sz == 2){
        td[0] = data[0];
        td[1] = data[1];
        td[2] = 0;
        B(str, td);
        str[3] = '=';
        os << str;
    } else if(sz == 1){
        td[0] = data[0];
        td[1] = 0;
        td[2] = 0;
        B(str, td);
        str[2] = '=';
        str[3] = '=';
        os << str;
    }
    return os.str();
}


string GetHWID()
{
    unsigned char hash[SHA256_DIGEST_LENGTH];
    SHA256_CTX sha256;
    SHA256_Init(&sha256);

    byte data[256];
    if(IFGetMac(NULL, data))SHA256_Update(&sha256, data, 6);

    string path = "/sys/block/sda/device/";
    int sz = ReadFile((path + "vendor").c_str(), data, sizeof(data));
    if(sz>0)SHA256_Update(&sha256, data, sz);
    sz = ReadFile((path + "model").c_str(), data, sizeof(data));
    if(sz>0)SHA256_Update(&sha256, data, sz);
    sz = ReadFile((path + "rev").c_str(), data, sizeof(data));
    if(sz>0)SHA256_Update(&sha256, data, sz);

    path = "/sys/block/mmcblk0/device/";
    sz = ReadFile((path + "serial").c_str(), data, sizeof(data));
    if(sz>0)SHA256_Update(&sha256, data, sz);
    sz = ReadFile((path + "hwrev").c_str(), data, sizeof(data));
    if(sz>0)SHA256_Update(&sha256, data, sz);
    sz = ReadFile((path + "name").c_str(), data, sizeof(data));
    if(sz>0)SHA256_Update(&sha256, data, sz);

    sz = ReadFile("/proc/cpuinfo", data, sizeof(data));
    if(sz>0){
        int a=0;
        int b=0;
        for(int i=0; i<sz; i++)if(data[i] == '\n'){
            b = a+1;
            a = i;
            if(!strncmp((char*)&data[b], "model name", 10)){
                SHA256_Update(&sha256, &data[b], a-b);
                break;
            }
        }
    }
    SHA256_Final(hash, &sha256);
    return base64(hash, 12);
}

