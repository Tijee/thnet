/*
 * MIT License
 * Copyright (c) 2016 Tijee Corporation.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of 
 * this software and associated documentation files (the "Software"), to deal in 
 * the Software without restriction, including without limitation the rights to use, 
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject 
 * to the following conditions:

 * The above copyright notice and this permission notice shall be included in all 
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *  
 * This file is part of the thnet project.
 */

#include "thnet.h"

#ifndef TIJEE
#define THVER "tjapps/0.3.0"
#else
extern cstr exeVersion;
#define THVER exeVersion
#endif

extern thnetApp apps[];

namespace thnet {

struct attrApp;
struct PollAppFd : PollFd {
    attrApp* app;
    PollAppFd(attrApp* app_, IPoll* p) : PollFd(p), app(app_) {}
};

struct attrApp: attrh<int, ATTR_NUMBER> {
    string name;
    int in;
    PollAppFd out;
    attrApp(const string& nm, IPoll* p): name(nm), in(-1), out(this, p) {
        t = 0;
    }
    
    ~attrApp(){
        if(in > 0)close(in);
    }
    
    void check(int pid, int st){
        if( kill(t, 0) != 0 ){
            if(t == pid){
                LOG_ERROR("proc %s %d exit status %d", name.c_str(), t, WEXITSTATUS(st)); 
            }
            close(in); in = -1;
            out.close();
            t = 0;
        }
    }
};

struct Sys : attrDict, IPoll, PollLoop {
    bool debug;
    Sys(char* a) : PollLoop(a), debug(false) {
    }
    
    void OnMessage(CtrlMsg& m, attrApp* sapp) {
        string err;
        if(m.data->get("error", err))return;  // other's response
        
        if(!strcmp(m.type, "info")){
            m.data->clear();
            m.data->bind("euid", GetEUID());
            m.data->bind("hwid", GetHWID());
            m.data->bind("version", THVER);
            
            string name = appname;
            get("name", name);
            m.data->bind("name", name);
            
            m.Send(sapp->in);
            return;
        }
        if(!strcmp(m.type, "restart") || !strcmp(m.type, "stop")){
            string app;
            if( m.data->get("app", app) ){
                auto i = find(app);
                if(i == end() || i->second->type != ATTR_DICT){
                    m.ReplyError("invalid app", sapp->in);
                    return;
                }
                auto a = static_pointer_cast<attrDict>(i->second);
                auto b = a->find("_app");
                if(b == a->end()){
                    m.ReplyError("invalid app", sapp->in);
                    return;
                }
                auto tapp = static_pointer_cast<attrApp>(b->second);
                kill(tapp->t, SIGINT);
                sleep(1);
                kill(tapp->t, SIGTERM);
                if(!strcmp(m.type, "stop")) erase(app);
                m.ReplyResult("ok", sapp->in);
                return;
            }
        }
        if(!strcmp(m.type, "start")){
            string app;
            shared_ptr< attrDict > cfg;
            if( m.data->get("app", app) && m.data->get("cfg", cfg) ){
                auto i = find(app);
                if(i != end()){
                    m.ReplyError("already start", sapp->in);
                    return;
                }
                (*this)[app] = cfg;
                m.ReplyResult("ok", sapp->in);
                return;
            }
        }
        m.ReplyError("invalid type", sapp->in);
    }
    
    void OnPoll(short evt, PollFd* fd) override {
        if(evt & POLLHUP){
            fd->close();
            return;
        }

        char bufin[1300];
        vector<char*> lines;
        if(!fd->readLines(bufin, sizeof(bufin), lines))return;

        auto sapp = ((PollAppFd*)fd)->app;

        for(auto line : lines){
            if(debug) LOG_DEBUG("-%s- %s", sapp->name.c_str(), line);
            CtrlMsg m(line);
            if(!m)continue;
            if(!strcmp(m.peer, "all")){
                m.id = "all";
                for(auto a = begin(); a != end(); a++){
                    if(a->second->type != ATTR_DICT)continue;
                    SendOne(m, sapp, static_pointer_cast<attrDict>(a->second));
                }
            } else {
                auto a = find(m.peer);
                if(a == end()){
                    if(!strcmp(m.peer, appname)){
                        OnMessage(m, sapp);
                        continue;
                    }
                    m.ReplyError("unknown peer", sapp->in);
                    continue;
                }
                SendOne(m, sapp, static_pointer_cast<attrDict>(a->second));
            }
        }
    }

    void SendOne(CtrlMsg& m, attrApp* sapp, shared_ptr<attrDict> appcfg){
        auto b = appcfg->find("_app");
        if(b == appcfg->end()){
            m.ReplyError("peer stopped", sapp->in);
            return;
        }
        auto tapp = static_pointer_cast<attrApp>(b->second);
        m.peer = sapp->name.c_str();
        m.Send(tapp->in, m.isreply ? 2 : 1);
    }
    
    void openfd(cstr nm, int fd){
        int sock = -1;
        NetAddress na(&nm[4]);
        int af = na.alen == 4 ? AF_INET : AF_INET6;
        if(!strncmp(nm, "tcp/", 4))sock = socket(af, SOCK_STREAM, 0);
        if(!strncmp(nm, "udp/", 4))sock = socket(af, SOCK_DGRAM, 0);
        if(sock<0)return;
        
        int one = 1;
        setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one));
        
        sockaddr_in6 sin;
        socklen_t slen = sizeof(sin);
        na.get((sockaddr*)&sin, slen);
        if(::bind(sock, (sockaddr*)&sin, slen) != 0){
            LOG_INFO("bind error %s slen=%d alen=%d port=%d", nm, slen, na.alen, na.port);
        }
        if(sock != fd){
            dup2(sock, fd);
            close(sock);
        }
        LOG_INFO("fd %s (%s) -> %d", nm, na.str().c_str(), fd);
    }
    
    void psplit(char* p, vector<char*>& vp){
        char* pb = p;
        bool inquote = false;
        while(true){
            if( (!inquote && (*p == 0 || *p == ' ' || *p == '\t' || *p == ';')) || (inquote && (*p == '\'' || *p == '"'))){
                if(p != pb || inquote)vp.push_back(pb);
                if(*p == 0)break;
                inquote = false;
                *p = 0;
                pb = ++p;
                continue;
            }
            if(*p == '\'' || *p == '"'){
                inquote = true;
                pb = ++p;
                continue;
            }
            p++;
        }
        vp.push_back(NULL);
    }

    void doexec(shared_ptr<attrDict> cfg) {
        string exe;
        
        if( !cfg->get("exec", exe) ){
            LOG_ERROR("need exec config for external program");
            return;
        }
        LOG_INFO("exec=%s", exe.c_str());
        
        vector<char*> argv;
        psplit((char*)exe.c_str(), argv);  // dont worry, exec will discard the memory.
        
        char ebuf[512];
        char* pebuf = ebuf;
        shared_ptr<attrDict> dict;
        if( cfg->get("env", dict) ){
            for(auto i : *dict){
                string v;
                if( dict->get(i.first, v)){
                    snprintf(pebuf, sizeof(ebuf) - (pebuf - ebuf), "%s=%s;", i.first.c_str(), v.c_str());
                    pebuf += strlen(pebuf);
                }
            }
        }
        snprintf(pebuf, sizeof(ebuf) - (pebuf - ebuf), "TJAPP=%s", appname);
        
        vector<char*> env;
        psplit(ebuf, env);
        
        execve(argv[0], &argv[0], &env[0]);
        LOG_INFO("execve errno=%d", errno);
    }

    void check(cstr nm, thnetApp*papp, shared_ptr<attrDict> cfg) {
        if(cfg->find("_app") == cfg->end()) cfg->bind("_app", new attrApp(nm, this));
        auto app = static_pointer_cast<attrApp>(cfg->find("_app")->second);
        if(app->t != 0)return;

        int fin[2], fout[2];
        if(pipe(fin) || pipe(fout)){
            LOG_ERROR("pipe() error");
            return;
        }
        app->t = fork();
        if(app->t == 0){
            // clear() will discard all names & configs, so we need rebase appname to make app happy
            memset(argv0, 0, 80);
            sprintf(argv0, "#%s", nm);
            appname = &argv0[1];
            clear();
            
            dup2(fin[0], 0);
            dup2(fout[1], 1);
            for(int i=3; i<64; i++)close(i);
            LOG_INFO("app start pid=%d", getpid());
            
            shared_ptr<attrDict> dict;
            if( cfg->get("fd", dict) ){
                for(auto i : *dict){
                    int fd;
                    if( dict->get(i.first, fd) && fd > 2) 
                        openfd(i.first.c_str(), fd);
                }
            }
            string cwd;
            if( cfg->get("chroot", cwd) && chroot(cwd.c_str()) != 0){
                LOG_ERROR("chroot to %s failed", cwd.c_str());
                return;
            }
            
            int uid = getuid();
            int gid = getgid();
            if( cfg->get("gid", gid) ) ignore_result( setgid(gid) );
            if( cfg->get("uid", uid) ) {
                ignore_result( setuid(uid) );
                ignore_result( seteuid(uid) );
            }
            if( cfg->get("cwd", cwd) && chdir(cwd.c_str()) != 0){
                LOG_ERROR("set cwd to %s failed", cwd.c_str());
                return;
            }
            
            if(papp)papp->entry(cfg);
            else doexec(cfg);
            exit(99);
        }
        app->in = fin[1]; close(fin[0]);
        app->out.setfd(fout[0]); close(fout[1]);
        if(papp == &apps[0])dup2(fin[1], 2); // enable tjapps' LOG_XXXX
    }
    
    void checkapp(int pid, int st)
    {
        for(auto c = begin(); c != end(); c++){
            if(c->second->type != ATTR_DICT)continue;
            auto cfg = static_pointer_cast<attrDict>(c->second);
            if(cfg->find("_app") == cfg->end())continue;
            auto app = static_pointer_cast<attrApp>(cfg->find("_app")->second);
            if(pid < 0 && app->t) 
                kill(app->t, SIGINT);
            else 
                app->check(pid, st);
        }
    }

    void run(){
        memset(argv0, 0, 80);
        sprintf(argv0, "#%s", appname);
        
        get("debug", debug);
        while(running){
            for(int i=0; apps[i].name; i++){
                shared_ptr<attrDict> cfg;
                if(!get(apps[i].name, cfg))continue;
                check(apps[i].name, &apps[i], cfg);
            }
            for(auto c = begin(); c != end(); c++){
                if(c->second->type != ATTR_DICT)continue;
                check(c->first.c_str(), NULL, static_pointer_cast<attrDict>(c->second));
            }
            int st = 0;
            int ret = waitpid(-1, &st, WNOHANG);
            if(ret > 0)checkapp(ret, st);
            Run(NULL, 1000);
        }
        checkapp(-1, 0);
    }
};

} // namespace thnet

using namespace thnet;

int main(int argc, char* argv[])
{
    if(argc<2){
        printf("Usage:\n\ttjapps [appname] [config|configfile] [-configname] [+configname=configvalue]\n");
        return 1;
    }
    if(!strcmp(argv[1], "-v")){
        printf("%s, Copyright (c) 2016 Tijee Corporation.\n", THVER);
        printf("EUID=%s\n", GetEUID().c_str());
        if(geteuid() == 0)printf("HWID=%s\n", GetHWID().c_str()); // dont show it when normal user run 
        printf("APPS=");
        for(int i=0; apps[i].name; i++)printf("%s ", apps[i].name);
        printf("\n");
        return 0;
    }
    
    shared_ptr<Sys> sys(new Sys(argv[0]));
    thnetApp* app = NULL;
    string err;
    
    int aidx = 0;
    while(++aidx < argc){
        if(!app){
            for(int i=0; apps[i].name; i++)if(!strcmp(argv[aidx], apps[i].name)){
                app = &apps[i];
                break;
            }
            if(app)continue;
        }
        if(argv[aidx][0] == '-'){
            vector<string> nms;
            tokenize(&argv[aidx][1], nms, ".", true);
            
            shared_ptr<attrDict> dict = sys;
            unsigned idx = 0;
            for(auto& nm : nms){
                idx++;
                auto d = dict->find(nm);
                if(d == dict->end()){
                    fprintf(stderr, "cannot find configname \"%s\" in argv \"%s\"\n", nm.c_str(), argv[aidx]);
                    return 3;
                }
                if(idx == nms.size()){
                    ((DictIattr*)dict.get())->erase(d);
                    break;
                }
                if(d->second->type != ATTR_DICT){
                    fprintf(stderr, "configname \"%s\" is not dict type\n", nm.c_str());
                    return 3;
                }
                dict = static_pointer_cast<attrDict>(d->second);
            }
        } else if(argv[aidx][0] == '+'){
            cstr pv = strchr(argv[aidx], '=');
            if(pv == NULL){
                fprintf(stderr, "cannot find \"=\" in argv \"%s\"\n", argv[aidx]);
                return 3;
            }
            vector<string> nms;
            err.assign((cstr)&argv[aidx][1], pv++);
            tokenize(err, nms, ".", true);
            
            shared_ptr<attrDict> dict = sys;
            unsigned idx = 0;
            for(auto& nm : nms){
                idx++;
                if(idx == nms.size())break;
                auto d = dict->find(nm);
                if(d == dict->end()){
                    fprintf(stderr, "cannot find configname \"%s\" in argv \"%s\"\n", nm.c_str(), argv[aidx]);
                    return 3;
                }
                if(d->second->type != ATTR_DICT){
                    fprintf(stderr, "configname \"%s\" is not dict type\n", nm.c_str());
                    return 3;
                }
                dict = static_pointer_cast<attrDict>(d->second);
            }
            err = nms[nms.size()-1];
            if(pv[0] >= '0' && pv[0] <= '9')
                dict->bind(err.c_str(), atoi(pv));
            else if(!strcmp(pv, "true"))
                dict->bind(err.c_str(), true);
            else if(!strcmp(pv, "false"))
                dict->bind(err.c_str(), false);
            else
                dict->bind(err.c_str(), pv);
                
        } else if(argv[aidx][0] == '{'){
            if(!sys->set(argv[aidx], err)){
                fprintf(stderr, "invalid config \"%s\", error=\"%s\"\n", argv[aidx], err.c_str());
                return 3;
            }
        } else {
            char buf[1200];
            int sz = ReadFile(argv[aidx], buf, sizeof(buf)-1);
            if(sz < 0){
                fprintf(stderr, "opening \"%s\" failed\n", argv[aidx]);
                return 3;
            }
            buf[sz] = 0;
            if(!sys->set(buf, err)){
                fprintf(stderr, "invalid configfile \"%s\", error=\"%s\"\n", argv[aidx], err.c_str());
                return 3;
            }
        }
    }
    if(sys->get("name", err)) SingleApplication(err.c_str(), true);
    
    if(app){
        sys->appname = app->name;
        app->entry(sys);
    } else {
        // logger
        if(sys->find(apps[0].name) == sys->end())
            sys->bind(apps[0].name, new attrDict());

        LOG_INFO("start");
        sys->run();
        LOG_INFO("stop");
    }
    return 0;
}

