/*
 * MIT License
 * Copyright (c) 2016 Tijee Corporation.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of 
 * this software and associated documentation files (the "Software"), to deal in 
 * the Software without restriction, including without limitation the rights to use, 
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject 
 * to the following conditions:

 * The above copyright notice and this permission notice shall be included in all 
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *  
 * This file is part of the thnet project.
 */


#ifndef _thnet__h
#define _thnet__h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <vector>
#include <map>
#include <set>
#include <string>
#include <queue>
#include <list>
#include <memory>
#include <exception>
#include <stdexcept>
#include <iostream>
#include <algorithm>
#include <sstream>
#include <fstream>
using namespace std;

#include "thutil.h"
#include "thattr.h"
#include "thpoll.h"

inline ostream& operator<<(ostream& os, const NetAddress& a) { a.get(os); return os; }
inline istream& operator>>(istream& is, NetAddress& a) { string c; is >> c; if(!a.set(c)) is.setstate(ios_base::failbit); return is; }

// default c++ lib treats byte as char !!!
inline ostream& operator<<(ostream& os, const byte& a) { return os << (int)a; }
inline istream& operator>>(istream& is, byte& a) { int ia; is >> ia; a = (byte)ia; return is; }



typedef void (*thnetAppEntry)(shared_ptr<attrDict> cfg);
struct thnetApp {
    cstr name;
    thnetAppEntry entry;
};


#endif // _thnet__h

