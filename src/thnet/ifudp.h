/*
 * MIT License
 * Copyright (c) 2016 Tijee Corporation.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of 
 * this software and associated documentation files (the "Software"), to deal in 
 * the Software without restriction, including without limitation the rights to use, 
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject 
 * to the following conditions:

 * The above copyright notice and this permission notice shall be included in all 
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *  
 * This file is part of the thnet project.
 */

#include "thnet.h"


struct UdpHandler : IFHandler {
    UdpHandler(HomeNets*hn, cstr name, shared_ptr<IFIO> io): IFHandler(hn, name, io) 
    {
#ifdef TIJEE
        void RTPRelayIO(shared_ptr<PollFd> io);
        RTPRelayIO(io);
#endif
    }

    void OnRecv(const NetAddress& src, byte data[], size_t dsz)
    {
        if(dsz < sizeof(LLPkt))return;
        LLPkt* pkt = (LLPkt*)data;

        //show("udp", data, dsz);
        uint16 hid = htons(pkt->hid);
        if((hid&HID_BACKWARD) && hn->self.hid == 0){ 
            // root never create own LL
#ifdef TIJEE
            void RTPRelay(const NetAddress& src, byte data[], size_t dsz);
            RTPRelay(src, data, dsz);
#endif
            return;
        }
        HomeNet* h = hn->Get(hid&HID_UNKNOWN);
        if(h == NULL || !h->crypt(pkt, dsz)){
            // FIXME DDOS
            return;
        }
        if((hid&HID_BACKWARD) == 0 && h == &hn->self){
            LOG_WARN("%s send forward packet to me ", src.str().c_str());
            return;
        }
        if((hid&HID_BACKWARD) > 0 && h != &hn->self){
            LOG_WARN("%s send %d's backward packet to me ", src.str().c_str(), h->hid);
            return;
        }
        hn->AddrUpdate(h, src.get4(), (hid&HID_BACKWARD)>0);
        hn->SendQueuedPkts(h);
        if(pkt->llid == LLID_NONE)return;  // a PING packet
        hn->WriteTun(h, pkt, dsz);
    }
};


struct UdpIO : IFIO, IPoll {
    UdpIO(cstr value) : IFIO(this) {
        int h = socket(AF_INET, SOCK_DGRAM, 0);
     
        int one = 1;
        setsockopt(h, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one));
        
        struct sockaddr_in sin;
        sin.sin_family = AF_INET;
        sin.sin_port = htons(atoi(value));
        sin.sin_addr.s_addr = 0;
        if(bind(h, (struct sockaddr*)&sin, sizeof(sin)) < 0){
            ::close(h);
            return;
        }
        setfd(h);
        
        socklen_t slen = sizeof(sin);
        getsockname(h, (struct sockaddr*)&sin, &slen);
        
        ostringstream os;
        os << htons(sin.sin_port);
        desc = os.str();
        
        ioctl(h, FIONBIO, &one);
    }
    void OnPoll(short evt, PollFd*){
        if(!(evt & POLLIN))return;
        
        struct sockaddr_in sin;
        socklen_t slen = sizeof(sin);
        byte data[1500];
        int dsz = recvfrom(fd(), data, sizeof(data), 0, (struct sockaddr*)&sin, &slen);
        if(dsz >= 0)ifh->OnRecv(NetAddress(sin), data, (size_t)dsz);
    }
    ssize_t Send(const NetAddress& target, const byte* pkt, size_t sz) override {
        struct sockaddr_in sin = target.get4();
        //show("sendudp", pkt, sz);
        return sendto(fd(), pkt, sz, 0, (struct sockaddr*)&sin, sizeof(sin));
    }
};

