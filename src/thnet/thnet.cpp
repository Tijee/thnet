/*
 * MIT License
 * Copyright (c) 2016 Tijee Corporation.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of 
 * this software and associated documentation files (the "Software"), to deal in 
 * the Software without restriction, including without limitation the rights to use, 
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject 
 * to the following conditions:

 * The above copyright notice and this permission notice shall be included in all 
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *  
 * This file is part of the thnet project.
 */

#include "thnet.h"


attrDictHost::attrDictHost(HomeNet* h, bool bkey_): hid(h->hid), address(h->sin), lifetime(h->lifetime), bkey(bkey_)
{
    if(bkey)memcpy(key, h->key, sizeof(key));
    memcpy(euid, &h->euid, sizeof(euid));
    bindall();
}



void HomeNets::WSRecvHost(const attrDictHost& dh)
{
    if(dh.hid == self.hid)return;
    
    //LOG_INFO("WSRecvHost %d %s", dh.hid, dh.address.str().c_str());
    
    struct sockaddr_in sin = dh.address.get4();
    sins[ sinkey(sin) ] = dh.hid;
    
    HomeNet* h = Get(dh.hid, true);
    bool changed = sinkey(h->sin) != sinkey(sin);
    h->sin = sin;
    h->sintm = h->tm = TS::tm();
    h->lifetime = dh.lifetime;
    memcpy(&h->euid, dh.euid, sizeof(dh.euid));
    if(dh.bkey)memcpy(h->key, dh.key, sizeof(h->key));
    if(changed)OnAddrUpdated(h);
    
    // send data to host
    if(!SendQueuedPkts(h)){  // PING
        LLPkt pkt;
        pkt.reset();
        pkt.hid = self.hid;
        pkt.llid = LLID_NONE;
        
        self.crypt(&pkt, sizeof(pkt));
        SendUdp(sin, &pkt, sizeof(pkt));
    }
}

void HomeNets::WSRecvLL(uint16 hid, const LLEntry& e)
{
    HomeNet* h = Get(hid);
    //LOG_INFO("WSRecvLL %d %d %p", hid, e.llid, h);
    if(!h)return;
    h->mi[e.llid] = LLData(e);
    h->ma[e] = e.llid;
}

void HomeNets::WSRecvLLAddr(const LLAddr& addr, bool good)
{
    if(!good){
        //FIXME DDOS
        return;
    }

    int64 e;
    memcpy(&e, addr.local, sizeof(e));
    if(!IsLocalServer(e))return;

    IPv6Pkt pkt;
    memcpy(pkt.src, prefix, sizeof(prefix));
    memcpy(pkt.src+sizeof(prefix), addr.local, sizeof(addr.local));
    memcpy(pkt.dest, addr.remote, sizeof(addr.remote));
    
    //LOG_INFO("LLADDR %s->%s", NetAddress(addr.local, 8).host().c_str(), NetAddress(addr.remote, 16).host().c_str());

    LLResult p;
    GetLL(&pkt, p);
}

void HomeNets::WSRecvAction(uint16 hid, const byte* msg, size_t sz)
{
    ASSERT(self.hid == 0 && hid != 0);
    HomeNet* h = Get(hid);
    if(!h){
        LOG_WARN("WSRecvAction unknown hid=%d", hid);
        return;
    }

    switch(*msg){
    case ACTION_NEWKEY:
        h->keyVer = msg[1];
    	h->keytm = TS::tm();
        random(h->keyVer ? h->key : h->key+AES_KEY_LEN, AES_KEY_LEN);
        LOG_INFO("gateway hid=%d changekey %d", hid, h->keyVer);

        h->attr.bkey = true;
        memcpy(h->attr.key, h->key, sizeof(h->key));
        QueueAttr(hid);
        
        if(TS::exp(h->sintm, h->lifetime)){
            h->attr.hosts->insert( shared_ptr<attrDictHost>(new attrDictHost(&self, false)) );
        } else {
            set<uint16> ids;
            for(auto p : h->mi){
                if(memcmp(p.second.remote, prefix, 6) != 0)continue;
                ids.insert( (p.second.remote[6]<<8) + p.second.remote[7] );
            }
            for(auto id : ids){
                if(id == 0)continue;
                HomeNet* hh = Get(id);
                if(hh == NULL)continue;
                hh->attr.hosts->insert( new attrDictHost(h, true) );
                QueueAttr(hh->hid);
            }
        }
        break;

    case ACTION_LL_NEW:
        for(byte idx=0; idx<msg[1]; idx++){
            LLEntry* pe = ((LLEntry*)&msg[2]) + idx;
            pe->bsw();
            h->mi[ pe->llid ] = LLData( *pe );
            h->ma[ *pe ] = pe->llid;

            HomeNet* hh = &self;
            if(memcmp(pe->remote, prefix, 6) == 0){
                hid = (pe->remote[6]<<8) + pe->remote[7];
                hh = Get(hid);
                if(hh == NULL)continue;
            }
            
            if(!TS::exp(hh->sintm, hh->lifetime)){
                h->attr.hosts->insert( new attrDictHost(hh, hh->hid != 0) );
                QueueAttr(h->hid);
            } else {
                hh->attr.hosts->insert(new attrDictHost(&self, false));
                QueueAttr(hh->hid);
            }
            if(hh == &self)continue;

            hh->attr.lls->insert( new attrDictLLEntry(h->hid, *pe) );
            hh->attr.hosts->insert( new attrDictHost(h, true) );
            QueueAttr(hh->hid);
        }
        break;

    case ACTION_LL_ID:
        for(byte idx=0; idx<msg[1]; idx++){
            LLID* pe = ((LLID*)&msg[2]) + idx;
            pe->bsw();
            HomeNet* hh = Get(pe->hid);
            if(!hh)continue;
            auto p = hh->mi.find(pe->llid);
            if(p == hh->mi.end())continue;
            
            h->attr.lls->insert(new attrDictLLEntry(hh->hid, LLEntry(pe->llid, p->second)));
            QueueAttr(h->hid);
            
            if(!TS::exp(hh->sintm, hh->lifetime)){
                hh->attr.hosts->insert(new attrDictHost(&self, false));
                QueueAttr(hh->hid);
            } else {
                h->attr.hosts->insert(new attrDictHost(hh, true));
            }
        }
        break;

    case ACTION_HOST:
        for(byte idx=0; idx<msg[1]; idx++){
            hid = htons(((uint16*)&msg[2])[idx]);
            HomeNet* hh = Get(hid & HID_UNKNOWN);
            if(hh == NULL)continue;
            
            if(!TS::exp(hh->sintm, hh->lifetime)){
                h->attr.hosts->insert( new attrDictHost(hh, hh->hid != 0) );
                QueueAttr(h->hid);
            } else {
                hh->attr.hosts->insert(new attrDictHost(&self, false));
                QueueAttr(hh->hid);
            }

            if(hh == &self)continue;

            if(!TS::exp(h->sintm, h->lifetime)){
                hh->attr.hosts->insert( new attrDictHost(h, h->hid != 0) );
                QueueAttr(hh->hid);
            } else {
                h->attr.hosts->insert(new attrDictHost(&self, false));
                QueueAttr(h->hid);
            }
        }
        break;
    }
}

HomeNet* HomeNets::Get(uint16 hid, bool cannew)
{
    if(hid == HID_UNKNOWN)return NULL;
    if(hid == self.hid)return &self;
    auto p = hnets.find(hid);
    if(p == hnets.end()){
        if(!cannew)return NULL;
        
        hnets[hid] = HomeNet();
        HomeNet* h = &hnets[hid];
        h->hid = hid;
        return h;
    }
    return &p->second;
}


bool HomeNets::GetLL(const IPv6Pkt* pkt, LLResult& p)
{
    static HomeNet* fake = (HomeNet*)4;   // just not NULL
    
    p.isnew = false;
    p.h = NULL;

    // remote or root/64 -> local/48
    if(!memcmp(pkt->dest, prefix, 6)){
        p.hid = (pkt->dest[6]<<8) + pkt->dest[7];
        p.h = Get(p.hid);
        if(!p.h) {
            if(self.hid == 0)return false;
            // we use fake because the 'local/64 -> remote' part assumes p.h != NULL which comes from here
            p.h = fake;
        } else {
            memcpy(p.local, &pkt->dest[sizeof(prefix)], sizeof(prefix));
            memcpy(p.remote, pkt->src, sizeof(p.remote));

            auto i = p.h->ma.find(p);
            if(i != p.h->ma.end()){
                 p.hid |= HID_BACKWARD;
                 p.llid = i->second;
                 return true;
            }
            if(self.hid == 0){
                // packet from outside/root wants to connect inside
                // FIXME DDOS
                p.h->attr.newlls->insert( shared_ptr<attrDictLLAddr>(new attrDictLLAddr(p)) );
                QueueAttr(p.hid);
                return false;
            }
         }
    }

    // local/64 -> remote
    if(self.hid != 0 && !memcmp(pkt->src, prefix, sizeof(prefix))){
        memcpy(p.local, &pkt->src[sizeof(prefix)], sizeof(prefix));
        memcpy(p.remote, pkt->dest, sizeof(p.remote));
        
        // we have set h-pointer when 'local/64 -> local/48'
        if(p.h == NULL){
            p.h = Get(0);
            // make sure we have root's info
            if(p.h == NULL){ 
                p.hid = 0;
                QueueAction(ACTION_HOST, &p.hid);
                return false;
            }
        }

        auto i = self.ma.find(p);
        if(i != self.ma.end()){
            if(p.h == fake){
                QueueAction(ACTION_HOST, &p.hid);
                return false; // we alloced the LL, but no answer from the root
            }
            p.hid = self.hid;
            p.llid = i->second;
            return true;
        }

        // new one
        uint16 tm = TS::tm();
        uint16 mdtm = 0;
        uint16 mid = 0;
        uint16 n = 0;

        auto t = self.mi.begin();
        while(n < LLID_NONE){
            if( t == self.mi.end() || n != t->first){
                mid = n;
                break;
            }
            if( (uint16)(tm - t->second.tm) > mdtm ){
                mdtm = (uint16)(tm - t->second.tm);
                mid = n;
            }
            n++;
            t++;
        }
        p.hid = self.hid;
        p.llid = mid;
        p.isnew = true;

        self.ma[p] = mid;
        self.mi[mid] = LLData(p);
        
        QueueAction(ACTION_LL_NEW, &p);
        return p.h != fake;
    }
    // invalid route table
    LOG_ERROR("bad route table %s->%s", NetAddress(pkt->src, 16).host().c_str(), NetAddress(pkt->dest, 16).host().c_str());
    return false;
}

bool HomeNets::HaveDefGateway()
{
    if(defgw < 0)return false;
    if(defgw > 0)return true;

    char nm[64];
    if( GetGateway(AF_INET6, nm, NULL) && strstr(nm, "thnet") == NULL ){
        defgw = 1;
        return true;
    }
    
    defgw = -1;
    return false;
}


void HomeNets::OnTick()
{
    static int tick = 0;
    tick++;
    
    // change key    
    if( self.hid != 0 && (TS::exp(self.tm, keepalive) || TS::exp(self.keytm, keylife))){
        self.tm = self.keytm = TS::tm();
        self.keyVer = 1-self.keyVer;
        QueueAction(ACTION_NEWKEY, &self.keyVer);
        LOG_INFO("change key ver=%d", self.keyVer);
    }

    self.OnTick(true);
    
    for(auto i = hnets.begin(); i != hnets.end(); ){
        HomeNet* h = &i->second;
        h->OnTick(false);
        if(self.hid == 0 && TS::exp(h->tm, 30*60) && TS::expset(h->keytm, 30*60)){ 
            // long time no see, try tls connection
            h->attr.hosts->insert(new attrDictHost(&self, false));
            QueueAttr(h->hid);
        }
        if( h->hid != 0 && TS::exp(h->tm, ROOT_TIMEOUT) ){  
            // delete inactive clients to save memory
            LOG_INFO("HomeNet::Tick() delete inactive client hid=%d", h->hid);
            i = hnets.erase(i);
            continue;
        }
        i++;
    }
}

#include <openssl/aes.h>
#include <openssl/rand.h>
void random(byte data[], size_t sz)
{
    RAND_bytes(data, (int)sz);
}

int llencrypt = 2;
bool HomeNet::crypt(LLPkt* pkt, size_t sz) const
{
    
    if(llencrypt == 0){
        if(pkt->salt != 0)return false;
        // plain mode
        pkt->csum = htons(0x8000); 
        pkt->hid = htons(pkt->hid);
        pkt->llid = htons(pkt->llid);
        return true;
    }
    if(llencrypt == 1){
        // plain mode
        if(pkt->salt == 0 && pkt->csum == htons(0x8000)){
            pkt->hid = htons(pkt->hid);
            pkt->llid = htons(pkt->llid);
            return true; 
        }
    }

    byte iv[AES_KEY_LEN];
    AES_KEY ekey;

    uint16 ts = (uint16)(time(NULL) / 5);
    
    memset(iv, 0, sizeof(iv));
    if(pkt->salt == 0){ // encrypt
        pkt->salt =  (keyVer ? 0x8000 : 0) | ( ts >> 1 );
        if(pkt->salt == 0) pkt->salt = 1;
        pkt->salt = htons(pkt->salt);
        
        if(keyVer)
            AES_set_encrypt_key(&key[AES_KEY_LEN], 8*AES_KEY_LEN, &ekey);
        else
            AES_set_encrypt_key(&key[0], 8*AES_KEY_LEN, &ekey);
            
        pkt->hid = htons(pkt->hid);
        pkt->llid = htons(pkt->llid);
        pkt->csum = 0;
        
        memcpy(iv, pkt, 6);

        //LOG_INFO("encrypt iv=%llx key=%llx rkey=%llx sz=%d", *(int64*)iv, *(int64*)&ekey.rd_key[0], *(int64*)key, sz);
        
        int num = 0;
        AES_cfb128_encrypt((byte*)&pkt->vers, (byte*)&pkt->vers, sz-8, &ekey, iv, &num, AES_ENCRYPT);
        AES_cfb128_encrypt((byte*)&pkt->csum, (byte*)&pkt->csum, 2, &ekey, iv, &num, AES_ENCRYPT);
        return true;
        
    } else { // decrypt
        memcpy(iv, pkt, 6);
        pkt->salt = htons(pkt->salt);

        if(pkt->salt & 0x8000)
            AES_set_encrypt_key(&key[AES_KEY_LEN], 8*AES_KEY_LEN, &ekey);
        else
            AES_set_encrypt_key(&key[0], 8*AES_KEY_LEN, &ekey);

        pkt->salt <<= 1;
        if( (int16)(pkt->salt - ts) < -120 || (int16)(pkt->salt - ts) > 120 ){ // 20 minutes range
            LOG_DEBUG("decrypt packet %d.%d timediff=%d", htons(pkt->hid), htons(pkt->llid), pkt->salt - ts);
            return false;
        }
        
        //LOG_INFO("decrypt iv=%llx key=%llx rkey=%llx sz=%d", *(int64*)iv, *(int64*)&ekey.rd_key[0], *(int64*)key, sz);

        int num = 0;
        AES_cfb128_encrypt((byte*)&pkt->vers, (byte*)&pkt->vers, sz-8, &ekey, iv, &num, AES_DECRYPT);
        AES_cfb128_encrypt((byte*)&pkt->csum, (byte*)&pkt->csum, 2, &ekey, iv, &num, AES_DECRYPT);
    
        if(pkt->csum != 0){
            LOG_DEBUG("decrypt packet %d.%d crc error", htons(pkt->hid), htons(pkt->llid));
            return false;
        }
        pkt->salt = 0;
        pkt->hid = htons(pkt->hid);
        pkt->llid = htons(pkt->llid);
        return true; 
    }
}

