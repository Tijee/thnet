/*
 * MIT License
 * Copyright (c) 2016 Tijee Corporation.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of 
 * this software and associated documentation files (the "Software"), to deal in 
 * the Software without restriction, including without limitation the rights to use, 
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject 
 * to the following conditions:

 * The above copyright notice and this permission notice shall be included in all 
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *  
 * This file is part of the thnet project.
 */

#include "thnet.h"
#include <net/if.h>
#include <linux/if_tun.h>


struct TunHandler : IFHandler {
    TunHandler(HomeNets*hn, cstr name, shared_ptr<IFIO> io): IFHandler(hn, name, io) {}

    void OnRecv(const NetAddress& src, byte data[], size_t dsz)
    {
        IPv6Pkt* p =(IPv6Pkt*)data;
        if(htonl(p->proto) != 0x86dd || (p->vers[0]&0xf0) != 0x60 || p->dest[0] == 0 || p->dest[0] >= 0xfe){
            show("T0", data, dsz);
            return;   // loop/multicast packet
        }
        if(!hn->HaveDefGateway() && memcmp(p->src, hn->prefix, 6) != 0){
            show("T1", data, dsz);
            return;  // not from homenet, bad route table
        }
        if(hn->HaveDefGateway() && memcmp(p->dest, hn->prefix, 6) != 0){
            show("T2", data, dsz);
            return;  // we have def router, so don't relay other packets
        }
        p->bsw();
        
        LLResult e;
        if(! hn->GetLL(p, e) ){ // ll policy
            p->bsw();
            show("T3", data, dsz);
            return;
        }
        
        // encapsulation
        LLPkt* pkt = (LLPkt*)&p->data[ sizeof(LLPkt::data) - sizeof(LLPkt) ];
        {
            byte ndata[sizeof(pkt->vers) + sizeof(pkt->flags)];
            memcpy(ndata, &p->vers[2], sizeof(pkt->vers));
            memcpy(ndata+sizeof(pkt->vers), p->flags, sizeof(pkt->flags));
            
            pkt->reset();
            pkt->hid = e.hid;
            pkt->llid = e.llid;
            memcpy(pkt->vers, ndata, sizeof(ndata));
        }
        dsz = p->len + sizeof(*pkt) - sizeof(pkt->data);

        HomeNet* h = hn->Get(pkt->hid & HID_UNKNOWN);
        ASSERT(h != NULL);
        if(!h->crypt(pkt, dsz)){
            LOG_WARN("encrypt pkt for %d.%d failed", e.hid, e.llid);
            return;
        }
        
        // try send or queue
        //LOG_DEBUG("getll target=%d hid=%d llid=%d isnew=%d age=%d", e.h->hid, e.hid, e.llid, e.isnew, (uint16)(TS::tm() - e.h->sintm));
        if(e.isnew || TS::exp(e.h->sintm, e.h->lifetime)){
            if(!e.isnew){
                if(hn->self.hid == 0){
                    e.h->attr.hosts->insert( new attrDictHost(&hn->self, false) );
                    hn->QueueAttr(e.h->hid);
                } else {
                    hn->QueueAction(ACTION_HOST, &e.h->hid);
                }
            }
            hn->QueuePkt(e.h->hid, pkt, dsz);
        } else {
            hn->SendUdp(e.h->sin, pkt, dsz);
        }
    }
};


void HomeNets::WriteTun(HomeNet*h, const LLPkt* pkt, int sz)
{   
    // FIXME DDOS & delayask
    auto pl = h->mi.find(pkt->llid);
    if(pl == h->mi.end()){
        LOG_DEBUG("WT unknown %d %d", pkt->hid, pkt->llid);
        if(self.hid != 0){
            LLID id(h->hid, pkt->llid);
            QueueAction(ACTION_LL_ID, &id);
        }
        return;
    }

    if(h == &self && TS::exp(pl->second.tm, LL_TIMEOUT*2/3)){
        LLEntry e(pkt->llid, pl->second);
        QueueAction(ACTION_LL_NEW, &e);
    }
    pl->second.tm = TS::tm();
    
    //LOG_DEBUG("WT %d %d %s->%s", pkt->hid, pkt->llid, NetAddress(pl->second.local, 8).host().c_str(), NetAddress(pl->second.remote, 16).host().c_str());
    
    // decapsulation
    byte msg[1540];
    IPv6Pkt* p = (IPv6Pkt*)msg;
    sz = sz - sizeof(LLPkt) + sizeof(LLPkt::data);
    memcpy(p->data, pkt->data, sz);
    p->proto = 0x86dd;
    p->len = (uint16)sz;
    p->vers[0] = 0x60;
    p->vers[1] = 0;
    memcpy(&p->vers[2], pkt->vers, 2);
    memcpy(p->flags, pkt->flags, 2);
    if(pkt->hid & HID_BACKWARD){
        memcpy(p->dest, prefix, 8);
        p->dest[6] = (byte)(h->hid >> 8);
        p->dest[7] = (byte)(h->hid >> 0);
        memcpy(p->dest+8, pl->second.local, 8);
        memcpy(p->src, pl->second.remote, 16);
    } else {
        memcpy(p->src, prefix, 8);
        p->src[6] = (byte)(h->hid >> 8);
        p->src[7] = (byte)(h->hid >> 0);
        memcpy(p->src+8, pl->second.local, 8);
        memcpy(p->dest, pl->second.remote, 16);
    }
    p->bsw();
    sz += sizeof(IPv6Pkt)-sizeof(p->data);
    
    if(ifs.size()>=1 && ifs[0] && ifs[0]->io){
        ifs[0]->io->Write(msg, sz);
    }
}


struct TunIO : IFIO, IPoll {
    TunIO(cstr value) : IFIO(this) {
        int h = open("/dev/net/tun", O_RDWR);
        if(h < 0){
            LOG_ERROR("/dev/net/tun is not supported");
            return;
        }
        setfd(h);

        struct ifreq ifr;
        memset(&ifr, 0, sizeof(ifr));
        ifr.ifr_flags = IFF_TUN;
        strcpy(ifr.ifr_name, value);
        if (ioctl(h, TUNSETIFF, (void *)&ifr) < 0) {
            LOG_ERROR("failed to create tun device");
            close();
            return;
        }
        desc = value;
    }

    void OnPoll(short, PollFd*){
        byte data[1520];
        int sz = read(fd(), data, sizeof(data));
        if(sz <= 0)return;
        ifh->OnRecv(NetAddress(), data, (size_t)sz);
    }
    
    ssize_t Write(const byte* pkt, size_t sz) override
    {
        return write(fd(), pkt, sz);
    }
};


