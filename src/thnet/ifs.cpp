/*
 * MIT License
 * Copyright (c) 2016 Tijee Corporation.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of 
 * this software and associated documentation files (the "Software"), to deal in 
 * the Software without restriction, including without limitation the rights to use, 
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject 
 * to the following conditions:

 * The above copyright notice and this permission notice shall be included in all 
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *  
 * This file is part of the thnet project.
 */

#include "thnet.h"
#include "ifndp.h"
#include "if6lowpan.h"
#include "ifdhcp.h"
#include "ifudp.h"
#include "iftun.h"

IFHandler* IFHandler::New(HomeNets* hn, cstr name, shared_ptr<IFIO> io)
{
    if(!strcmp(name, "tun")) return new TunHandler(hn, name, io);
    if(!strcmp(name, "udp")) return new UdpHandler(hn, name, io);
    return new IcmpHandler(hn, name, io);
}


IFHandler* IFHandler::New(HomeNets* hn, cstr name, cstr value)
{
    shared_ptr<IFIO> p;
    if(!strcmp(name, "tun")) p.reset( new TunIO(value) );
    else if(!strcmp(name, "udp")) p.reset( new UdpIO(value) );
    else {
        int en = 0;
        if(strstr(value, "ndp")) en |= ENABLE_NDP;
        if(strstr(value, "ra")) en |= ENABLE_RAD;
        p.reset( new EthIcmpIO(name, en) );
    }
    if(!p || p->fd() < 0)return NULL;
    return New(hn, name, p);
}

