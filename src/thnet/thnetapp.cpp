/*
 * MIT License
 * Copyright (c) 2016 Tijee Corporation.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of 
 * this software and associated documentation files (the "Software"), to deal in 
 * the Software without restriction, including without limitation the rights to use, 
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject 
 * to the following conditions:

 * The above copyright notice and this permission notice shall be included in all 
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *  
 * This file is part of the thnet project.
 */

#include "thnet.h"

void WSAttr::set(HomeNets* ht, cstr msg){
    hid = HID_UNKNOWN;
    prefix.alen = 0;
    bkey = false;
    hosts->clear();
    lls->clear();
    newlls->clear();
    badlls->clear();

    string err;
    if(!attrDictStrict::set(msg, err)){
        LOG_DEBUG("wsrecv error %s", err.c_str());
        return;
    }

    if(bkey)memcpy(ht->self.key, key, sizeof(key));
    for(auto p : *hosts){
        ht->WSRecvHost(*p);
    }
    for(auto p : *lls){
        ht->WSRecvLL(p->hid, *p);
    }
    for(auto p : *newlls){
        ht->WSRecvLLAddr(*p, true);
    }
    for(auto p : *badlls){
        ht->WSRecvLLAddr(*p, false);
    }
}


struct INodeRecv {
    virtual void OnNodeRecv(byte data[], int sz) =0;
};

struct NodeUDP : PollFd {
    INodeRecv* rcv;
    sockaddr_in6 node;
    bool debug;
    NodeUDP(INodeRecv* r) : PollFd(NULL, socket(AF_INET6, SOCK_DGRAM, 0)), rcv(r), debug(false) {
        NetAddress na("::1");
        memset(&node, 0, sizeof(node));
        struct sockaddr_in6 sin = na.get6();
        bind(fd(), (sockaddr*)&sin, sizeof(sin));
    }
    int port() {
        struct sockaddr_in6 sin;
        socklen_t slen = sizeof(sin);
        getsockname(fd(), (sockaddr*)&sin, &slen);
        return htons(sin.sin6_port);
    }
    
    void Debug(cstr type, byte data[], int size){
        if(!debug)return;
        if(data[2] == '{')
            LOG_DEBUG("%s (%d,%d) sz=%d hid=%d \"%s\"", type, port(), htons(node.sin6_port), size, data[0]*256 + data[1], (char*)&data[2]);
        else
            LOG_DEBUG("%s (%d,%d) sz=%d hid=%d \"%s\"", type, port(), htons(node.sin6_port), size, data[0]*256 + data[1], hex(&data[2], size-2).c_str());
    }
    
    void OnPoll(short evt) override {
        int ret;
        byte data[1800];
        
        if(node.sin6_family)
            ret = recv(fd(), data, sizeof(data)-1, 0);
        else {
            socklen_t slen = sizeof(node);
            ret = recvfrom(fd(), data, sizeof(data)-1, 0, (sockaddr*)&node, &slen);
        }
        if(ret <= 3)return;
        data[ret] = 0;
        
        Debug("wsrecv", data, ret);
        
        rcv->OnNodeRecv(data, ret);
    }
    
    ssize_t Send(byte data[], size_t sz){
        Debug("wssend", data, (int)sz);
        
        return sendto(fd(), data, sz, 0, (sockaddr*)&node, sizeof(node));
    }
};


struct VirtualIO : IFIO, INodeRecv {
    NodeUDP node;
    
    VirtualIO() : IFIO(NULL), node(this) {
        desc = "virtual";
    }
    
    void OnNodeRecv(byte data[], int sz) {
        ifh->OnRecv(NetAddress(data, 6), data+6, (size_t)(sz-6));
    }

    ssize_t Send(const NetAddress& target, const byte* pkt, size_t sz) override {
        byte data[1800];
        if(target.alen != 4)memset(data, 0, 6); else target.get(data, 6);
        memcpy(data+6, pkt, sz);
        return node.Send(data, sz+6) - 6;
    }
    ssize_t Write(const byte* pkt, size_t sz) override {
        return Send(NetAddress(), pkt, sz);
    }
};

struct Actions : ITimer, INodeRecv {
    HomeNets* hn;
    NodeUDP node;
    int delayid;
    
    byte keyVer;
    set<LLEntry> newlls;
    set<LLID> llids;
    set<uint16> hosts;

    set<uint16> mark;
    
    
    Actions(HomeNets* hn_) : hn(hn_), node(this), delayid(3), keyVer(0xff) {
    }
    
    void Reset(){
        memset(&node.node, 0, sizeof(node.node));
        keyVer = 0xff;
        newlls.clear();
        hosts.clear();
        llids.clear();
        mark.clear();
        stop();
    }
    
    void OnNodeRecv(byte data[], int sz) {
        uint16 hid = data[0]*256 + data[1];
        HomeNet* h = hn->Get(hid);
        if(h == NULL){
            if(hid != 0){
                LOG_ERROR("actions unknown hid %d", hid);
                return;
            }
        } else {
            h->tm = TS::tm();
        }
        hn->self.tm = TS::tm();
        if(data[2] == '{' || data[2] == '['){
            if(hn->self.hid == 0 || hid != 0){
                LOG_ERROR("text message should be sent from root to gateway");
                return;
            }
            hn->self.attr.set(hn, (cstr)&data[2]);
        } else {
            if(hn->self.hid != 0 || hid == 0){
                LOG_ERROR("binary message should be sent from gateway to root");
                return;
            }
            hn->WSRecvAction(hid, &data[2], sz-2);
        }
    }
    
    
    void Queue(byte action, const void* msg){
        if(action == ACTION_NEWKEY){
            if(!msg)keyVer = 0;
            else keyVer = *(const byte*)msg;
        }
        if(action == ACTION_LL_NEW){
            LLEntry e( *(const LLEntry*)msg );
            e.bsw();
            newlls.insert(e);
        }
        if(action == ACTION_LL_ID){
            LLID e(*(const LLID*)msg);
            e.bsw();
            llids.insert(e);
            return;  // will active by onTick
        }
        if(action == ACTION_HOST){
            hosts.insert(htons(*(const uint16*)msg));
        }
        if(!isactive()){
            start(100, 0);
            delayid = 3;
        }
    }
    
    void OnTick()
    {
        if(llids.size()){
            if(delayid > 0) delayid--;
            if(delayid == 0) Queue(ACTION_NULL, NULL);
        }
    }

    void Mark(uint16 hid)
    {
        mark.insert(hid);
        Queue(ACTION_NULL, NULL);
    }
    
#define ACTION_SEND(vv, action)\
    else if(vv.size()){\
        byte cnt = 255;\
        if(vv.size() < cnt)\
            cnt = (byte)vv.size();\
        bsz = 2+cnt*sizeof(*vv.begin());\
        byte* pb = buf+2;\
        *pb++ = action;\
        *pb++ = cnt;\
        for(auto i = vv.begin(); cnt>0; cnt--){\
            memcpy(pb, &*i, sizeof(*i));\
            pb += sizeof(*i);\
            i = vv.erase(i);\
        }\
    }

    void Call() { // timer
        byte buf[1800];
        size_t bsz = 0;
        while(true) {
            bsz = 0;
            if(keyVer == 0 || keyVer == 1){
                buf[2] = ACTION_NEWKEY;
                buf[3] = keyVer;
                keyVer = 0xff;
                bsz = 2;
            }
            ACTION_SEND(newlls,  ACTION_LL_NEW)
            ACTION_SEND(llids,   ACTION_LL_ID)
            ACTION_SEND(hosts,   ACTION_HOST)
            if(!bsz)break;
            buf[0] = buf[1] = 0;
            node.Send(buf, bsz+2);
        }
        for(auto ph = mark.begin(); ph != mark.end(); ph = mark.erase(ph)){
            HomeNet* h = hn->Get(*ph);
            if(!h || !h->attr.havedata())continue;
            ostringstream os;
            h->attr.get(os);
            h->attr.reset();
            auto s = os.str();
            bsz = s.size();
            if(bsz >= sizeof(buf)-10){
                LOG_ERROR("wssend too long %d", s.size());
            }
            strcpy((char*)&buf[2], s.c_str());
            buf[0] = (byte)(h->hid >> 8);
            buf[1] = (byte)(h->hid);
            node.Send(buf, bsz+2);
        }
    }
};


struct HomeTun : HomeNets, ITimer, ICtrlRecv {
    Actions actions;
    string ctrlapp;
    shared_ptr<attrDict> cfg;
    
    HomeTun() : actions(this)
    {
        start(100, 1000);

        CtrlMsg m("all", "init", "");
        m.Send();
    }
    
    virtual ~HomeTun(){
    }
    
    void Call(){ // timer
        OnTick();
        for(auto p : ifs){
            p->OnTick();
        }
        actions.OnTick();
    }
    
    void QueueAction(byte action, const void* msg) override
    {
        ASSERT(self.hid != 0);
        actions.Queue(action, msg);
    }

    void QueueAttr(uint16 hid) override
    {
        ASSERT(self.hid == 0 && hid != 0);
        actions.Mark(hid);
    }
    
    bool IsLocalServer(int64 euid) override
    {
        return euid == self.euid;
    }
    
    bool SetNet(shared_ptr<attrDict> interfaces, string& err)
    {
        char buf[64];
        
        ///////////////////////////// tun
        cstr ifname = "tun";
        shared_ptr<IFHandler> p;
        if(interfaces->find(ifname) != interfaces->end()){
            string param;
            if(!interfaces->get(ifname, param) || param != "virtual"){
                err = "interfaces.tun should not exist or be a function";
                return false;
            }
            shared_ptr<IFIO> io(new VirtualIO());
            p.reset( IFHandler::New(this, ifname, io) );
        } else 
        {
            static char callcnt = 'a';
            sprintf(buf, "thnet%d%c", getpid(), callcnt);
            callcnt++;
            if(callcnt > 'z')callcnt = 'a';
            
            if(geteuid() != 0){  // linux root !
                err = "need root privilege to create tun device";
                return false;
            }
            p.reset( IFHandler::New(this, ifname, buf) );
        }
        if(!p)return false;
        ifs.push_back(p);
        
        ///////////////////////////// udp
        ifname = "udp";
        p.reset();
        if(interfaces->find(ifname) != interfaces->end()){
            string param;
            int port;
            if(interfaces->get(ifname, param) && param == "virtual"){
                shared_ptr<IFIO> io(new VirtualIO());
                p.reset( IFHandler::New(this, ifname, io) );
            }
            if(interfaces->get(ifname, port) && port > 0){
                sprintf(buf, "%d", port);
                p.reset( IFHandler::New(this, ifname, buf) );
                if(!p)err = "udp bind failed 2";
            }
        } else {
            p.reset( IFHandler::New(this, ifname, "1194") );
            if(!p)err = "udp bind failed 1";
        }
        if(!p)return false;
        ifs.push_back(p);
        
        ///////////////////////////// others
        char cmd[200];
        cstr netconf = "/proc/sys/net/ipv6/conf/";
        cmd[0]=0;
        for(auto pf : *interfaces){
            if(pf.first == "udp" || pf.first == "tun")continue;
            ifname = pf.first.c_str();
            string param;
            if(!interfaces->get(pf.first, param)){
                sprintf(cmd, "interface %s need string param", ifname);
                err = cmd;
                break;
            }
            if(param == "virtual"){
                shared_ptr<IFIO> io(new VirtualIO());
                p.reset( IFHandler::New(this, ifname, io) );
            } else {
                sprintf(cmd, "%s%s/autoconf", netconf, ifname);
                if( ReadFile(cmd, buf, sizeof(buf)) < 0 ){
                    sprintf(cmd, "interface %s does not exist", ifname);
                    err = cmd;
                    break;
                }
                p.reset( IFHandler::New(this, ifname, param.c_str()) );
            }
            if(p)ifs.push_back(p);
        }
        if(err.size())return false;
        
        LOG_INFO("thnet start hid=%d tun=%s udp=%s", self.hid, ifs[0]->io->desc.c_str(), ifs[1]->io->desc.c_str());

        char defIf[64];
        NetAddress via;
        if(!GetGateway(AF_INET6, defIf, &via)){
            defIf[0] = 0;
            via.alen = 0;
        }

        sprintf(cmd, "%sall/forwarding", netconf);
        WriteFile(cmd, "1");
        
        // delay tun setup to last for the euid maybe one interface's IP.
        int idx = ifs.size();
        while(idx > 0){
            auto pf = ifs[--idx];
            ifname = pf->name.c_str();
            if(*ifname == '/')continue;
            if(pf->io->desc == "virtual")continue;
            if(pf->name == "udp")continue;
            if(pf->name == "tun")ifname = pf->io->desc.c_str();

            sprintf(cmd, "%s%s/autoconf", netconf, ifname);
            WriteFile(cmd, "0");
            sprintf(cmd, "%s%s/disable_ipv6", netconf, ifname);
            WriteFile(cmd, "0");
            sprintf(cmd, "%s%s/accept_ra", netconf, ifname);
            WriteFile(cmd, "1");
            sprintf(cmd, "%s%s/mtu", netconf, ifname);
            sprintf(buf, "%d", mtu);
            WriteFile(cmd, buf);
            
            NetAddress na(prefix, sizeof(prefix));
            if(pf->name == "tun"){
                na.port = 48;
                memcpy(&na.addr[8], &self.euid, sizeof(self.euid));
            } else {
                na.port = 64;
                memcpy(&na.addr[8], "THNET", 5);
                na.addr[13] = idx;
                na.addr[14] = 0;
                na.addr[15] = 1;
            }
            memcpy((byte*)&pf->euid, &na.addr[8], 8);
            pf->euids[pf->euid] = TS::tm();
            
            LOG_INFO("addrs %s %s >%s", ifname, na.str().c_str(), defIf);

            vector<NetAddress> addrs;
            addrs.push_back(na);
            IFSetIP(ifname, addrs);

            if(defIf[0] == 0){
       	        if(pf->name == "tun")IFAddRoute(ifname, "::/0", NULL);
            } else if(!strcmp(ifname, defIf)){ // set route manually for autoconf is disabled
                IFAddRoute(defIf, "::/0", via.host().c_str());
            }
        }
        return true;
    }
    
    void OnAddrUpdated(HomeNet* h)
    {
        NetAddress a(h->sin);
        
        NetAddress aaaa(prefix, sizeof(prefix));
        aaaa.addr[6] = (byte)(h->hid>>8);
        aaaa.addr[7] = (byte)h->hid;
        memcpy(aaaa.addr+sizeof(prefix), &h->euid, sizeof(prefix));
        
        CtrlMsg m(ctrlapp.c_str(), "updateaddr", "none");
        m.data->bind("hid", h->hid);
        m.data->bind("a", a.host());
        m.data->bind("aaaa", aaaa.host());
        m.Send();
    }

    void NewClient(CtrlMsg& m)
    {
        int hid = 0;
        if(!m.data->get("hid", hid) || hid<=0 || hid > 32760){
            m.ReplyError("hid range [1, 32760]");
            return;
        }
        string euid;
        if(!m.data->get("euid", euid) || euid.size() != 16){
            m.ReplyError("euid should be 16-chars hex string");
            return;
        }
        int lifetime = 0;
        if(!m.data->get("lifetime", lifetime) || lifetime < 20 || lifetime > ROOT_TIMEOUT){
            m.ReplyError("lifetime range [20, ROOT_TIMEOUT]");
            return;
        }
        HomeNet* h = Get(hid, true);
        fromhex(euid.c_str(), (byte*)&h->euid);
        h->mi.clear();
        h->ma.clear();

        h->lifetime = lifetime;
        h->attr.bindall(true);
        h->attr.hid = h->hid;
        h->attr.bkey = true;
        h->attr.mtu = mtu;
        memcpy(h->attr.key, h->key, sizeof(h->key));
        
        h->attr.prefix.set(prefix, sizeof(prefix));
        h->attr.prefix.addr[6] = (byte)(hid>>8);
        h->attr.prefix.addr[7] = (byte)(hid);
        *(h->attr.dns) = *(self.attr.dns);
        h->attr.hosts->insert( new attrDictHost(&self, false) );
        QueueAttr(hid);
        
        m.ReplyResult("ok");
    }

    void Start(CtrlMsg& m)
    {
        // local config override
        attrDict& param = *m.data;
        for(auto v = cfg->begin(); v != cfg->end(); v++ ){
            param[v->first] = v->second;
        }
        
        int hid;
        if(!param.get("hid", hid) || hid<0 || hid > 32760){
            m.ReplyError("cfg.hid range [0, 32760]");
            return;
        }
        extern int llencrypt;
        if(param.get("encrypt", llencrypt) && (llencrypt < 0 || llencrypt > 2)){
            m.ReplyError("cfg.encrypt should be in range [0, 2]");
            return;
        }
        
        mtu = 1500;
        if(param.get("mtu", mtu) && (mtu < 1380 || mtu > 1500)){
            m.ReplyError("cfg.mtu should be in range [1380, 1500]");
            return;
        }
        mtu -= mtu%4;
        
        if(param.get("keepalive", keepalive) && (keepalive < 60 || keepalive > 60*60)){
            m.ReplyError("cfg.keepalive should be in range [60, 3600]");
            return;
        }

        if(param.get("keylife", keylife) && (keylife < 30*60 || keylife > 48*60*60)){
            m.ReplyError("cfg.keylife should be in range [1800, 48*3600]");
            return;
        }

        shared_ptr<attrDict> interfaces;
        if(!param.get("interfaces", interfaces)){
            m.ReplyError("cfg should have interfaces member");
            return;
        }
        
        NetAddress prefix;
        if(!param.get("prefix", prefix) || prefix.alen != 16){
            m.ReplyError("cfg.prefix should be ipv6 address");
            return;
        }
        NetAddress addr;
        if(!param.get("addr", addr) || addr.alen != 4){
            m.ReplyError("cfg.addr should be global IPv4 address");
            return;
        }
        param.get("wsdebug", actions.node.debug);
        
        shared_ptr< attrVector<Iattr> > dns;
        param.get("dns", dns);
        
        ctrlapp = m.peer;
        Reset(hid, prefix.addr);
        actions.Reset();

        string err;
        if(!SetNet(interfaces, err)){
            m.ReplyError(err);
            return;
        }
        self.sin = addr.get4();
        self.sin.sin_port = htons((uint16)atoi(ifs[1]->io->desc.c_str()));
        self.sintm = TS::tm();
        if(dns && dns->size()){
            self.attr.dns->clear();
            uint32 ac = dns->size();
            for(uint32 i=0; i<ac; i++){
                if((*dns)[i]->type != ATTR_STRING)continue;
                auto str = static_pointer_cast< attrh<string, ATTR_STRING> >((*dns)[i]);
                self.attr.dns->push_back( NetAddress(str->t) );
            }
        }
        
        // return all ports in return string
        param.clear();
        param.bind("action", actions.node.port());
        for(auto pf : ifs){
            if(pf->io->desc != "virtual")continue;
            auto io = static_pointer_cast<VirtualIO>(pf->io);
            param.bind(pf->name.c_str(), io->node.port());
        }
        param.get(err);
        m.Reply();
    }
    
    void OnCtrlRecv(const vector<char*>& lines){
        for(auto line : lines){
            CtrlMsg m(line);
            if(!m || m.isreply)continue;
            if(!strcmp(m.type, "newclient"))NewClient(m);
            else if(!strcmp(m.type, "start"))Start(m);
            else m.ReplyError("unknown type");
        }
    }
};



void appthnet(shared_ptr<attrDict> cfg)
{
    shared_ptr<HomeTun> ht( new HomeTun() );
    ht->cfg = cfg;
    PollLoop::instance->Run(ht.get());
}


