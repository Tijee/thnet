/*
 * MIT License
 * Copyright (c) 2016 Tijee Corporation.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of 
 * this software and associated documentation files (the "Software"), to deal in 
 * the Software without restriction, including without limitation the rights to use, 
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject 
 * to the following conditions:

 * The above copyright notice and this permission notice shall be included in all 
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *  
 * This file is part of the thnet project.
 */

#ifndef __thnet_ifndp_h
#define __thnet_ifndp_h

#include <stddef.h>
#include <netinet/in.h>
#include <netinet/ip6.h>
#include <netinet/icmp6.h>
#include <netinet/ether.h>
#include <netpacket/packet.h>
#include <net/if.h>
#include <linux/filter.h>


#define ALL_NODES   "FF02::1"
#define ALL_ROUTERS "FF02::2"
#define ENABLE_NDP   1
#define ENABLE_RAD   2
#define ENABLE_DHCP  4

struct EthIcmpIO : IFIO, IPoll {
    // ND_NEIGHBOR_SOLICIT pakcets are sent to solicited node multicast address( FF02::1:FFXX:YYZZ )
    // XXYYZZ are the last 3 bytes of mac address
    struct NSReceiver : PollFd {
        NSReceiver(IPoll* p) : PollFd(p) { }
        
        bool Start(cstr name){
            int h = socket(PF_PACKET, SOCK_DGRAM, htons(ETH_P_IPV6));
            if( h < 0) {
                LOG_ERROR("NDP socket failed %s", name);
                return false;
            }
            setfd(h);
            
            struct sockaddr_ll lladdr;
            memset(&lladdr, 0, sizeof(struct sockaddr_ll));
            lladdr.sll_family   = AF_PACKET;
            lladdr.sll_protocol = htons(ETH_P_IPV6);

            if (!(lladdr.sll_ifindex = if_nametoindex(name)) || bind(h, (struct sockaddr* )&lladdr, sizeof(struct sockaddr_ll)) < 0) {
                close();
                LOG_ERROR("NDP failed to bind %s", name);
                return false;
            }
            int on = 1;
            ioctl(h, FIONBIO, (char* )&on);
            
            static struct sock_filter bpf[] = {
                BPF_STMT(BPF_LD | BPF_B | BPF_ABS, offsetof(struct ip6_hdr, ip6_nxt)),
                BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, IPPROTO_ICMPV6, 0, 3),
                BPF_STMT(BPF_LD | BPF_B | BPF_ABS, sizeof(struct ip6_hdr) +
		                offsetof(struct icmp6_hdr, icmp6_type)),
                BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, ND_NEIGHBOR_SOLICIT, 0, 1),
                BPF_STMT(BPF_RET | BPF_K, 0xffffffff),
                BPF_STMT(BPF_RET | BPF_K, 0),
            };
            static const struct sock_fprog fprog = {sizeof(bpf) / sizeof(*bpf), bpf};

            if (setsockopt(h, SOL_SOCKET, SO_ATTACH_FILTER, &fprog, sizeof(fprog)) < 0) {
                close();
                LOG_ERROR("NDP failed to add filter %s", name);
                return NULL;
            }
            
            struct ifreq ifr;
            memset(&ifr, 0, sizeof(ifr));
            strncpy(ifr.ifr_name, name, IFNAMSIZ);
            ioctl(h, SIOCGIFFLAGS, &ifr);
            ifr.ifr_flags |= IFF_ALLMULTI;
            ioctl(h, SIOCSIFFLAGS, &ifr);
            return true;
        }
    } ns;
    int enable;
    vector<NetAddress> locals;
    
    EthIcmpIO(cstr name, int en) : IFIO(this), ns(this), enable(en) {
        setfd( EthIcmpSocket(name, hwaddr) );
        if(fd() < 0) return;
        if(en & ENABLE_NDP)ns.Start(name);
        IFGetIP(name, locals);

        ostringstream os;
        os << en;
        desc = os.str();
    }
    
    void OnPoll(short, PollFd* fd){
        struct iovec iov;
        byte msg[200];

        iov.iov_len = sizeof(msg);
        iov.iov_base = (caddr_t)msg;

        struct sockaddr_in6 sdr;
        struct msghdr mhdr;
        memset(&mhdr, 0, sizeof(mhdr));
        mhdr.msg_name = (caddr_t)&sdr;
        mhdr.msg_namelen = sizeof(sdr);
        mhdr.msg_iov = &iov;
        mhdr.msg_iovlen = 1;

        int len = recvmsg(fd->fd(), &mhdr, 0);
        if(fd == &ns){
            if(len < (int)(sizeof(struct ip6_hdr) + sizeof(struct icmp6_hdr)))return;
            const struct ip6_hdr* ip6h = (const struct ip6_hdr* )msg;
            NetAddress saddr((const byte*)&ip6h->ip6_src, sizeof(struct in6_addr));
            ifh->OnRecv(saddr, msg + sizeof(struct ip6_hdr), (size_t)(len - sizeof(struct ip6_hdr)));
        } else {
            if(len < (int)sizeof(struct icmp6_hdr))return;
            for(auto& i : locals){
                if(i.alen != sizeof(in6_addr))continue;
                if(!memcmp(i.addr, &sdr.sin6_addr, sizeof(in6_addr)))return;  // sent by me
            }
            ifh->OnRecv(NetAddress(sdr), msg, (size_t)len);
        }
    }
    
    ssize_t Send(const NetAddress& target, const byte icmp[], size_t size) override
    {
        struct iovec iov;
        iov.iov_len = size;
        iov.iov_base = (caddr_t)icmp;
        
        sockaddr_in6 ddr = target.get6();
        
        struct msghdr mhdr;
        memset(&mhdr, 0, sizeof(mhdr));
        mhdr.msg_name = (const caddr_t)&ddr;
        mhdr.msg_namelen = sizeof(sockaddr_in6);
        mhdr.msg_iov = &iov;
        mhdr.msg_iovlen = 1;
        
        ssize_t sz = sendmsg(fd(), &mhdr, 0);
        if( sz < 0){
            LOG_ERROR("sendmsg return %d %s", errno, strerror(errno));
        }
        return sz;
    }
    
    static int EthIcmpSocket(cstr name, byte hwaddr[6])
    {
        int h = socket(PF_INET6, SOCK_RAW, IPPROTO_ICMPV6);
        if(h <= 0) {
            LOG_ERROR("NDP failed to create raw socket");
            return -1;
        }
        
        // bind on interface
        struct ifreq ifr;
        memset(&ifr, 0, sizeof(ifr));
        strncpy(ifr.ifr_name, name, IFNAMSIZ);
        if (setsockopt(h, SOL_SOCKET, SO_BINDTODEVICE, &ifr, sizeof(ifr)) < 0) {
            ::close(h);
            LOG_ERROR("NDP failed to bind %s", name);
            return -1;
        }

        // get hw address
        ioctl(h, SIOCGIFHWADDR, &ifr);
        memcpy(hwaddr, ifr.ifr_hwaddr.sa_data, sizeof(struct ether_addr));
        
        // multicast
        struct ipv6_mreq mreq6;
        mreq6.ipv6mr_interface = if_nametoindex(name);
        NetAddress(ALL_ROUTERS).get((byte*)&mreq6.ipv6mr_multiaddr, sizeof(struct in6_addr));
        setsockopt(h, IPPROTO_IPV6, IPV6_ADD_MEMBERSHIP, &mreq6, sizeof(mreq6));
        NetAddress(ALL_NODES).get((byte*)&mreq6.ipv6mr_multiaddr, sizeof(struct in6_addr));
        setsockopt(h, IPPROTO_IPV6, IPV6_ADD_MEMBERSHIP, &mreq6, sizeof(mreq6));

        // we want send some multicast packet
        int hops = 255;
        if( setsockopt(h, IPPROTO_IPV6, IPV6_MULTICAST_HOPS, &hops, sizeof(hops)) < 0) {
            LOG_ERROR("NDP multicast hops %d %s", errno, strerror(errno));
        }
        setsockopt(h, IPPROTO_IPV6, IPV6_UNICAST_HOPS, &hops, sizeof(hops));
        
        // 
        int on = 1;
        ioctl(h, FIONBIO, (char* )&on);
        
        // we need these icmp packets
        struct icmp6_filter filter;
        ICMP6_FILTER_SETBLOCKALL(&filter);
        ICMP6_FILTER_SETPASS(ND_NEIGHBOR_ADVERT,&filter);
        ICMP6_FILTER_SETPASS(ND_ROUTER_SOLICIT,&filter);

        if( setsockopt(h, IPPROTO_ICMPV6, ICMP6_FILTER, &filter, sizeof(filter)) < 0){
            LOG_ERROR("NDP failed to ICMP6_FILTER %s", name);
        }
        return h;
    }
};


struct IcmpHandler : IFHandler {
    int enable;
    uint16 ratm;
    int reqRA;

    // pending EUIDs requested
    struct PEUID {
        std::set<NetAddress> src;
        uint64 ts;
        uint16 cnt;
        PEUID() {}
        PEUID(const NetAddress& s) : ts(TS::ns()), cnt(0) { src.insert(s); }
    };
    map<NetAddress, PEUID> peuids;

    IcmpHandler(HomeNets* hn, cstr nm, shared_ptr<IFIO> io) : IFHandler(hn, nm, io), reqRA(5) {
        enable = atoi(io->desc.c_str());
        if(enable == 0) enable = ENABLE_RAD;
    }
    
    void OnTick() {
        if(enable & ENABLE_RAD){
            if(TS::expset(ratm, 180)){
                reqRA = 0;
                SendRA();
            }
            if(reqRA){
                reqRA--;
                if(!reqRA){
                    ratm = TS::tm();
                    SendRA();
                }
            }
        }
        for(auto i = peuids.begin(); i != peuids.end(); ){
            if(TS::expset(i->second.ts, MILLISECOND(30000)))
                i = peuids.erase(i);
            else
                i++;
        }
    }

    void EuidSolicit(int64 euid) {
        if((enable & ENABLE_NDP) == 0)return;
        NetAddress t(hn->prefix, sizeof(hn->prefix));
        memcpy(t.addr+8, (byte*)&euid, 8);
        SendNS(t);
    }
    void EuidInvalid(int64 euid) {
        if((enable & ENABLE_NDP) == 0)return;
        euids.erase(euid);
        NetAddress t(hn->prefix, sizeof(hn->prefix));
        memcpy(t.addr+8, (byte*)&euid, 8);
        SendNA(NetAddress("::"), t);
    }

    void OnRecv(const NetAddress& saddr, byte msg[], size_t sz){
        auto icmp6h = (struct icmp6_hdr*)msg;
        if(icmp6h->icmp6_type == ND_NEIGHBOR_SOLICIT){
            if((enable & ENABLE_NDP) == 0)return;
            struct nd_neighbor_solicit*  ns = (struct nd_neighbor_solicit* )icmp6h;
            NetAddress taddr((byte*)&ns->nd_ns_target, sizeof(struct in6_addr));

            if(memcmp(taddr.addr, hn->prefix, sizeof(hn->prefix)) != 0)return;
            LOG_INFO("<%s NS %s %s", name.c_str(), saddr.host().c_str(), taddr.host().c_str());
            
            int64 euid;
            memcpy(&euid, &taddr.addr[8], sizeof(euid));
            string ifname;
            if( hn->GetEuidIf(euid, ifname) ){
                if(ifname != name) SendNA(saddr, taddr);
            } else {
                hn->EuidSolicit(euid, name);
            }
        }
        if(icmp6h->icmp6_type == ND_NEIGHBOR_ADVERT){
            struct nd_neighbor_advert*  na = (struct nd_neighbor_advert* )icmp6h;
            NetAddress taddr((byte*)&na->nd_na_target, sizeof(struct in6_addr));
            if(memcmp(taddr.addr, hn->prefix, 8) != 0)return;
            LOG_INFO("<%s NA %s %s", name.c_str(), saddr.host().c_str(), taddr.host().c_str());
            
            int64 euid;
            memcpy(&euid, &taddr.addr[8], sizeof(euid));
            if(euids.find(euid) == euids.end()){
                IFAddRoute(name.c_str(), (taddr.host() + "/128").c_str(), NULL);
            }
            euids[euid] = TS::tm();
            hn->EuidInvalid(euid, name);
        }
        if(icmp6h->icmp6_type == ND_ROUTER_SOLICIT){
            if((enable & ENABLE_RAD) == 0)return;
            if(reqRA == 0) reqRA = 3;
            LOG_INFO("<%s RS %s", name.c_str(), saddr.host().c_str());
        }
    }

    void SendNS(const NetAddress& taddr)
    {
        byte outmsg[200];
        memset(outmsg, 0, sizeof(outmsg));
        struct nd_neighbor_solicit* ns = (struct nd_neighbor_solicit* )&outmsg[0];

        ns->nd_ns_type           = ND_NEIGHBOR_SOLICIT;

        memcpy(&ns->nd_ns_target, taddr.addr, sizeof(struct in6_addr));

        NetAddress saddr(ALL_NODES);
        struct sockaddr_in6 ddr;
        memset(&ddr, 0, sizeof(ddr));
        ddr.sin6_family = AF_INET6;
        ddr.sin6_port = htons(IPPROTO_ICMPV6);
        memcpy(&ddr.sin6_addr, saddr.addr, sizeof(struct in6_addr));

        LOG_INFO(">%s NS %s", name.c_str(), taddr.host().c_str());

        io->Send(ddr, outmsg, sizeof(struct nd_neighbor_solicit));
    }

    void SendNA(const NetAddress& saddr, const NetAddress& taddr)
    {
        byte outmsg[200];
        memset(outmsg, 0, sizeof(outmsg));
        struct nd_neighbor_advert* na = (struct nd_neighbor_advert* )&outmsg[0];
        struct nd_opt_hdr* opt = (struct nd_opt_hdr* )&outmsg[sizeof(struct nd_neighbor_advert)];
    
        opt->nd_opt_type         = ND_OPT_TARGET_LINKADDR;
        opt->nd_opt_len          = 1;  // means 8 octets
    
        na->nd_na_type           = ND_NEIGHBOR_ADVERT;

        struct sockaddr_in6 ddr;
        memset(&ddr, 0, sizeof(ddr));
        ddr.sin6_family = AF_INET6;
        ddr.sin6_port = htons(IPPROTO_ICMPV6);
        
        if(saddr.isAny()){
            NetAddress(ALL_NODES).get((byte*)&ddr.sin6_addr, sizeof(struct in6_addr));
        } else {
            na->nd_na_flags_reserved = ND_NA_FLAG_SOLICITED;
            memcpy(&ddr.sin6_addr, saddr.addr, sizeof(struct in6_addr));
        }
    
        memcpy(&na->nd_na_target, taddr.addr, sizeof(struct in6_addr));
        memcpy(outmsg + sizeof(struct nd_neighbor_advert) + sizeof(struct nd_opt_hdr), &io->hwaddr, 6);
        
        LOG_INFO(">%s NA %s %s", name.c_str(), saddr.host().c_str(), taddr.host().c_str());

        io->Send(ddr, outmsg, sizeof(struct nd_neighbor_advert) + sizeof(struct nd_opt_hdr) + 6);
    }
    
    
#define  ND_OPT_RDNSS_INFORMATION       25
    struct nd_opt_rdnss_info_local {
        uint8_t nd_opt_rdnssi_type;
        uint8_t nd_opt_rdnssi_len;
        uint16_t nd_opt_rdnssi_pref_flag_reserved;
        uint32_t nd_opt_rdnssi_lifetime;
        struct in6_addr nd_opt_rdnssi_addr1;
        struct in6_addr nd_opt_rdnssi_addr2;
        struct in6_addr nd_opt_rdnssi_addr3;
    };

#define  ND_OPT_DNSSL_INFORMATION       31
    struct nd_opt_dnssl_info_local {
        uint8_t nd_opt_dnssli_type;
        uint8_t nd_opt_dnssli_len;
        uint16_t nd_opt_dnssli_reserved;
        uint32_t nd_opt_dnssli_lifetime;
        unsigned char nd_opt_dnssli_suffixes[];
    };
    
    
    void SendRA()
    {
        byte outmsg[200];
        memset(outmsg, 0, sizeof(outmsg));
        
        // header
        {
            struct nd_router_advert* ra = (struct nd_router_advert*)outmsg;
            ra->nd_ra_type           = ND_ROUTER_ADVERT;
            ra->nd_ra_curhoplimit    = 64;
            ra->nd_ra_router_lifetime= htons(3600);
        }
        int sz = sizeof(struct nd_router_advert);
        
        // Prefix
        {
            struct nd_opt_prefix_info* p = (struct nd_opt_prefix_info*)&outmsg[sz];
            sz += sizeof(*p);
            memset(p, 0, sizeof(*p));
            
            p->nd_opt_pi_type = ND_OPT_PREFIX_INFORMATION;
            p->nd_opt_pi_len = 4;
            p->nd_opt_pi_prefix_len = sizeof(hn->prefix) * 8;
            p->nd_opt_pi_flags_reserved = ND_OPT_PI_FLAG_ONLINK | ND_OPT_PI_FLAG_AUTO;
            p->nd_opt_pi_valid_time = htonl(1200);
            p->nd_opt_pi_preferred_time = htonl(800);
            memcpy(&p->nd_opt_pi_prefix, hn->prefix, sizeof(hn->prefix));
        }
        
        // MTU
        {
            struct nd_opt_mtu* p = (struct nd_opt_mtu*)&outmsg[sz];
            sz += sizeof(*p);
            memset(p, 0, sizeof(*p));
	        p->nd_opt_mtu_type = ND_OPT_MTU;
	        p->nd_opt_mtu_len = 1;
	        p->nd_opt_mtu_mtu = htonl(hn->mtu);
        }
        
        // DNS server
        {
            vector<NetAddress>& dns = *hn->self.attr.dns;
            
            struct nd_opt_rdnss_info_local* p = (struct nd_opt_rdnss_info_local*)&outmsg[sz];

            byte optlen = 1 + 2 * dns.size();
            sz += optlen * 8;
            memset(p, 0, optlen);
            
            p->nd_opt_rdnssi_type = ND_OPT_RDNSS_INFORMATION;
            p->nd_opt_rdnssi_len = optlen;
            p->nd_opt_rdnssi_lifetime = htonl(3600);
            
            in6_addr* paddr = &p->nd_opt_rdnssi_addr1;
            for(auto i : dns){
                memcpy(paddr, i.addr, sizeof(*paddr));
                paddr++;
            }
        }
        
        NetAddress allnodes(ALL_NODES);
        allnodes.port = IPPROTO_ICMPV6;
        
        LOG_INFO("SendRA %s %d %s", name.c_str(), sz, allnodes.str().c_str());
       
        io->Send(allnodes, outmsg, sz);
    }
};



#endif

