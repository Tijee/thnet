/*
 * MIT License
 * Copyright (c) 2016 Tijee Corporation.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of 
 * this software and associated documentation files (the "Software"), to deal in 
 * the Software without restriction, including without limitation the rights to use, 
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject 
 * to the following conditions:

 * The above copyright notice and this permission notice shall be included in all 
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *  
 * This file is part of the thnet project.
 */

#pragma once

#include "../thnet.h"


#define AES_KEY_LEN   16
#define HID_UNKNOWN   0x7fff
#define HID_BACKWARD  0x8000
#define LLID_NONE     0xffff

#define ACTION_NULL    0
#define ACTION_NEWKEY  1
#define ACTION_HOST    2
#define ACTION_LL_NEW  3
#define ACTION_LL_ID   4

#define EUID_TIMEOUT   30
#define LL_TIMEOUT     7200
#define ROOT_TIMEOUT   7200

void random(byte data[], size_t sz);
  
#pragma pack(push, 1)

struct LLPkt {
    uint16 hid;
    uint16 llid;
    uint16 salt;
    uint16 csum;
    
    byte vers[2];
    byte flags[2];
    byte data[8];
    
    void reset()
    {
        memset(&hid, 0, sizeof(uint16)*4);
    }
};


struct IPv6Pkt {
    uint   proto;
    byte   vers[4];
    uint16 len;
    byte   flags[2];
    byte   src[16];
    byte   dest[16];
    byte   data[8];
    
    void bsw(){
        proto = htonl(proto);
        len = htons(len);
    }
};

struct LLPktData : LLPkt {
    byte extdata[1500];
    
    // members for queue only
    uint16 target;
    size_t size;
    
    LLPktData(){
    }
    
    LLPktData(uint16 target_, const LLPkt* pkt, size_t size_) : target(target_), size(size_)
    {
        if(size > sizeof(LLPkt) + sizeof(extdata)){
            size = sizeof(LLPkt) + sizeof(extdata);
        }
        memcpy(&hid, &pkt->hid, size);
    }
};

#pragma pack(pop)

struct LLAddr {
    byte remote[16];  // ip = remote & local 
    byte local[8];
    
    bool operator <(const LLAddr& t) const {
        return memcmp(remote, t.remote, sizeof(LLAddr))<0;
    }
    bool operator ==(const LLAddr& t) const {
        return memcmp(remote, t.remote, sizeof(LLAddr))==0;
    }
};

struct LLData : LLAddr {
    uint16 tm;

    LLData() {}
    LLData(const LLAddr& a) : LLAddr(a), tm(TS::tm()) {}
};

struct LLEntry: LLAddr {
    uint16 llid;
    LLEntry(){}
    LLEntry(uint16 lid, const LLAddr& addr): LLAddr(addr), llid(lid) {
    }
    void bsw() { llid = htons(llid); }
    bool operator <(const LLEntry& t) const {
        return memcmp(remote, t.remote, sizeof(LLAddr))<0;
    }
    bool operator ==(const LLEntry& t) const {
        return memcmp(remote, t.remote, sizeof(LLAddr))==0;
    }
};

struct LLID {
    uint16 hid;
    uint16 llid;
    LLID() {}
    LLID(uint16 hid_, uint16 llid_) : hid(hid_), llid(llid_) {}
    void bsw() { llid = htons(llid); hid = htons(hid); }
    bool operator <(const LLID& t) const {
        return memcmp(&hid, &t.hid, sizeof(LLID))<0;
    }
};


inline Iattr* attrNewHid(uint16& hid){ return new attrNumberRange<uint16>(hid, 0, HID_UNKNOWN-1); }

struct HomeNet;
struct IFHandler;
class HomeNets;

struct attrDictHost : attrDictStrict {
    uint16 hid;
    NetAddress address;
    int lifetime;
    bool bkey;
    byte euid[8];
    byte key[AES_KEY_LEN*2];

    attrDictHost() : bkey(false) {
        bindall();
    }
    attrDictHost(HomeNet* h, bool bkey);

    void iget(DictIattr& v) const override {
        attrDictStrict::iget(v);
        if(!bkey)v.erase("key");
    }

    void bindall(){
        bind("hid", attrNewHid(hid));
        bind("address", new attr<NetAddress, ATTR_STRING>(address));
        bind("euid", new attrBytes(euid, sizeof(euid)));
        bind("lifetime", new attrNumberRange<int>(lifetime, 10, 6*86400));
        bind("key", new attrBytes(key, sizeof(key), &bkey));
    }
    
    bool operator ==(const attrDictHost& t) const {
        return hid == t.hid;
    }
};

struct attrDictLLAddr : attrDictStrict, LLAddr {
    attrDictLLAddr() {
        bindall();
    }
    attrDictLLAddr(const LLAddr& addr) : LLAddr(addr) {
        bindall();
    }
    void bindall(){
        bind("remote", new attrBytes(remote, sizeof(remote)));
        bind("local", new attrBytes(local, sizeof(local)));
    }
    bool operator ==(const attrDictLLAddr& t) const {
        return *((LLAddr*)this) == t;
    }
};

struct attrDictLLEntry : attrDictStrict, LLEntry {
    uint16 hid;

    attrDictLLEntry() {
        bindall();
    }
    attrDictLLEntry(uint16 hid_, const LLEntry& e): LLEntry(e), hid(hid_) {
        bindall();
    }
    void bindall(){
        bind("hid", attrNewHid(hid));
        bind("llid", new attr<uint16, ATTR_NUMBER>(llid));
        bind("remote", new attrBytes(remote, sizeof(remote)));
        bind("local", new attrBytes(local, sizeof(local)));
    }
    bool operator ==(const attrDictLLEntry& t) const {
        return *((LLEntry*)this) == t;
    }
};

struct WSAttr : attrDictStrict {
    uint16 hid;
    NetAddress prefix;
    int  mtu;

    bool bkey;
    byte key[AES_KEY_LEN*2];

    shared_ptr< attrMonoVector<NetAddress, ATTR_STRING> > dns;
    
    shared_ptr< attrSet<attrDictHost> > hosts;
    shared_ptr< attrSet<attrDictLLEntry> > lls;
    shared_ptr< attrSet<attrDictLLAddr> > newlls;
    shared_ptr< attrSet<attrDictLLAddr> > badlls;

    WSAttr() :
        bkey(false),
        dns(new attrMonoVector<NetAddress, ATTR_STRING>()),
        hosts(new attrSet<attrDictHost>()),
        lls(new attrSet<attrDictLLEntry>()),
        newlls(new attrSet<attrDictLLAddr>()),
        badlls(new attrSet<attrDictLLAddr>()) {
        bindall(true);
    }
    void bindall(bool mode){
        if(mode){
            extern int llencrypt;
            
            bind("hid", attrNewHid(hid));
            bind("mtu", new attrNumberRange<int>(mtu, 1380, 1500));
            bind("encrypt", new attrNumberRange<int>(llencrypt, 0, 2) );
            bind("prefix", new attr<NetAddress, ATTR_STRING>(prefix));
            bind("dns", dns);
        } else {
            erase("hid");
            erase("prefix");
            erase("encrypt");
            erase("dns");
            erase("mtu");
        }
        bind("key", new attrBytes(key, sizeof(key), &bkey));
        bind("hosts", hosts);
        bind("lls", lls);
        bind("newlls", newlls);
        bind("badlls", badlls);
    }
    bool havedata() {
        if(hosts->empty() && lls->empty() && newlls->empty() && badlls->empty() && !bkey)return false;
        return true;
    }
    void iget(DictIattr& v) const override {
        attrDictStrict::iget(v);
        if(!bkey)v.erase("key");
        if(hosts->empty())v.erase("hosts");
        if(lls->empty())v.erase("lls");
        if(newlls->empty())v.erase("newlls");
        if(badlls->empty())v.erase("badlls");
    }
    void reset(){
        bkey = false;
        hosts->clear();
        lls->clear();
        newlls->clear();
        badlls->clear();
        bindall(false);
    }
    void set(HomeNets* hn, cstr msg);
};

struct HomeNet {
    WSAttr attr;
    uint16 hid;
    uint16 tm;
    struct sockaddr_in sin;
    byte key[AES_KEY_LEN*2];
    byte keyVer;
    uint16 sintm;
    uint16 keytm;
    int  lifetime; // the seconds of sin validation
    int64 euid;

    map<LLAddr, uint16> ma;
    map<uint16, LLData> mi;
    
    HomeNet(): hid(HID_UNKNOWN), keyVer(0), lifetime(30) {
        tm = keytm = TS::tm();
        sintm = TS::tm(-ROOT_TIMEOUT);
        sin.sin_port = 0;
        random(key, sizeof(key));
    }
 
    void OnTick(bool self) {
        for(auto i = mi.begin(); i != mi.end(); ){
            if(TS::exp(i->second.tm, LL_TIMEOUT)){
                ma.erase(i->second);
                i = mi.erase(i);
            } else
                i++;
        }
    }
    
    bool crypt(LLPkt* pkt, size_t sz) const;
};

struct IFIO : PollFd {
    IFHandler* ifh;
    string desc;
    byte hwaddr[6];
    
    IFIO(IPoll*p, int h_ = -1) : PollFd(p, h_){ }
    
    virtual ssize_t Send(const NetAddress& target, const byte* pkt, size_t sz){return -1;}
    virtual ssize_t Write(const byte* pkt, size_t sz){return -1;}
};

struct IFHandler {
    shared_ptr<IFIO> io;
    HomeNets* hn;
    string name;
    uint16 tm;
    map<int64, uint16> euids;
    int64 euid;
    
    IFHandler(HomeNets* hn_, cstr nm, shared_ptr<IFIO> io_) : io(io_), hn(hn_), name(nm), tm(TS::tm()), euid(0) {
        io->ifh = this;
    }
    
    virtual void EuidSolicit(int64 euid){};
    virtual void EuidInvalid(int64 euid){};
    virtual void OnTick(){};
    virtual void OnRecv(const NetAddress& src, byte msg[], size_t sz) =0;
    
    static IFHandler* New(HomeNets* hn, cstr name, cstr value);
    static IFHandler* New(HomeNets* hn, cstr name, shared_ptr<IFIO> io);
};

struct LLResult: LLEntry {
    uint16 hid;
    bool isnew;
    HomeNet* h;
};
class HomeNets {
    int defgw;
    map<uint16, HomeNet> hnets;

    map<int64, uint16> sins; // udp address -> hid
    list<LLPktData> pkts;

public:
    vector< shared_ptr<IFHandler> > ifs;  // 0 -> tun, 1 -> udp
    
    byte prefix[8];
    HomeNet self;
    int mtu;
    int keepalive;
    int keylife;
    
    HomeNets() : defgw(0), mtu(1500), keepalive(25*60), keylife(24*60*60)  { }

    void Reset(uint16 hid, byte net[8])
    {
        self.mi.clear();
        self.ma.clear();
        self.hid = hid;
        if(hid == 0)self.lifetime = ROOT_TIMEOUT;
        GetEUID((byte*)&self.euid);
        memcpy(prefix, net, sizeof(prefix));
        ifs.clear();
        hnets.clear();
        sins.clear();
        pkts.clear();
    }
    
    bool HaveDefGateway();

    HomeNet* Get(uint16 hid, bool cannew=false);
    bool GetLL(const IPv6Pkt* pkt, LLResult& p);
    
    void WSRecvHost(const attrDictHost& h);
    void WSRecvLL(uint16 hid, const LLEntry& e);
    void WSRecvLLAddr(const LLAddr& addr, bool good);
    void WSRecvAction(uint16 hid, const byte* msg, size_t sz);

    void OnTick();
    void WriteTun(HomeNet*h, const LLPkt* pkt, int sz);
    
    void QueuePkt(uint16 target, const LLPkt* pkt, size_t size)
    {
        pkts.push_back(LLPktData(target, pkt, size));
    }
    
    bool SendQueuedPkts(HomeNet* h)
    {
        int scnt = 0;
        for(auto p = pkts.begin(); p != pkts.end(); ){
            if(p->target == h->hid){
                SendUdp(h->sin, &*p, p->size);
                p = pkts.erase(p);
                scnt++;
            } else {
                p++;
            }
        }
        return scnt > 0;
    }
    
    void AddrUpdate(HomeNet* h, const sockaddr_in&sin, bool backward)
    {
        int64 n = sinkey(sin);
        if(backward){
            auto p = sins.find(n);
            if(p == sins.end())return;
            h = Get(p->second);
            if(h)h->sintm = TS::tm();
        } else {
            h->sintm = TS::tm();
            int64 o = sinkey(h->sin);
            if(o != n){
                sins[n] = h->hid;
                h->sin = sin;
                OnAddrUpdated(h);
            }
        }
        self.sintm = TS::tm();
    }

    void EuidSolicit(int64 euid, const string& exclude){
        for(auto p : ifs){
            if( p->name == exclude)continue;
            p->EuidSolicit(euid);
        }
    }
    
    void EuidInvalid(int64 euid, const string& exclude){
        for(auto p : ifs){
            if( p->name == exclude)continue;
            p->EuidInvalid(euid);
        }
    }

    bool GetEuidIf(int64 euid, string& name){
        for(auto p : ifs) {
            auto i = p->euids.find(euid);
            if(i != p->euids.end() && !TS::exp(i->second, EUID_TIMEOUT)){
                name = p->name;
                return true;
            }
        }
        return false;
    }
    
    bool SendUdp(const sockaddr_in& sin, LLPkt* pkt, size_t sz){
        if(ifs.size()>=2 && ifs[1] && ifs[1]->io){
            ifs[1]->io->Send(NetAddress(sin), (const byte*)pkt, sz);
            return true;
        }
        return false;
    }
    
    virtual void QueueAction(byte action, const void* msg) =0;
    virtual void QueueAttr(uint16 hid) =0;
    
    virtual bool IsLocalServer(int64 euid) =0;
    virtual void OnAddrUpdated(HomeNet* h) =0;
};





