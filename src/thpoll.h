/*
 * MIT License
 * Copyright (c) 2016 Tijee Corporation.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of 
 * this software and associated documentation files (the "Software"), to deal in 
 * the Software without restriction, including without limitation the rights to use, 
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject 
 * to the following conditions:

 * The above copyright notice and this permission notice shall be included in all 
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *  
 * This file is part of the thnet project.
 */

#pragma once

#include <poll.h>


struct CriticalSection {
    pthread_mutex_t mutex;
    CriticalSection(bool recursive = false) : mutex( Template(recursive) ){
    }
    ~CriticalSection()
    {
    }
    
    void Lock()
    {
        pthread_mutex_lock(&mutex);
    }
    
    void Unlock()
    {
        pthread_mutex_unlock(&mutex);
    }
    
private:
    static const pthread_mutex_t & Template(bool recursive)
    {
        static pthread_mutex_t tdefault = PTHREAD_MUTEX_INITIALIZER;
        static pthread_mutex_t trecursive = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP;
        if(recursive)return trecursive;
        return tdefault;
    } 
};

struct CriticalSectionLocker {
    CriticalSectionLocker(CriticalSection & cs) : mutex(cs.mutex)
    {
        pthread_mutex_lock(&mutex);
    }
    CriticalSectionLocker(pthread_mutex_t & cs) : mutex(cs)
    {
        pthread_mutex_lock(&mutex);
    }
    ~CriticalSectionLocker()
    {
        pthread_mutex_unlock(&mutex);
    }
private:
    pthread_mutex_t& mutex;
};


struct PollFd;
struct IPoll {
    virtual void OnPoll(short evt, PollFd* p) =0;
    virtual void OnClose(PollFd* p) {};
};


struct PollFd {
    PollFd(IPoll*p, int h_ = -1) : t(NULL), ip(p)  {
        setfd(h_);
    }
    
    virtual ~PollFd() {
        close();
    }
        
    int fd() const {
        if(!t)return -1;
        return t->fd;
    }

    virtual void setfd(int nh);
    virtual void mode(short evt = POLLIN);

    void setcb(IPoll* p){
        ip = p;
    }
    
    void close() {
        setfd(-1);
    }

    bool readLines(char buf[], int size, vector<char*>& lines);
    
protected:
    friend struct PollLoop;
    
    struct pollfd* t;
    IPoll* ip;

    virtual void OnPoll(short m) {
        if(ip)ip->OnPoll(m, this);
    }
};

struct ICall {
    virtual ~ICall() {}
    virtual void Call() =0;
};

struct ITimer {
    ITimer() : active(false), next(NULL) {
    }
    virtual ~ITimer() {
        stop();
    }
    
    virtual void Call() =0;
    
    void start(unsigned ms, unsigned repeat);
    void stop();
    bool isactive(){
        return active;
    }
protected:
    friend struct PollLoop;

    unsigned ts;
    unsigned period;
    bool active;
    ITimer* next;
};


template<class B, class T>
struct ClassMember : B {
    typedef void (T::*Tfunc)();

    ClassMember(T* t_, Tfunc func_): t(t_), func(func_) {
    }
    
    void Call(){
        (t->*func)();
    }
private:
    T* t;
    Tfunc func;
};

template<class T>
struct ClassMemberTimer : ClassMember<ITimer, T> {
    typedef void (T::*Tfunc)();
    ClassMemberTimer(T* t_, Tfunc func_): ClassMember<ITimer, T>(t_, func_) {
    }
    ClassMemberTimer(unsigned ms, unsigned repeat, T* t_, Tfunc func_): ClassMember<ITimer, T>(t_, func_)
    {
        ITimer::start(ms, repeat);
    }
};    

template<class T>
struct ClassMemberCall : ClassMember<ICall, T> {
    typedef void (T::*Tfunc)();
    ClassMemberCall(T* t_, Tfunc func_): ClassMember<ICall, T>(t_, func_) {
    }
};

#define MAX_FD 256
struct ICtrlRecv {
    virtual void OnCtrlRecv(const vector<char*>& lines) =0;
};


struct PollLoop {
    static PollLoop* instance;
    const pthread_t ptmain;
    cstr appname;
    char* argv0;

    PollLoop(char*argv0);
    ~PollLoop();
    void Run(ICtrlRecv* rcv, unsigned ms =0);  // zero means running forever
    void Stop() { running = false; }

protected:
    friend class PollFd;
    friend class ITimer;
    
    bool FdAttach(PollFd* pfd, int h);
    void TimerAttach(ITimer*t);
    void TimerDetach(ITimer*t);
    
    struct pollfd pollfds[MAX_FD];
    vector<PollFd*> fds;
    ITimer* timer;
    
    bool running;
    CriticalSection cs;
};

struct CtrlMsg {
    bool isreply;
    cstr peer;
    cstr type;
    cstr id;
    shared_ptr<attrDict> data;
    
    CtrlMsg(char* line);
    CtrlMsg(cstr p, cstr t, cstr i) : isreply(false), peer(p), type(t), id(i), data(new attrDict()) { }
    
    operator bool() const {
        return data.get() != NULL;
    }
    
    bool Send(int fd=1, char ch=1) const;
    
    bool Reply(int fd=1) const { 
        if(isreply || !strcmp(id, "all"))return false;
        return Send(fd, 2); 
    }
    
    bool ReplyError(const string& str, int fd=1){
        data->clear();
        data->bind("error", str);
        return Reply(fd);
    }
    bool ReplyResult(const string& str, int fd=1){
        data->clear();
        data->bind("result", str);
        return Reply(fd);
    }
};


