#include "thnet.h"

CtrlMsg::CtrlMsg(char* line) : isreply(false), peer(NULL), type(NULL), id(NULL)
{
    if(*line > 2)return;
    isreply = *line == 2;
    line++;
    peer = line;

    while(*line && *line != '!')line++;
    if(!*line)return;
    *line = 0;
    line++;
    type = line;
    
    while(*line && *line != '!')line++;
    if(!*line)return;
    *line = 0;
    line++;
    id = line;

    while(*line && *line != '!')line++;
    if(!*line)return;
    *line = 0;
    line++;

    string err;    
    data.reset(new attrDict());
    if(! data->set(line, err) ){
        LOG_DEBUG("CtrlMsg (%s %s) invalid param \"%s\" for %s", peer, type, line, err.c_str());
        data.reset();
    }
}

bool CtrlMsg::Send(int fd, char ch) const
{
    if(!data)return false;
    
    ostringstream os;
    os << ch << peer << "!" << type << "!" << id << "!";
    ((Iattr*)data.get())->get(os);
    os << "\n";
    
    
    string str = os.str();
    ignore_result( write(fd, str.c_str(), str.size()) );
    //LOG_INFO("cm %d %s", fd, str.c_str());
    return true;
}


void PollFd::mode(short evt)
{
    if(!t)return;
    t->events = evt;
}

void PollFd::setfd(int nh)
{
    if(t){
        if(nh == t->fd)return;
        ::close(t->fd);
        t->fd = -1;
        t = NULL;
        
        if(ip)ip->OnClose(this);
    }
    if(nh >= 0){
        ASSERT( PollLoop::instance );
        PollLoop::instance->FdAttach(this, nh);
    }
}

bool PollFd::readLines(char buf[], int size, vector<char*>& lines)
{
    int rsz = 0;
    if( ioctl(fd(), FIONREAD, &rsz) < 0 )return false;
    rsz = min(rsz, size-2);
    rsz = read(fd(), buf, rsz);
    char* begin = buf;
    for(int i=0; i<rsz; i++)if(buf[i] == '\n'){
        if(i-(begin-buf)>4){
            buf[i] = 0;
            lines.push_back(begin);
        }
        begin = &buf[i+1];
    }
    return lines.size() > 0;
}

void ITimer::start(unsigned ms, unsigned rr)
{
    ASSERT( PollLoop::instance );
    if(active) PollLoop::instance->TimerDetach(this);
    ts = ms + (unsigned)( TS::ns() / MILLISECOND(1) );
    period = rr;
    PollLoop::instance->TimerAttach(this);
}

void ITimer::stop()
{
    if(!active)return;
    ASSERT( PollLoop::instance );
    PollLoop::instance->TimerDetach(this);
}

static void daemonSignal(int signum)
{
    if(PollLoop::instance)
        PollLoop::instance->Stop();
    signal(SIGINT, exit);
}

static void noneSignal(int signum)
{
}

PollLoop* PollLoop::instance = NULL;
PollLoop::PollLoop(char*a) : ptmain(pthread_self()), appname("tjapps"), argv0(a), timer(NULL), running(true)
{
    ASSERT(instance == NULL);
    instance = this;
    
    signal(SIGUSR1, SIG_IGN);
    signal(SIGPIPE, SIG_IGN);
    signal(SIGCHLD, noneSignal);  // SIG_IGN will erase the child's exit code
    signal(SIGINT, daemonSignal);
    signal(SIGTERM, daemonSignal);
    
    TS::update();
}

PollLoop::~PollLoop()
{
    instance = NULL;
}

bool PollLoop::FdAttach(PollFd* fd, int h)
{
    CriticalSectionLocker lkr(cs);
    
    unsigned idx = fds.size();
    for(unsigned i=0; i<idx; i++){
        auto p = &pollfds[i];
        if(p->fd < 0){
            p->fd = h;
            p->events = POLLIN;
            fds[i] = fd;
            fd->t = p;
            return true;
        }
    }
    
    if(idx >= MAX_FD)return false;
    
    pollfds[idx].fd = h;
    pollfds[idx].events = POLLIN;
    fd->t = &pollfds[idx];
    fds.push_back(fd);
    return true;
}

void PollLoop::TimerAttach(ITimer*t)
{
    CriticalSectionLocker lkr(cs);
    ASSERT(t->active == false);
    
    ITimer** p = &timer;
    while(*p){
        if(diff(t->ts, (*p)->ts) < 0)break;
        p = &((*p)->next);
    }
    t->next = *p;
    *p = t;
    t->active = true;
}

void PollLoop::TimerDetach(ITimer*t)
{
    CriticalSectionLocker lkr(cs);
    ASSERT(t->active == true);
    
    ITimer** p = &timer;
    while(*p){
        if(*p == t){
            *p = t->next;
            t->next = NULL;
            t->active = false;
            return;
        }
        p = &((*p)->next);
    }
    ASSERT(false);
}


struct PollInput : PollFd {
    ICtrlRecv* rcv;
    PollInput(ICtrlRecv* r) : PollFd(NULL, 0), rcv(r) {
    }
    
    void OnPoll(short evt) override {
        if(evt & POLLHUP){
            LOG_ERROR("ctrl/stdin disconnected");
            PollLoop::instance->Stop();
            return;
        }
        
        char buf[1300];
        vector<char*> lines;
        if(readLines(buf, sizeof(buf), lines))rcv->OnCtrlRecv(lines);
    }
};


void PollLoop::Run(ICtrlRecv* rcv, unsigned rtm)
{
    shared_ptr<PollInput> in;
    if(rcv)in.reset(new PollInput(rcv));
    
    TS::update();
    unsigned ts = (unsigned)( TS::ns() / MILLISECOND(1) );
    if(rtm != 0) rtm += ts;
    
    while(running){
        int ms = 1000;
        if(rtm != 0){
            ms = min(ms, diff(rtm, ts));
            if(ms < 0)break;
        }
        cs.Lock();
        if(timer){
            ms = min(ms, max( diff(timer->ts, ts), 0 ));
        }
        cs.Unlock();

        unsigned sz = fds.size();
        poll(pollfds, sz, ms);
        
        TS::update();
        ts = (unsigned)( TS::ns() / MILLISECOND(1) );
        
        for(unsigned i=0; i<sz; i++)
        {
            if(!pollfds[i].revents || pollfds[i].fd < 0)continue;
            fds[i]->OnPoll(pollfds[i].revents);
        }

        // call delay task
        list< ITimer* > tt;
        
        cs.Lock();
        while(timer){
            ITimer* t = timer;
            if(diff(t->ts, ts) >= 0)break;
            
            tt.push_back(t);
            timer = t->next;
            
            if(t->period){
                t->ts += t->period;
                
                ITimer** p = &timer;
                while(*p){
                    if(diff(t->ts, (*p)->ts) < 0)break;
                    p = &((*p)->next);
                }
                t->next = *p;
                *p = t;
                //LOG_DEBUG("ITask 2 %p next=%u", t.get(), t->next);
            } else {
                t->active = false;
            }
        }
        cs.Unlock();
        
        for(auto i = tt.begin(); i!= tt.end(); i++){
            (*i)->Call();
        }
    }
}



