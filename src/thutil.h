/*
 * MIT License
 * Copyright (c) 2016 Tijee Corporation.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of 
 * this software and associated documentation files (the "Software"), to deal in 
 * the Software without restriction, including without limitation the rights to use, 
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject 
 * to the following conditions:

 * The above copyright notice and this permission notice shall be included in all 
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *  
 * This file is part of the thnet project.
 */

#pragma once

typedef unsigned short uint16;
typedef unsigned int uint32;
typedef unsigned long long uint64;
typedef short int16;
typedef int int32;
typedef long long int64;
typedef const char* cstr;
typedef const void* handle;
typedef unsigned char byte;

#define LOG_DEBUG(msg, ...)    TJlog('D', msg, ##__VA_ARGS__) 
#define LOG_INFO(msg, ...)     TJlog('I', msg, ##__VA_ARGS__) 
#define LOG_WARN(msg, ...)     TJlog('W', msg, ##__VA_ARGS__) 
#define LOG_ERROR(msg, ...)    TJlog('E', msg, ##__VA_ARGS__) 
#define ASSERT(x)  if(!(x)){ LOG_ERROR("ASSERT \"" #x "\" failed @" __FILE__ ":%d", __LINE__); abort(); }

#define MILLISECOND(x)         ((uint64)(x) * 1000 * 1000)

namespace TS {
    uint64 ns();
    void update();
    uint16 tm(int seconds = 0);
    bool expset(uint64& ts, int64 ns);
    bool expset(uint16& tm, int seconds);
    bool exp(uint64 ts, int64 ns);
    bool exp(uint16 tm, int seconds);
};

inline int64 diff(uint64 a, uint64 b) { return (int64)(a-b); }
inline int diff(uint32 a, uint32 b) { return (int)(a-b); }

template<typename T>
inline T ignore_result(T x __attribute__((unused)))
{
    return x;
}

struct NetAddress {
    NetAddress() : alen(0), port(0) {}
    NetAddress(const struct sockaddr* sa) { set(sa); }
    NetAddress(const struct sockaddr_in& sin) { set(sin); }
    NetAddress(const struct sockaddr_in6& sin) { set(sin); }
    NetAddress(cstr addr) { set(addr); }
    NetAddress(const string& addr) { set(addr); }
    NetAddress(const byte* addr, byte w) { set(addr, w); }
    
    string host() const;
    string str() const { ostringstream os; get(os); return os.str(); }
    bool adapt(const struct sockaddr* sa) const;
    void get(ostream& os) const ;
    byte get(byte* pb, byte w) const ;
    bool get(struct sockaddr* sa, socklen_t& slen) const ;
    struct sockaddr_in get4() const ;
    struct sockaddr_in6 get6() const ;
    bool set(const struct sockaddr* sa);
    bool set(const byte* addr, byte w);
    bool set(cstr addr);
    bool set(const string& addr) { return set(addr.c_str()); }
    bool resolve(cstr addr);
    bool isAny() const ;
    bool isLoopback() const ;
    bool isLocal() const ;
    bool isMulticast() const ;

    bool set(const struct sockaddr_in6& sin) { return set((struct sockaddr*)&sin); }
    bool set(const struct sockaddr_in& sin) { return set((struct sockaddr*)&sin); }
    
    bool operator == (const NetAddress& t) const {
        return t.alen == alen && !memcmp(t.addr, addr, alen) && t.port == port;
    }
    bool operator != (const NetAddress& t) const {
        return t.alen != alen || memcmp(t.addr, addr, alen) || t.port != port;
    }
    bool operator <(const NetAddress& t) const {
        return memcmp(&alen, &t.alen, sizeof(NetAddress)) < 0;
    }
    
    uint16 alen;
    uint16 port;
    byte addr[16];
};

typedef vector<NetAddress> NetAddresses;

template<class ContainerT>
inline void tokenize(const std::string& str, ContainerT& tokens,
              const std::string& delimiters = " ", bool trimEmpty = false)
{
   std::string::size_type pos, lastPos = 0;

   using value_type = typename ContainerT::value_type;
   using size_type  = typename ContainerT::size_type;

   tokens.clear();
   while(true)
   {
      pos = str.find_first_of(delimiters, lastPos);
      if(pos == std::string::npos)
      {
         pos = str.length();

         if(pos != lastPos || !trimEmpty)
            tokens.push_back(value_type(str.data()+lastPos,
                  (size_type)pos-lastPos ));

         break;
      }
      else
      {
         if(pos != lastPos || !trimEmpty)
            tokens.push_back(value_type(str.data()+lastPos,
                  (size_type)pos-lastPos ));
      }

      lastPos = pos + 1;
   }
}


void rstrip(char *str);
size_t fromhex(cstr txt, byte* data);
string hex(const byte*data, size_t sz);
string base64(const byte*data, size_t sz);
void show(cstr msg, const byte* data, size_t msz);


bool GetGateway(int family, char* intf, NetAddress* next);
string GetEUID();
string GetHWID();
bool GetEUID(byte euid[8]);
int ReadFile(cstr name, void* buf, unsigned sz);
int WriteFile(cstr name, const void* buf, unsigned sz);
int SingleApplication(cstr name, bool doset);
void TJlog(char type, cstr fmt, ...);
bool IFGetMac(cstr iface, byte mac[6]);
bool IFGetIP(cstr iface, vector<NetAddress>& addrs);
bool IFSetIP(cstr iface, const vector<NetAddress>& addrs);
bool IFAddRoute(cstr iface, cstr target, cstr via);
bool IFDelRoute(cstr iface, cstr target, cstr via);

inline int ReadFile(cstr name, int* v){ return ReadFile(name, v, sizeof(int)); }
inline int WriteFile(cstr name, int v){ return WriteFile(name, &v, sizeof(int)); }
inline int WriteFile(cstr name, cstr str){ return WriteFile(name, str, strlen(str)); }

inline int64 sinkey(const sockaddr_in& sin) {
    return ((int64)sin.sin_addr.s_addr << 16) + sin.sin_port;
}

