/*
 * MIT License
 * Copyright (c) 2016 Tijee Corporation.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of 
 * this software and associated documentation files (the "Software"), to deal in 
 * the Software without restriction, including without limitation the rights to use, 
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject 
 * to the following conditions:

 * The above copyright notice and this permission notice shall be included in all 
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *  
 * This file is part of the thnet project.
 */

#pragma once

namespace inner {

template<class T> 
struct EqsParse
{
    EqsParse(cstr text, T& t){
        cstr k = NULL;
        cstr v = NULL;
        cstr p = text;
        while(1){
            if(k == NULL && strchr("\r\n\t ", *p) == NULL) k=p;
            if(k && v == NULL && *p == '=')v=p+1;
            if(v && (*p == '\r' || *p == '\n' || *p == '&' || *p == 0)){
                if(*k != '#')t.set( string(k, v-1), string(v, p) );
                k = v = NULL;
            }
            if(*p == 0)break;
            p++;
        }
    }
};

struct stringpairs : vector< pair<string, string> > {
    void set(const string& n, const string& v){
        push_back(make_pair(n, v));
    }
};

struct stringmap : map<string, string> {
    void set(const string& n, const string& v){
        (*this)[n] = v;
    }
    string get(const string& n, const string& def = "") const
    {
        auto i = find(n);
        if(i == end())return def;
        return i->second;
    }
};


template<class T>
struct stringpairbase : T {
    stringpairbase() {}
    stringpairbase(cstr text){
        str(text);
    }
    bool str(cstr text){
        if(text == NULL)return false;
        EqsParse<T>(text, *this);
        return true;
    }
    string str() const {
        stringbuf text;
        ostream os (&text);
        for(auto i : *this){
            os << i.first << "=" << i.second << "\n";
        }
        return text.str();
    }
};

}; // namespace

typedef inner::stringpairbase<inner::stringpairs> stringpairs;
typedef inner::stringpairbase<inner::stringmap> stringmap;

inline ostream& jsonstr(ostream& os, const string& ns) {
    os << "\""; 
    for(auto n : ns){
        if(n == '"' || n == '\\') os << '\\';
        os << n;
    }
    return os << "\""; 
}

enum ATTR_TYPE {
    ATTR_NULL,
    ATTR_NUMBER,
    ATTR_STRING,
    ATTR_BOOLEAN,
    ATTR_ARRAY,
    ATTR_DICT,
};

struct attrToken {
    ATTR_TYPE type;
    string str;
    
    attrToken() : type(ATTR_NULL) {}
    attrToken(ATTR_TYPE t, const string& s) : type(t), str(s) {}
};

class attrTokens {
    unsigned idx;
    ostringstream err;
    vector<attrToken> data;
    
public:
    attrTokens() : idx(0) {
    }
    
    attrTokens(istream& is) : idx(0) {
        addall(is);
    }

    
    ostringstream& error()
    {
        unsigned i = 0;
        if(idx >= 2) i = idx - 2;
        if(err.tellp() > 0) err << "; ";
        err << "@";
        while(i < idx){
            err << "'" << data[i++].str << "'";
            if(i < idx) err << " ";
        }
        err << ": ";
        return err;
    }
    
    void error(const string& str)
    {
        err << str;
    }
    
    bool geterror(string& e)
    {
        if(err.tellp() == 0) return false;
        e = err.str();
        return true;
    }

    bool good() {
        return err.tellp() == 0;
    }

    attrToken* peek(){
        if(idx >= data.size())return NULL;
        return &data[idx];
    }

    attrToken* get(){
        if(idx >= data.size())return NULL;
        return &data[idx++];
    }

    void addall(istream& is) {
        while(!is.eof())add(is);
    }
    
    void add(istream& is) {
        attrToken a;
        a.type = get(is, a.str);
        data.push_back(a);
    }
    
    void add(ATTR_TYPE type, const string& str)
    {
        data.push_back(attrToken(type, str));
    }

    static ATTR_TYPE get(istream& is, string& str) {
        int  quote = 0;
        char ch = 0;
        ostringstream ostr;

        while(true){
            if(!is.get(ch))return ATTR_NULL;
            if(ch == '"' || ch == '\''){ quote=1; break; }
            if(isalnum(ch)){ ostr << ch; break; }
            if(ch == '{' || ch == '}'){ str = ch; return ATTR_DICT; }
            if(ch == '[' || ch == ']'){ str = ch; return ATTR_ARRAY; }
        }
        while(true){
            if(!is.get(ch))break;
            if(ch == '\\'){
                quote = 3 - quote; 
                if(quote == 1) ostr << ch;
                continue; 
            }
            if(ch == '"' || ch == '\''){ 
                quote--;
                if(quote <= 0){ str = ostr.str(); return ATTR_STRING; }
                ostr << ch;
                continue;
            }
            if(quote == 2){
                quote--;
                if(ch == 't')ostr << '\t';
                else if(ch == 'r')ostr << '\r';
                else if(ch == 'n')ostr << '\n';
                else if(ch == 'b')ostr << '\b';
                else if(ch == 'x'){
                    char hex[3];
                    is.get(hex, 2);
                    hex[2] = 0;
                    int v;
                    sscanf(hex, "%x", &v);
                    ostr << (char)v;
                }
                else 
                    ostr << ch;
                continue;
            }
            if(quote == 1){
                ostr << ch;
                continue;
            }
            if(isalnum(ch)){ 
                ostr << ch; 
                continue;
            }
            is.unget();
            break;
        }
        str = ostr.str();
        if(str == "" || str == "null" || str == "Null")return ATTR_NULL;
        if(isdigit(str[0]))return ATTR_NUMBER;
        if(str == "true" || str == "True"){
            str = "1";
            return ATTR_BOOLEAN;
        }
        if(str == "false" || str == "False"){
            str = "0";
            return ATTR_BOOLEAN;
        }
        return ATTR_STRING;
    }

};
/////////////////////
template<typename T, ATTR_TYPE type> struct IattrGet ;
template<typename T>
struct IattrGet<T, ATTR_NULL> {
    IattrGet(ostream& os, const T& t){ os << "null"; }
};

template<typename T>
struct IattrGet<T, ATTR_STRING> {
    IattrGet(ostream& os, const T& t){ 
        ostringstream s;
        s << t;
        jsonstr(os, s.str());
    }
};

template<typename T>
struct IattrGet<T, ATTR_NUMBER> {
    IattrGet(ostream& os, const T& t){ os << t; }
};

template<typename T> 
struct IattrGet<T, ATTR_BOOLEAN> {
    IattrGet(ostream& os, const T& t){ os << (t ? "true" : "false"); }
};

/////////////////////
template<typename T, ATTR_TYPE type> 
struct IattrSet {
    bool ok;
    IattrSet(attrTokens& tks, T& t): ok(false){
        attrToken*a = tks.get();
        if(!a)return;
        if(a->type != type){ tks.error() << "type incompatible"; return; }
        istringstream is(a->str);
        is >> t;
        ok = !is.fail() || is.eof(); 
    }
    operator bool() const { return ok; }
};

template<> 
struct IattrSet<string, ATTR_STRING> {
    bool ok;
    IattrSet(attrTokens& tks, string& t): ok(false){
        attrToken*a = tks.get();
        if(!a)return;
        if(a->type != ATTR_STRING){ tks.error() << "type incompatible"; return; }
        t = a->str;
        ok = true; 
    }
    operator bool() const { return ok; }
};

//////////////////////
struct Iattr {
    ATTR_TYPE type;

    Iattr(ATTR_TYPE t = ATTR_NULL) : type(t) {}

    virtual ~Iattr(){}
    virtual void get(ostream&) const =0;
    virtual void set(attrTokens&) =0;
private:
    bool operator == (const Iattr & t) const;
};

inline ostream& operator<<(ostream& os, const Iattr& a) { a.get(os); return os; }

template<typename T, ATTR_TYPE TYPE, bool v> struct attr;

template<typename T, ATTR_TYPE TYPE, bool readonly=false>
struct attr : Iattr {
    T& t;
    bool* nb;
    attr(T& t_, bool* nb_ = NULL) : Iattr(TYPE), t(t_), nb(nb_) {}
    void get(ostream& o) const { IattrGet<T, TYPE>(o, t); }
    void set(attrTokens& tks) { 
        T nt;
        IattrSet<T, TYPE> is(tks, nt);
        if(!is)tks.error() << "failed";
        if(nt != t && nb)*nb=true; 
        t = nt;
    }
};


template<typename T, ATTR_TYPE TYPE>
struct attr<T, TYPE, true> : Iattr {
    const T& t;
    attr(const T& t_) : Iattr(TYPE), t(t_) {}
    void get(ostream& o) const { IattrGet<T, TYPE>(o, t); }
    void set(attrTokens& i) { i.error() << "readonly"; }
};

template<typename T, ATTR_TYPE TYPE>
struct attrc : Iattr {
    T ct;
    attrc(const T& t) : Iattr(TYPE), ct(t) {}
    void get(ostream& o) const { IattrGet<T, TYPE>(o, ct); }
    void set(attrTokens& i) { i.error() << "readonly"; }
};

template<typename T, ATTR_TYPE TYPE>
struct attrh : Iattr {
    T t;
    attrh() : Iattr(TYPE) {}
    void get(ostream& o) const { IattrGet<T, TYPE>(o, t); }
    void set(attrTokens& tks) {
        IattrSet<T, TYPE> is(tks, t);
        if(!is)tks.error() << "failed";
    }
};

template<typename T>
struct attrNumberRange : Iattr {
    T& t;
    const T tmin, tmax;
    bool* nb;
    attrNumberRange(T& t_, const T& tmin_, const T& tmax_, bool* nb_ = NULL) : Iattr(ATTR_NUMBER), t(t_), tmin(tmin_), tmax(tmax_), nb(nb_) {}
    void get(ostream& o) const { o << t; }
    void set(attrTokens& tks) { 
        T nt = T();
        IattrSet<T, ATTR_NUMBER> is(tks, nt);
        if(!is)tks.error() << "not number";
        if(nt < tmin || nt > tmax){
            tks.error() << "not in range [" << tmin << "," << tmax << "]";
            return;
        }
        if(t != nt && nb)*nb = true;
        t = nt;
    }
};


template<typename T, ATTR_TYPE TYPE>
struct attrf : Iattr {
    typedef T (*fget)();
    typedef void (*fset)(const T&);
    fget pget;
    fset pset;
    attrf(fget pget_, fset pset_) : Iattr(TYPE), pget(pget_), pset(pset_) {}
    void get(ostream& o) const { T t=(*pget)(); IattrGet<T, TYPE>(o, t); }
    void set(attrTokens& tks) { 
        if(!pset){ tks.error() << "readonly"; return; } 
        T t; 
        IattrSet<T,TYPE> is(tks, t); 
        if(!is)tks.error() << "failed"; else (*pset)(t); 
    }
};

template<typename T, ATTR_TYPE TYPE, typename C>
struct attrmf : Iattr {
    typedef T (C::*mfget)();
    typedef void (C::*mfset)(const T&);
    C* c;
    mfget pget;
    mfset pset;
    attrmf(C* c_, mfget pget_, mfset pset_) : Iattr(TYPE), c(c_), pget(pget_), pset(pset_) {}
    void get(ostream& o) const { T t=(c->*pget)(); IattrGet<T, TYPE>(o, t); }
    void set(attrTokens& tks) { 
        if(!pset){ tks.error() << "readonly"; return; } 
        T t; 
        IattrSet<T, TYPE> is(tks, t); 
        if(!is)tks.error() << "failed"; else (c->*pset)(t); 
    }
};

struct attrBytes : Iattr {
    byte* pb;
    unsigned size;
    unsigned capacity;
    bool* nb;
    attrBytes(byte* p, unsigned cap, bool* nb_=NULL) : Iattr(ATTR_STRING), pb(p), capacity(cap), nb(nb_) {
        size = cap;
    }
    void get(ostream& o) const { o << '"' << hex(pb, size) << '"'; }
    void set(attrTokens& tks) { 
        auto p = tks.get(); 
        if(!p) { tks.error() << "no data"; return; }
        if(p->str.size() > 2*capacity){ tks.error() << "too many data"; return; }
        if(nb)*nb = true; 
        size = fromhex(p->str.c_str(), pb);
    }
};


//////////////////////////////////////////////////////////////////////
typedef vector< shared_ptr<Iattr> > VectorIattr;
struct IattrArray : Iattr {
    virtual bool iget(unsigned idx, ostream& o) const =0;
    virtual bool clear() =0;
    virtual shared_ptr<Iattr> add(const attrToken& hint) =0;
    IattrArray() : Iattr(ATTR_ARRAY) {}
    void get(ostream& o) const { 
        o << "[";
        
        unsigned i = 0;
        while(true) {
            if(i>0) o << ", "; 
            if(! iget(i, o) ){
                if(i > 0)o.seekp(-2, ios_base::cur);
                break;
            }
            i++;
        }
        o << "]";
    }
    void set(attrTokens& tks) { 
        auto a = tks.get();
        if(!a || a->type != type || a->str != "["){
            tks.error() << "input is not array"; 
            return;
        }
        if(!clear()){
            tks.error() << "readonly";
            return;
        }
        while(true){
            a = tks.peek();
            if(a == NULL || (a->type == type && a->str == "]")) {
                tks.get();
                break;
            }
            auto p = add(*a);
            if(p)
                p->set(tks);
            else 
                tks.error() << "set array element error";
            if(!tks.good())break;
        }
    }
private:
    bool operator ==(const IattrArray&t) const ;
};


template<class T>
struct attrVector : IattrArray, vector< shared_ptr<T> > {
    bool *nb;
    
    attrVector(bool* nb_ = NULL) : nb(nb_) {}

    void push_back(shared_ptr<T> p) {
        vector< shared_ptr<T> >::push_back(p);
    }

    void push_back(T* p) {
        vector< shared_ptr<T> >::push_back(shared_ptr<T>(p));
    }
    
    bool iget(unsigned idx, ostream& o) const {
        if(idx>= vector< shared_ptr<T> >::size())return false;
        (*this)[idx]->get(o);
        return true;
    }
    bool clear() {
        vector< shared_ptr<T> >::clear();
        return true;
    }
    shared_ptr<Iattr> add(const attrToken& hint){
        if(hint.type != ATTR_DICT)return NULL;
        shared_ptr<T> p( new T() );
        push_back( p );
        return p;
    }
};

template<>
struct attrVector<Iattr> : IattrArray, vector< shared_ptr<Iattr> > {
    bool *nb;

    attrVector(bool* nb_ = NULL) : nb(nb_) {}

    bool iget(unsigned idx, ostream& o) const {
        if(idx>= vector< shared_ptr<Iattr> >::size())return false;
        (*this)[idx]->get(o);
        return true;
    }
    bool clear() { return true; }
    shared_ptr<Iattr> add(const attrToken& hint);
};

template<class T>
struct attrSet : attrVector<T> {
    attrSet(bool* nb_ = NULL) : attrVector<T>(nb_) {
    }

    void insert(shared_ptr<T> p) {
        if(!p)return;
        for(auto i : *this){
            if(*i == *p)return;
        }
        attrVector<T>::push_back(p);
    }
    void insert(T* p) {
        insert(shared_ptr<T>(p));
    }
private:
    // disable other's call push_back
    void push_back(shared_ptr<T> p);
    void push_back(T* p);
};

template<class T, ATTR_TYPE ETYPE, bool readonly=false>
struct attrMonoVectorRef : IattrArray {
    vector<T>& t;
    bool *nb;
    
    attrMonoVectorRef(vector<T>& t_, bool* nb_) : t(t_), nb(nb_) {}
    
    bool iget(unsigned idx, ostream& o) const {
        if(idx>=t.size())return false;
        IattrGet<T, ETYPE>(o, t[idx]);
        return true;
    }
    bool clear() {
        t.clear();
        return true;
    }
    shared_ptr<Iattr> add(const attrToken& hint) {
        if(hint.type != ETYPE)return NULL;
        if(nb)*nb=true;
        t.push_back( T() );
        return shared_ptr<Iattr>(new attr<T, ETYPE>(*t.rbegin()));
    }
};

template<class T, ATTR_TYPE ETYPE>
struct attrMonoVectorRef<T, ETYPE, true> : IattrArray {
    vector<T>& t;
    
    attrMonoVectorRef(vector<T>& t_) : t(t_) {}
    
    bool iget(unsigned idx, ostream& o) const {
        if(idx>=t.size())return false;
        IattrGet<T, ETYPE>(o, t[idx]);
        return true;
    }
    bool clear(){ return false; }
    shared_ptr<Iattr> add(const attrToken& hint) {
        return NULL;
    }
};

template<class T, ATTR_TYPE ETYPE>
struct attrMonoVector : IattrArray, vector<T> {
    bool *nb;
    attrMonoVector(bool* nb_ = NULL) : nb(nb_){}
    bool iget(unsigned idx, ostream& o) const {
        if(idx>=vector<T>::size())return false;
        IattrGet<T, ETYPE>(o, (*this)[idx]);
        return true;
    }
    bool clear() {
        vector<T>::clear();
        return true;
    }
    shared_ptr<Iattr> add(const attrToken& hint) {
        if(hint.type != ETYPE)return NULL;
        if(nb)*nb=true;
        vector<T>::push_back( T() );
        return shared_ptr<Iattr>(new attr<T, ETYPE>( *(this->rbegin()) ));
    }
};


template<class T, ATTR_TYPE ETYPE, bool readonly=false>
struct attrArray : IattrArray {
    T* t;
    bool *nb;
    unsigned length;
    unsigned capacity;
    
    attrArray(T* t_, unsigned sz_, bool* nb_) : t(t_), nb(nb_), length(sz_), capacity(sz_) { }
    
    bool iget(unsigned idx, ostream& o) const {
        if(idx >= length)return false;
        IattrGet<T, ETYPE>(o, t[idx]);
        return true;
    }
    bool clear() {
        length = 0;
        return true;
    }
    shared_ptr<Iattr> add(const attrToken& hint) {
        if(hint.type != ETYPE || length >= capacity)return NULL;
        return shared_ptr<Iattr>(new attr<T, ETYPE>(t[length++], nb));
    }
};

template<class T, ATTR_TYPE ETYPE>
struct attrArray<T, ETYPE, true> : IattrArray {
    const T* t;
    unsigned length;
    
    attrArray(const T* t_, unsigned sz_) : t(t_), length(sz_) { }
    
    bool iget(unsigned idx, ostream& o) const {
        if(idx >= length)return false;
        IattrGet<T, ETYPE>(o, t[idx]);
        return true;
    }
    
    bool clear() { return false; }
    
    shared_ptr<Iattr> add(const attrToken& hint) {
        return NULL;
    }
};


//////////////////////////////////////////////////////////
typedef map< string, shared_ptr<Iattr> > DictIattr;
struct IattrDict : Iattr {
    virtual void iget(DictIattr& v) const =0;
    virtual bool prepairset() {return true;}
    virtual void iset(const string&, attrTokens&) =0;

    IattrDict() : Iattr(ATTR_DICT) {}

    void get(ostream& o) const { 
        DictIattr vs;
        iget(vs);
        o << "{";
        bool first = true;
        for(auto p : vs){
            ASSERT(p.second);
            if(!first) o << ", "; else first = false;
            o << '"' << p.first << "\":";
            p.second->get(o);
        }
        o << "}";
    }

    void get(string& str) const {
        ostringstream os;
        get(os);
        str = os.str();
    }

    void set(attrTokens& tks) {
        auto a = tks.get();
        if(!a || a->type != type || a->str != "{"){
            tks.error() << "not dict type";
            return;
        }

        if(!prepairset()){
            tks.error() << "readonly";
            return;
        }

        int cnt = 0;
        while(true){
            a = tks.get();
            if(!a || (a->type == type && a->str == "}"))break;
            
            if(a->type != ATTR_STRING){
                tks.error() << "not name type";
                break;
            }
            iset(a->str, tks);
            if(!tks.good())break;
            cnt++;
        }
    }
    bool set(const string& json, string& err){
        istringstream is(json);
        attrTokens tks(is);
        set(tks);
        return !tks.geterror(err);
    }
    
    bool eqset(const string& urlencode, string& err){
        stringpairs qs(urlencode.c_str());
        attrTokens tks;
        tks.add(ATTR_DICT, "{");
        for(auto q : qs){
            vector<string> nms;
            int level = 0;
            tokenize(q.first, nms, ".", true);
            for(auto n : nms){
                if(level++ > 0) 
                    tks.add(ATTR_DICT, "{");
                tks.add(ATTR_STRING, n);
            }
            
            istringstream is(q.second);
            tks.add(is);
            
            while(--level > 0){
                tks.add(ATTR_DICT, "}");
            }
        }
        tks.add(ATTR_DICT, "}");
        set(tks);
        return !tks.geterror(err);
    }
private:
    bool operator ==(const IattrDict&t) const ;
};

// for shared_ptr broken
struct attrDictProxy : IattrDict {
    IattrDict* p;
        
    attrDictProxy(IattrDict* p_) : p(p_) {}
    void get(ostream& os) const {
        p->get(os);
    }
    void set(attrTokens& tks) {
        p->set(tks);
    }
    void iget(DictIattr& v) const {
        p->iget(v);
    }
    bool prepairset() {
        return p->prepairset();
    }
    void iset(const string& a, attrTokens& b) {
        p->iset(a, b);
    }
};

struct attrDictStrict : IattrDict, DictIattr {
    void bind(cstr k, Iattr* p){
        (*this)[k] = shared_ptr<Iattr>(p);
    }
    void bind(cstr k, shared_ptr<Iattr> p){
        (*this)[k] = p;
    }
    void bind(cstr k, bool v){
        (*this)[k] = shared_ptr<Iattr>(new attrc<bool, ATTR_BOOLEAN>(v));
    }
    void bind(cstr k, int v){
        (*this)[k] = shared_ptr<Iattr>(new attrc<int, ATTR_NUMBER>(v));
    }
    void bind(cstr k, const string& v){
        (*this)[k] = shared_ptr<Iattr>(new attrc<string, ATTR_STRING>(v));
    }
    void bind(cstr k, cstr v){
        (*this)[k] = shared_ptr<Iattr>(new attrc<string, ATTR_STRING>(v));
    }
    
    template<typename T, ATTR_TYPE ETYPE>
    void bind(cstr k, vector<T>& v, bool* nb=NULL){
        (*this)[k] = shared_ptr<Iattr>(new attrMonoVector<T,ETYPE>(v, nb));
    }
    
    void erase(const string& k){
        auto i = find(k);
        if(i != end()){
             i->second.reset();
             DictIattr::erase(i);
        }
    }

    void iget(DictIattr& v) const {
        v = *this;
    }
    
    void iset(const string& key, attrTokens& tks){
        auto i = find(key);
        if(i == end()){
            tks.error() << "unknown key";
            return;
        }
        i->second->set(tks);
    }

};

struct attrDict : attrDictStrict {
    void iset(const string& key, attrTokens& tks);
    
    void get(ostream& o) const { 
        IattrDict::get(o);
    }
    void get(string& str) const {
        IattrDict::get(str);
    }
    bool get(const string& name, bool& v){
        auto i = find(name);
        if(i == end() || i->second->type != ATTR_BOOLEAN)return false;
        auto p = static_pointer_cast< attrh<bool, ATTR_BOOLEAN> >(i->second);
        v = p->t;
        return true;
    }
    
    bool get(const string& name, int& v){
        auto i = find(name);
        if(i == end() || i->second->type != ATTR_NUMBER)return false;
        auto p = static_pointer_cast< attrh<int, ATTR_NUMBER> >(i->second);
        v = p->t;
        return true;
    }
    
    bool get(const string& name, string& v){
        auto i = find(name);
        if(i == end() || i->second->type != ATTR_STRING)return false;
        auto p = static_pointer_cast< attrh<string, ATTR_STRING> >(i->second);
        v = p->t;
        return true;
    }
    
    bool get(const string& name, NetAddress& v){
        string sv;
        if(!get(name, sv))return false;
        return v.set(sv.c_str());
    }
    
    bool get(const string& name, shared_ptr< attrVector<Iattr> > & v){
        auto i = find(name);
        if(i == end() || i->second->type != ATTR_ARRAY)return false;
        v = static_pointer_cast< attrVector<Iattr> >(i->second);
        return true;
    }
    
    bool get(const string& name, shared_ptr< attrDict > & v){
        auto i = find(name);
        if(i == end() || i->second->type != ATTR_DICT)return false;
        v = static_pointer_cast< attrDict >(i->second);
        return true;
    }
};


inline shared_ptr<Iattr> attrHintNew(const attrToken& hint)
{
    shared_ptr<Iattr> p;
    if(hint.type == ATTR_BOOLEAN) p.reset(new attrh<bool, ATTR_BOOLEAN>());
    if(hint.type == ATTR_NUMBER) {
        if(hint.str.find('.') != string::npos)
            p.reset(new attrh<double, ATTR_NUMBER>());
        else
            p.reset(new attrh<int, ATTR_NUMBER>());
    }
    if(hint.type == ATTR_STRING) p.reset(new attrh<string, ATTR_STRING>());
    if(hint.type == ATTR_ARRAY) p.reset(new attrVector<Iattr>());
    if(hint.type == ATTR_DICT) p.reset(new attrDict());
    return p;
}


inline void attrDict::iset(const string& key, attrTokens& tks) {
    auto i = find(key);
    if(i != end()){
        i->second->set(tks);
        return;
    }
    auto a = tks.peek();
    if(!a)return;
    
    shared_ptr<Iattr> p = attrHintNew(*a);
    if(!p){
        tks.error() << "eos/null when setting dict value";
        return;
    }
    (*this)[key] = p;
    p->set(tks);
}

inline shared_ptr<Iattr> attrVector<Iattr>::add(const attrToken& hint){
    shared_ptr<Iattr> p = attrHintNew(hint);
    if(!p)return p;
    vector< shared_ptr<Iattr> >::push_back(p);
    return p;
}
