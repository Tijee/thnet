#pragma once


struct THLogger : ICtrlRecv {
    shared_ptr<attrDict> cfg;
    ClassMemberTimer<THLogger> timer;

    THLogger(shared_ptr<attrDict> c): cfg(c), timer(this, &THLogger::OnKickIdle){
        string file;
        if(cfg->get("file", file)){
            FILE* flog=fopen(file.c_str(), "at");
            if(flog) dup2(fileno(flog), 2);
        }
        int kickidle;
        if(cfg->get("kickidle", kickidle)){
            timer.start(1, (unsigned)kickidle);
        }
        setvbuf(stderr, NULL, _IONBF, 0);  // fprintf(stderr,) ie LOG won't buffer
    }

    void OnCtrlRecv(const vector<char*>& lines){
        for(auto line : lines){
            if(line[0] <= 2)continue;
            
            char* end = line + strlen(line);
            char sv = end[1];
            end[1] = 0;
            end[0] = '\n';
            fputs(line, stderr);
            end[1] = sv;
            end[0] = 0;
        }
    }
    void OnKickIdle(){
        LOG_INFO("---- kickidle ----");
    }
};

