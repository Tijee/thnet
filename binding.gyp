{
  "targets": [
    {
      "target_name": "tjapps",
      'type': 'executable',
      "sources": [ 
          "src/main.cpp",  
          "src/thutil.cpp",
          "src/thpoll.cpp",
          "src/apps.cpp",
          "src/logger.cpp",
          
          "src/thnet.h",
          "src/thattr.h",
          "src/thutil.h",
       ],
       
       'dependencies': [
          'thnet',
       ],
       'libraries': [
          '-lcrypto',
       ],
    },
    {
      "target_name": "thnet",
      'type': 'static_library',
      "sources": [ 
          "src/thnet/ifs.cpp",  
          "src/thnet/thnet.cpp",
          "src/thnet/thnetapp.cpp",

          "src/thnet/iftun.h",  
          "src/thnet/ifudp.h",
          "src/thnet/ifndp.h",
          "src/thnet/if6lowpan.h",
          "src/thnet/ifdhcp.h",
          "src/thnet/thnet.h",
          "src/thnet.h",
          "src/thattr.h",
          "src/thutil.h",
       ],
   
    }
  ]
}
