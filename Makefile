HOSTBIN:=/usr/bin

OBUILDTYPE=$(shell cat "build/type")
BUILDTYPE:=$(OBUILDTYPE)
ifeq ($(BUILDTYPE),)
BUILDTYPE=Release
endif
ifneq ($(BUILDTYPE),$(OBUILDTYPE))
$(shell rm -rf "build")
endif

GYP=node_modules/node-gyp/bin/node-gyp.js
EXE=build/$(BUILDTYPE)/tjapps

ifeq ($(BUILDTYPE), Debug)
DFLAGS=--debug
endif

.PHONY: all test clean

all: $(EXE)

$(EXE): $(GYP) build/type build/ver.h $(wildcard src/logger/*.cpp src/thnet/*.cpp src/thnet/*.h src/*.cpp src/*.h)
	$(HOSTBIN)/node $(GYP) build

build/type: binding.gyp
	$(HOSTBIN)/node $(GYP) configure $(DFLAGS)
	echo $(BUILDTYPE) >build/type

build/ver.h:
	#echo "v=`cat package.json`; console.info(\"#define THVER \", JSON.stringify(v.version))" | node >build/ver.h
	echo "#define THVER \"0.2.1\"" >build/ver.h
	
$(GYP):
	$(HOSTBIN)/npm install node-gyp nan

build/gateway.tgz: build/Release/tjapps
	./mktarball

install: build/gateway.tgz
	sudo tar xvz -C / -f build/gateway.tgz

test: $(EXE)
	node test/testmon.js

gtest: $(EXE)
	cd test; sudo node gateway.js

gdb:
	#echo "file $(HOSTBIN)/node\n$(CMD)\nr test/test.js" >~/.gdbinit
	echo >~/.gdbinit
	gdb $(EXE)

clean:
	-rm -rf build
