# thnet

thnet is a Node plugin for ipv6 homenet tunneling solution.
thnet means Tijee Home Network.

Original project site is https://bitbucket.org/tijee/thnet

## Features

 * little overload IPv4 tunneling in IPv4 TCP/UDP
 
 * robust IPv4 nat breakthrough

 * NDP (Neighbor Discovery Proxy) to solve ::/64 non-routing IPv6 problem
 
 * DHCPv6
 
 * 6LowPAN border router ( should work with contiki slip border router )
 
 * embeded simple IPv6 firewall

 * security solution based on TLS 1.0, AES-128 ...

## Build Instructions

```
npm install
```

## Supported Platforms

Linux 3.0 +

Nodjs 4+

